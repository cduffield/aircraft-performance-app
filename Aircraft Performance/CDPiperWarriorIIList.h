//
//  CDPiperWarriorIIList.h
//  Aircraft Performance
//
//  Created by Chip Duffield on 8/2/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface CDPiperWarriorIIList : NSManagedObject

@property (nonatomic, retain) NSString * nNumber;
@property (nonatomic, retain) NSString * emptyWeight;
@property (nonatomic, retain) NSString * arm;

@end
