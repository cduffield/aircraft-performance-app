//
//  CDCirrusSR20M3000LandingGraphViewController.h
//  Aircraft Performance
//
//  Created by Chip Duffield on 8/1/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CorePlot-CocoaTouch.h"

@interface CDCirrusSR20M3000LandingGraphViewController : UIViewController <CPTPlotDataSource>

@property (nonatomic) NSString *landingWeight;
@property (nonatomic) NSString *landingArm;
@property (nonatomic) NSString *landingCG;

@end
