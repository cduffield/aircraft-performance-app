//
//  CDCessna172SLandingDataViewController.h
//  Aircraft Performance
//
//  Created by Chip Duffield on 8/16/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDCessna172SLandingDataViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *gphTextField;
@property (weak, nonatomic) IBOutlet UITextField *timeEnrouteTextField;
@property (weak, nonatomic) IBOutlet UITextField *fieldElevationTextField;
@property (weak, nonatomic) IBOutlet UITextField *pressureTextField;
@property (weak, nonatomic) IBOutlet UITextField *temperatureTextField;

@property (weak, nonatomic) IBOutlet UILabel *totalWeightLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalArmLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalMomentLabel;
@property (weak, nonatomic) IBOutlet UILabel *cgLabel;
@property (weak, nonatomic) IBOutlet UILabel *groundRollLabel;
@property (weak, nonatomic) IBOutlet UILabel *obstacleClearanceLabel;
@property (weak, nonatomic) IBOutlet UITextField *windDirectionTextField;
@property (weak, nonatomic) IBOutlet UITextField *windSpeedTextField;
@property (weak, nonatomic) IBOutlet UITextField *runwayTextField;


- (IBAction)calculateLanding:(id)sender;

@property (nonatomic) NSString *totalWeightBefore;
@property (nonatomic) NSString *fuelGallongsBefore;
@property (nonatomic) NSString *totalMomentBefore;
@property (nonatomic) NSString *fuelARM;
@property (nonatomic) NSString *pilotWeight;
@property (nonatomic) NSString *frontSeatWeight;
@property (nonatomic) NSString *rearSeat1Weight;
@property (nonatomic) NSString *rearSeat2Weight;
@property (nonatomic) NSString *bagArea1Weight;
@property (nonatomic) NSString *bagArea2Weight;
@property (nonatomic) NSString *fuelGallonsLdg;
@property (nonatomic) NSString *fuelWeightLdg;
@property (nonatomic) NSString *totalWeightLdg;
@property (nonatomic) NSString *fuelWeightBefore;
@property (nonatomic) NSString *totalArmTakeoff;
@property (nonatomic) NSString *landingDensityAltitude;
@property (nonatomic) NSString *nNumber;
@property (nonatomic) NSString *toManeurveringSpeed;

@property (nonatomic) NSString *emptyWeight;
@property (nonatomic) NSString *emptyArm;

@property (nonatomic) NSString *fieldElevationTO;
@property (nonatomic) NSString *pressureTO;
@property (nonatomic) NSString *temperatureTO;
@property (nonatomic) NSString *takeoffWindComponent;
@property (nonatomic) NSString *landingWindComponent;
@property (nonatomic) NSString *landingCrossWindComponent;

@property (nonatomic) NSString *takeoffCG;
@property (nonatomic) NSString *takeoffGroundRoll;
@property (nonatomic) NSString *takeoffClearObstacle;
@property (nonatomic) NSString *takeoffDensityAltitude;
@property (nonatomic) NSString *takeoffWindDirection;
@property (nonatomic) NSString *takeoffWindSpeed;
@property (nonatomic) NSString *takeoffRunway;
@property (nonatomic) NSString *takeoffCrossWindComponent;
@property (nonatomic) NSDictionary *landingGroundRoll;
@property (nonatomic) NSDictionary *landingClearObstacle;

@property (nonatomic) NSString *ldgManeuveringSpeed;

@property (nonatomic) float a;
@property (nonatomic) float b;
@property (nonatomic) NSNumber *key1;
@property (nonatomic) NSNumber *key2;
@property (nonatomic) float pLower;
@property (nonatomic) float pHigher;
@property (nonatomic) float tempLower;
@property (nonatomic) float groundroll;
@property (nonatomic) float clearObstacle;


@end
