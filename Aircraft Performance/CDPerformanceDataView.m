//
//  CDPerformanceDataView.m
//  Aircraft Performance
//
//  Created by Chip Duffield on 7/19/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import "CDPerformanceDataView.h"

@implementation CDPerformanceDataView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
