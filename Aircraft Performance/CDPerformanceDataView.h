//
//  CDPerformanceDataView.h
//  Aircraft Performance
//
//  Created by Chip Duffield on 7/19/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDPerformanceDataView : UIView

@property (weak, nonatomic) IBOutlet UILabel *emptyWeightContent;

@property (weak, nonatomic) IBOutlet UILabel *pilotWeightContent;
@property (weak, nonatomic) IBOutlet UILabel *frontSeatWeightContent;

@property (weak, nonatomic) IBOutlet UILabel *rearSeat1WeightContent;
@property (weak, nonatomic) IBOutlet UILabel *rearSeat2WeightContent;
@property (weak, nonatomic) IBOutlet UILabel *bagArea1WeightContent;
@property (weak, nonatomic) IBOutlet UILabel *bagArear2WeightContent;
@property (weak, nonatomic) IBOutlet UILabel *fuelWeightContent;
@property (weak, nonatomic) IBOutlet UILabel *fuelGalContent;
@property (weak, nonatomic) IBOutlet UILabel *emptyWeightArmContent;
@property (weak, nonatomic) IBOutlet UILabel *totalWeightContent;
@property (weak, nonatomic) IBOutlet UILabel *totalArmContent;
@property (weak, nonatomic) IBOutlet UILabel *emptyWeightMomentContent;
@property (weak, nonatomic) IBOutlet UILabel *pilotMomentContent;
@property (weak, nonatomic) IBOutlet UILabel *frontSeatMomentContent;
@property (weak, nonatomic) IBOutlet UILabel *rearSeat1MomentContent;
@property (weak, nonatomic) IBOutlet UILabel *rearSeat2MomentContent;
@property (weak, nonatomic) IBOutlet UILabel *bagArea1MomentContent;
@property (weak, nonatomic) IBOutlet UILabel *bagArea2MomentContent;
@property (weak, nonatomic) IBOutlet UILabel *fuelMomentContent;
@property (weak, nonatomic) IBOutlet UILabel *totalMomentContent;
@property (weak, nonatomic) IBOutlet UILabel *cgLabelContent;
@property (weak, nonatomic) IBOutlet UILabel *toManSpeedContent;
@property (weak, nonatomic) IBOutlet UILabel *toWindContent;
@property (weak, nonatomic) IBOutlet UILabel *toDensityAltitudeContent;
@property (weak, nonatomic) IBOutlet UILabel *toGroundRollContent;
@property (weak, nonatomic) IBOutlet UILabel *to50ftObstacleContent;

@property (weak, nonatomic) IBOutlet UILabel *landingEmptyWeightContent;
@property (weak, nonatomic) IBOutlet UILabel *landingPilotWeightContent;
@property (weak, nonatomic) IBOutlet UILabel *landingFrontSeatWeightContent;
@property (weak, nonatomic) IBOutlet UILabel *landingRearSeat1WeightContent;
@property (weak, nonatomic) IBOutlet UILabel *landingRearSeat2WeightContent;
@property (weak, nonatomic) IBOutlet UILabel *landingBagArea1WeightContent;
@property (weak, nonatomic) IBOutlet UILabel *landingBagArea2WeightContent;
@property (weak, nonatomic) IBOutlet UILabel *landingFuelGalContent;
@property (weak, nonatomic) IBOutlet UILabel *landingFuelWeightContent;
@property (weak, nonatomic) IBOutlet UILabel *landingTotalWeightContent;
@property (weak, nonatomic) IBOutlet UILabel *landingEmptyWeighArmContent;
@property (weak, nonatomic) IBOutlet UILabel *landingTotalWeightArmContent;
@property (weak, nonatomic) IBOutlet UILabel *landingEmptyWeightMomentContent;
@property (weak, nonatomic) IBOutlet UILabel *landingPilotMomentContent;
@property (weak, nonatomic) IBOutlet UILabel *landingFrontSeatMomentContent;
@property (weak, nonatomic) IBOutlet UILabel *landingRearSeat1MomentContent;
@property (weak, nonatomic) IBOutlet UILabel *landingRearSeat2MomentContent;
@property (weak, nonatomic) IBOutlet UILabel *landingBagArea1MomentContent;
@property (weak, nonatomic) IBOutlet UILabel *landingBagArea2MomentContent;
@property (weak, nonatomic) IBOutlet UILabel *landingFuelMomentContent;
@property (weak, nonatomic) IBOutlet UILabel *landingTotalWeightMomentContent;
@property (weak, nonatomic) IBOutlet UILabel *landingCGLabelContent;
@property (weak, nonatomic) IBOutlet UILabel *landingManSpeed;
@property (weak, nonatomic) IBOutlet UILabel *landingWindSpeedContent;
@property (weak, nonatomic) IBOutlet UILabel *landingDensityAltitudeContent;
@property (weak, nonatomic) IBOutlet UILabel *landingGroundRollContent;
@property (weak, nonatomic) IBOutlet UILabel *landing50ftObstacleContent;
@property (weak, nonatomic) IBOutlet UILabel *pilotArmContent;

@property (weak, nonatomic) IBOutlet UILabel *frontSeatArmContent;
@property (weak, nonatomic) IBOutlet UILabel *rearSeat1ArmContent;
@property (weak, nonatomic) IBOutlet UILabel *rearSeat2ArmContent;
@property (weak, nonatomic) IBOutlet UILabel *bagArea1ArmContent;
@property (weak, nonatomic) IBOutlet UILabel *bagArea2ArmContent;
@property (weak, nonatomic) IBOutlet UILabel *fuelArmContent;
@property (weak, nonatomic) IBOutlet UILabel *nNumberContent;
@property (weak, nonatomic) IBOutlet UILabel *todaysDateContent;
@property (weak, nonatomic) IBOutlet UILabel *toRunwayContent;
@property (weak, nonatomic) IBOutlet UILabel *toWindComponentContent;
@property (weak, nonatomic) IBOutlet UILabel *landingRunwayContent;
@property (weak, nonatomic) IBOutlet UILabel *landingWindComponentText;
@property (weak, nonatomic) IBOutlet UILabel *takeoffCrossWindComponent;
@property (weak, nonatomic) IBOutlet UILabel *landingCrossWindComponent;




@end
