//
//  CDCirrusSR22TM3600TakeoffDataViewController.m
//  Aircraft Performance
//
//  Created by Chip Duffield on 8/20/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import "CDCirrusSR22TM3600TakeoffDataViewController.h"
#import "CDCirrusSR22TM3600LandingDataViewController.h"

@interface CDCirrusSR22TM3600TakeoffDataViewController ()

@end

@implementation CDCirrusSR22TM3600TakeoffDataViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = false;
    
    self.totalWeightLabel.text = self.totalWeightText;
    
    NSArray *seaLevel = [[NSArray alloc] initWithObjects:@485,@524,@564,@606,@650,@695,nil];
    NSArray *p1000 = [[NSArray alloc] initWithObjects:@517,@559,@602,@647,@694,@742,nil];
    NSArray *p2000 = [[NSArray alloc] initWithObjects:@552,@596,@642,@690,@740,@792,nil];
    NSArray *p3000 = [[NSArray alloc] initWithObjects:@589,@637,@686,@737,@791,@846,nil];
    NSArray *p4000 = [[NSArray alloc] initWithObjects:@630,@680,@733,@788,@845,@904,nil];
    NSArray *p5000 = [[NSArray alloc] initWithObjects:@673,@727,@783,@842,@903,@966,nil];
    NSArray *p6000 = [[NSArray alloc] initWithObjects:@720,@778,@838,@900,@965,@1033,nil];
    NSArray *p7000 = [[NSArray alloc] initWithObjects:@770,@832,@896,@963,@1033,@1105,nil];
    NSArray *p8000 = [[NSArray alloc] initWithObjects:@824,@890,@959,@1031,@1106,@1183,nil];
    NSArray *p9000 = [[NSArray alloc] initWithObjects:@883,@954,@1028,@1104,@1184,@1267,nil];
    NSArray *p10000 = [[NSArray alloc] initWithObjects:@946,@1022,@1101,@1183,@1269,@1358,nil];
    self.toDistance2900 = [[NSDictionary alloc] initWithObjectsAndKeys:seaLevel,@0,p1000,@1000,p2000,@2000,p3000,@3000,p4000,@4000,p5000,@5000,p6000,@6000,p7000,@7000,p8000,@8000,p9000,@9000,p10000,@10000, nil];
    
    NSArray *seaLevelat3600 = [[NSArray alloc] initWithObjects:@1352,@1461,@1574,@1692,@1814,@1941,nil];
    NSArray *p1000at3600 = [[NSArray alloc] initWithObjects:@1443,@1559,@1680,@1805,@1936,@2071,nil];
    NSArray *p2000at3600 = [[NSArray alloc] initWithObjects:@1540,@1664,@1793,@1927,@2066,@2210,nil];
    NSArray *p3000at3600 = [[NSArray alloc] initWithObjects:@1645,@1777,@1914,@2058,@2206,@2361,nil];
    NSArray *p4000at3600 = [[NSArray alloc] initWithObjects:@1757,@1898,@2045,@2198,@2357,@2522,nil];
    NSArray *p5000at3600 = [[NSArray alloc] initWithObjects:@1878,@2029,@2186,@2350,@2520,@2696,nil];
    NSArray *p6000at3600 = [[NSArray alloc] initWithObjects:@2008,@2170,@2338,@2513,@2694,@2883,nil];
    NSArray *p7000at3600 = [[NSArray alloc] initWithObjects:@2149,@2322,@2501,@2688,@2883,@3084,nil];
    NSArray *p8000at3600 = [[NSArray alloc] initWithObjects:@2300,@2485,@2678,@2878,@3086,@3302,nil];
    NSArray *p9000at3600 = [[NSArray alloc] initWithObjects:@2463,@2661,@2868,@3082,@3305,@3536,nil];
    NSArray *p10000at3600 = [[NSArray alloc] initWithObjects:@2640,@2852,@3073,@3303,@3541,@3789,nil];
    self.toDistance3600 = [[NSDictionary alloc] initWithObjectsAndKeys:seaLevelat3600,@0,p1000at3600,@1000,p2000at3600,@2000,p3000at3600,@3000,p4000at3600,@4000,p5000at3600,@5000,p6000at3600,@6000,p7000at3600,@7000,p8000at3600,@8000,p9000at3600,@9000,p10000at3600,@10000, nil];
    
    NSArray *pressureSeaLevel = [[NSArray alloc] initWithObjects:@766,@823,@882,@944,@1007,@1073,nil];
    NSArray *pressure1000 = [[NSArray alloc] initWithObjects:@812,@872,@935,@1000,@1068,@1138,nil];
    NSArray *pressure2000 = [[NSArray alloc] initWithObjects:@861,@925,@992,@1061,@1133,@1207,nil];
    NSArray *pressure3000 = [[NSArray alloc] initWithObjects:@914,@982,@1053,@1126,@1202,@1281,nil];
    NSArray *pressure4000 = [[NSArray alloc] initWithObjects:@970,@1043,@1118,@1196,@1277,@1360,nil];
    NSArray *pressure5000 = [[NSArray alloc] initWithObjects:@1030,@1108,@1188,@1271,@1357,@1446,nil];
    NSArray *pressure6000 = [[NSArray alloc] initWithObjects:@1095,@1177,@1262,@1351,@1442,@1537,nil];
    NSArray *pressure7000 = [[NSArray alloc] initWithObjects:@1164,@1252,@1343,@1437,@1534,@1634,nil];
    NSArray *pressure8000 = [[NSArray alloc] initWithObjects:@1239,@1332,@1428,@1529,@1632,@1739,nil];
    NSArray *pressure9000 = [[NSArray alloc] initWithObjects:@1318,@1418,@1521,@1627,@1738,@1852,nil];
    NSArray *pressure10000 = [[NSArray alloc] initWithObjects:@1404,@1510,@1620,@1733,@1851,@1973,nil];
    self.toDistanceObstacle2900 = [[NSDictionary alloc] initWithObjectsAndKeys:pressureSeaLevel,@0,pressure1000,@1000,pressure2000,@2000,pressure3000,@3000,pressure4000,@4000,pressure5000,@5000,pressure6000,@6000,pressure7000,@7000,pressure8000,@8000,pressure9000,@9000,pressure10000,@10000, nil];
    
    NSArray *pressureSeaLevelat3600 = [[NSArray alloc] initWithObjects:@1865,@2007,@2154,@2307,@2465,@2629,nil];
    NSArray *pressure1000at3600 = [[NSArray alloc] initWithObjects:@1980,@2131,@2288,@2450,@2618,@2792,nil];
    NSArray *pressure2000at3600 = [[NSArray alloc] initWithObjects:@2104,@2264,@2431,@2603,@2782,@2967,nil];
    NSArray *pressure3000at3600 = [[NSArray alloc] initWithObjects:@2236,@2407,@2584,@2767,@2958,@3154,nil];
    NSArray *pressure4000at3600 = [[NSArray alloc] initWithObjects:@2378,@2559,@2748,@2943,@3146,@3555,nil];
    NSArray *pressure5000at3600 = [[NSArray alloc] initWithObjects:@2530,@2723,@2924,@3132,@3347,@3570,nil];
    NSArray *pressure6000at3600 = [[NSArray alloc] initWithObjects:@2693,@2899,@3113,@3334,@3564,@3802,nil];
    NSArray *pressure7000at3600 = [[NSArray alloc] initWithObjects:@2868,@3088,@3315,@3552,@3796,@4050,nil];
    NSArray *pressure8000at3600 = [[NSArray alloc] initWithObjects:@3056,@3290,@3533,@3785,@4046,@4316,nil];
    NSArray *pressure9000at3600 = [[NSArray alloc] initWithObjects:@3258,@3508,@3767,@4036,@4314,@4603,nil];
    NSArray *pressure10000at3600 = [[NSArray alloc] initWithObjects:@3476,@3742,@4019,@4306,@4603,@4911,nil];
    self.toDistanceObstacle3600 = [[NSDictionary alloc] initWithObjectsAndKeys:pressureSeaLevelat3600,@0,pressure1000at3600,@1000,pressure2000at3600,@2000,pressure3000at3600,@3000,pressure4000at3600,@4000,pressure5000at3600,@5000,pressure6000at3600,@6000,pressure7000at3600,@7000,pressure8000at3600,@8000,pressure9000at3600,@9000,pressure10000at3600,@10000, nil];
    
    
}




- (IBAction)calculate:(id)sender {
    
    float windDirection = [self.windDirectionTextField.text floatValue];
    float windSpeed = [self.windSpeedTextField.text floatValue];
    float runwayDirection = ([self.runwayTextField.text floatValue]*10);
    
    if (runwayDirection > 360) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please enter runway without last Digit. Use 9 instead of 090" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
        [alertView show];
        
    } else if (windDirection > 360) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error'" message:@"Wind Direction can not be greater that 360" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
        [alertView show];
    } else {
        
        //constants for both
        float pressure = [self.pressureTextField.text floatValue];
        float temp = [self.temperatureTextField.text floatValue];
        float fieldElevation = [self.fieldElevationTextField.text floatValue];
        float pressureAltitude = (((29.92 - pressure)*1000) + fieldElevation);
        float isaTemp = (15 -((pressureAltitude/1000) * 1.98));
        float totalWeight = [self.totalWeightText floatValue];
        
        
        float groundRollCorrectedForWind;
        float clearObstacleCorrectedForWind;
        
        //calculate density altitude
        float densityAltitude = (pressureAltitude + (118.8 * (temp - isaTemp)));
        self.densityAltitudeLabel.text = [[NSString alloc] initWithFormat:@"%.f",densityAltitude];
        
        if (totalWeight <= 2900) {
            [self calculateDistances:temp pressure:pressureAltitude windDirection:windDirection windSpeed:windSpeed runwayDirection:runwayDirection dictionary:self.toDistance2900 dictionaryObstacle:self.toDistanceObstacle2900 weight:totalWeight];
            
            if ((windDirection - runwayDirection) <= 90) {
                float angle = windDirection - runwayDirection;
                float windComponent = cosf(angle*M_PI/180);
                float headwind = windSpeed * windComponent;
                float crosswindComponent = sinf(angle*M_PI/180);
                float crosswind = windSpeed * crosswindComponent;
                float crosswindAbs = fabsf(crosswind);
                self.takeoffCrossWindComponent = [[NSString alloc] initWithFormat:@"%.f",crosswindAbs];
                self.headwindComponent = [[NSString alloc] initWithFormat:@"%.f",headwind];
                int headwindInt = (int)floorf(headwind);
                int headwindCorrection = ((headwindInt/13)*.1);
                groundRollCorrectedForWind = self.groundroll * (1 - headwindCorrection);
                clearObstacleCorrectedForWind = self.clearObstacle * (1- headwindCorrection);
                self.takeoffGroundRollLabel.text = [[NSString alloc] initWithFormat:@"%.f",groundRollCorrectedForWind];
                self.takeoffClearObstacleLabel.text = [[NSString alloc] initWithFormat:@"%.f",clearObstacleCorrectedForWind];
            } else {
                float angle = windDirection - runwayDirection;
                float windComponent = cosf(angle*M_PI/180);
                float tailwind = windSpeed * windComponent;
                float crosswindComponent = sinf(angle*M_PI/180);
                float crosswind = windSpeed * crosswindComponent;
                self.takeoffCrossWindComponent = [[NSString alloc] initWithFormat:@"%.f",crosswind];
                self.headwindComponent = [[NSString alloc] initWithFormat:@"%.f",tailwind];
                int tailwindInt = (int)floorf(tailwind);
                int tailwindCorrection = ((tailwindInt/2)*.1);
                groundRollCorrectedForWind = self.groundroll * (1 + tailwindCorrection);
                clearObstacleCorrectedForWind = self.clearObstacle * (1 + tailwindCorrection);
                self.takeoffGroundRollLabel.text = [[NSString alloc] initWithFormat:@"%.f",groundRollCorrectedForWind];
                self.takeoffClearObstacleLabel.text = [[NSString alloc] initWithFormat:@"%.f",clearObstacleCorrectedForWind];
                
            }
            
        } else {
            [self calculateDistances:temp pressure:pressureAltitude windDirection:windDirection windSpeed:windSpeed runwayDirection:runwayDirection dictionary:self.toDistance2900 dictionaryObstacle:self.toDistanceObstacle2900 weight:2900];
            [self calculateDistances:temp pressure:pressureAltitude windDirection:windDirection windSpeed:windSpeed runwayDirection:runwayDirection dictionary:self.toDistance3600 dictionaryObstacle:self.toDistanceObstacle3600 weight:3600];
            
            float y = ((totalWeight - 2900)/700);
            self.groundroll = (((self.groundroll3000 - self.groundroll2500)*y)+self.groundroll2500);
            self.clearObstacle = (((self.clearObstacle3000 - self.clearObstacle2500)*y)+self.clearObstacle2500);
            
            if ((windDirection - runwayDirection) <= 90) {
                float angle = windDirection - runwayDirection;
                float windComponent = cosf(angle*M_PI/180);
                float headwind = windSpeed * windComponent;
                float crosswindComponent = sinf(angle*M_PI/180);
                float crosswind = windSpeed * crosswindComponent;
                float crosswindAbs = fabsf(crosswind);
                self.takeoffCrossWindComponent = [[NSString alloc] initWithFormat:@"%.f",crosswindAbs];
                self.headwindComponent = [[NSString alloc] initWithFormat:@"%.f",headwind];
                int headwindInt = (int)floorf(headwind);
                int headwindCorrection = ((headwindInt/9)*.1);
                groundRollCorrectedForWind = self.groundroll * (1 - headwindCorrection);
                clearObstacleCorrectedForWind = self.clearObstacle * (1- headwindCorrection);
                self.takeoffGroundRollLabel.text = [[NSString alloc] initWithFormat:@"%.f",groundRollCorrectedForWind];
                self.takeoffClearObstacleLabel.text = [[NSString alloc] initWithFormat:@"%.f",clearObstacleCorrectedForWind];
            } else {
                float angle = windDirection - runwayDirection;
                float windComponent = cosf(angle*M_PI/180);
                float tailwind = windSpeed * windComponent;
                float crosswindComponent = sinf(angle*M_PI/180);
                float crosswind = windSpeed * crosswindComponent;
                self.takeoffCrossWindComponent = [[NSString alloc] initWithFormat:@"%.f",crosswind];
                self.headwindComponent = [[NSString alloc] initWithFormat:@"%.f",tailwind];
                int tailwindInt = (int)floorf(tailwind);
                int tailwindCorrection = ((tailwindInt/2)*.1);
                groundRollCorrectedForWind = self.groundroll * (1 + tailwindCorrection);
                clearObstacleCorrectedForWind = self.clearObstacle * (1 + tailwindCorrection);
                self.takeoffGroundRollLabel.text = [[NSString alloc] initWithFormat:@"%.f",groundRollCorrectedForWind];
                self.takeoffClearObstacleLabel.text = [[NSString alloc] initWithFormat:@"%.f",clearObstacleCorrectedForWind];
                
            }
            
        }
        
        
        
        
    }
}

- (void)calculateTempIndex:(float)temp {
    if (temp < 0) {
        self.a = 0;
    }
    if (temp >= 0 && temp < 10) {
        self.a = 0;
        self.b = 1;
        self.tempLower = 0;
    }
    if (temp >= 10 && temp < 20) {
        self.a = 1;
        self.b = 2;
        self.tempLower = 10;
    }
    if (temp >= 20 && temp < 30) {
        self.a = 2;
        self.b = 3;
        self.tempLower = 20;
    }
    if (temp >= 30 && temp < 40) {
        self.a = 3;
        self.b = 4;
        self.tempLower = 30;
    }
    if (temp >= 40 && temp < 50) {
        self.a = 4;
        self.b = 5;
        self.tempLower = 40;
    }
    if (temp == 5) {
        self.a = 5;
    }
}

- (void)pressureAltitudeKeys:(int)pressureAltitude {
    if (pressureAltitude < 0) {
        self.key1 = @0;
    }
    if (pressureAltitude >= 0 && pressureAltitude < 1000) {
        self.key1 = @0;
        self.key2 = @1000;
        self.pLower = 0;
        self.pHigher = 1000;
    }
    if (pressureAltitude >= 1000 && pressureAltitude < 2000) {
        self.key1 = @1000;
        self.key2 = @2000;
        self.pLower = 1000;
        self.pHigher = 2000;
    }
    if (pressureAltitude >= 2000 && pressureAltitude < 3000) {
        self.key1 = @2000;
        self.key2 = @3000;
        self.pLower = 2000;
        self.pHigher = 3000;
    }
    if (pressureAltitude >= 3000 && pressureAltitude < 4000) {
        self.key1 = @3000;
        self.key2 = @4000;
        self.pLower = 3000;
        self.pHigher = 4000;
    }
    if (pressureAltitude >= 4000 && pressureAltitude < 5000) {
        self.key1 = @4000;
        self.key2 = @5000;
        self.pLower = 4000;
        self.pHigher = 5000;
    }
    if (pressureAltitude >= 5000 && pressureAltitude < 6000) {
        self.key1 = @5000;
        self.key2 = @6000;
        self.pLower = 5000;
        self.pHigher = 6000;
    }
    if (pressureAltitude >= 6000 && pressureAltitude < 7000) {
        self.key1 = @6000;
        self.key2 = @7000;
        self.pLower = 6000;
        self.pHigher = 7000;
    }
    if (pressureAltitude >= 7000 && pressureAltitude < 8000) {
        self.key1 = @7000;
        self.key2 = @8000;
        self.pLower = 7000;
        self.pHigher = 8000;
    }
    if (pressureAltitude >= 8000 && pressureAltitude < 9000) {
        self.key1 = @8000;
        self.key2 = @9000;
        self.pLower = 8000;
        self.pHigher = 9000;
    }
    if (pressureAltitude >= 9000 && pressureAltitude < 10000) {
        self.key1 = @9000;
        self.key2 = @10000;
        self.pLower = 9000;
        self.pHigher = 10000;
    }
    if (pressureAltitude == 10000) {
        self.key1 = @10000;
    }
    else {
        
    }
}

- (void)calculateDistances:(float)temp pressure:(float)pressureAltitude windDirection:(float)windDirection windSpeed:(float)windSpeed runwayDirection:(float)runwayDirection dictionary:(NSDictionary *)dictionaryValues dictionaryObstacle:(NSDictionary *)dictionaryObstacleValues  weight:(float)weight {
    [self calculateTempIndex:temp];
    [self pressureAltitudeKeys:pressureAltitude];
    float groundRoll;
    float clearObstacle;
    
    
    if (pressureAltitude < 0) {
        if (temp < 0) {
            NSArray *lowerValue = [dictionaryValues objectForKey:self.key1];
            if ([lowerValue objectAtIndex:self.a] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float lower = [[lowerValue objectAtIndex:self.a] floatValue];
                groundRoll = lower;
            }
            
            
            NSArray *lValue = [dictionaryObstacleValues objectForKey:self.key1];
            if ([lValue objectAtIndex:self.a] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float l = [[lValue objectAtIndex:self.a] floatValue];
                clearObstacle = l;
            }
        }
        if (temp >= 0 && temp < 50) {
            float x = ((temp - self.tempLower)/10);
            NSArray *lowerValue = [dictionaryValues objectForKey:self.key1];
            if ([lowerValue objectAtIndex:self.a] == nil || [lowerValue objectAtIndex:self.b] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float lower0 = [[lowerValue objectAtIndex:self.a] floatValue];
                float lower1 = [[lowerValue objectAtIndex:self.b] floatValue];
                float lower = (((lower1 - lower0)*x)+lower0);
                groundRoll = lower;
            }
            
            
            NSArray *lValue = [dictionaryObstacleValues objectForKey:self.key1];
            if ([lValue objectAtIndex:self.a] == nil || [lValue objectAtIndex:self.b] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float l0 = [[lValue objectAtIndex:self.a] floatValue];
                float l1 = [[lValue objectAtIndex:self.b] floatValue];
                float l = (((l1 - l0)*x)+l0);
                clearObstacle = l;
            }
            
        }
        if (temp == 50) {
            NSArray *lowerValue = [dictionaryValues objectForKey:self.key1];
            if ([lowerValue objectAtIndex:self.a] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float lower = [[lowerValue objectAtIndex:self.a] floatValue];
                groundRoll = lower;
            }
            
            NSArray *lValue = [dictionaryObstacleValues objectForKey:self.key1];
            if ([lValue objectAtIndex:self.a] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float l = [[lValue objectAtIndex:self.a] floatValue];
                clearObstacle = l;
            }
        }
        if (temp > 50) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data of 50 degrees Celsius" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
            [alertView show];
        }
    }
    
    if (pressureAltitude >= 0 && pressureAltitude < 10000) {
        if (temp < 0 ) {
            float y = ((pressureAltitude - self.pLower)/1000);
            NSArray *lowerValue = [dictionaryValues objectForKey:self.key1];
            NSArray *higherValue = [dictionaryValues objectForKey:self.key2];
            if ([lowerValue objectAtIndex:0] == nil || [higherValue objectAtIndex:0] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float lower = [[lowerValue objectAtIndex:0] floatValue];
                float higher = [[higherValue objectAtIndex:0] floatValue];
                groundRoll = (((higher - lower)*y)+lower);
            }
            
            
            NSArray *lValue = [dictionaryObstacleValues objectForKey:self.key1];
            NSArray *hValue = [dictionaryObstacleValues objectForKey:self.key2];
            if ([lValue objectAtIndex:0] == nil || [hValue objectAtIndex:0] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float l = [[lValue objectAtIndex:0] floatValue];
                float h = [[hValue objectAtIndex:0] floatValue];
                clearObstacle = (((h - l)*y)+l);
            }
        }
        
        
        if (temp >= 0 && temp < 50 ) {
            
            float y = ((pressureAltitude - self.pLower)/1000);
            float x = ((temp - self.tempLower)/10);
            NSArray *lowerValue = [dictionaryValues objectForKey:self.key1];
            NSArray *higherValue = [dictionaryValues objectForKey:self.key2];
            if ([lowerValue objectAtIndex:self.a] == nil || [lowerValue objectAtIndex:self.b] == nil || [higherValue objectAtIndex:self.a] == nil || [higherValue objectAtIndex:self.b] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float lower0 = [[lowerValue objectAtIndex:self.a] floatValue];
                float lower1 = [[lowerValue objectAtIndex:self.b] floatValue];
                float lower = (((lower1 - lower0)*x)+lower0);
                float higher0 = [[higherValue objectAtIndex:self.a] floatValue];
                float higher1 = [[higherValue objectAtIndex:self.b] floatValue];
                float higher = (((higher1 - higher0)*x)+higher0);
                groundRoll = (((higher - lower)*y)+lower);
            }
            
            NSArray *lValue = [dictionaryObstacleValues objectForKey:self.key1];
            NSArray *hValue = [dictionaryObstacleValues objectForKey:self.key2];
            if ([lValue objectAtIndex:self.a] == nil || [lValue objectAtIndex:self.b] == nil || [hValue objectAtIndex:self.a] == nil || [hValue objectAtIndex:self.b] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float l0 = [[lValue objectAtIndex:self.a] floatValue];
                float l1 = [[lValue objectAtIndex:self.b] floatValue];
                float l = (((l1 - l0)*x)+l0);
                float h0 = [[hValue objectAtIndex:self.a] floatValue];
                float h1 = [[hValue objectAtIndex:self.b] floatValue];
                float h = (((h1 - h0)*x)+h0);
                clearObstacle = (((h - l)*y)+l);
            }
        }
        if (temp == 50) {
            float y = ((pressureAltitude - self.pLower)/1000);
            NSArray *lowerValue = [dictionaryValues objectForKey:self.key1];
            NSArray *higherValue = [dictionaryValues objectForKey:self.key2];
            if ([lowerValue objectAtIndex:self.a] == nil || [higherValue objectAtIndex:self.a]){
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float lower = [[lowerValue objectAtIndex:self.a] floatValue];
                float higher = [[higherValue objectAtIndex:self.a] floatValue];
                groundRoll = (((higher - lower)*y)+lower);
            }
            
            
            NSArray *lValue = [dictionaryObstacleValues objectForKey:self.key1];
            NSArray *hValue = [dictionaryObstacleValues objectForKey:self.key2];
            if ([lValue objectAtIndex:self.a] == nil || [hValue objectAtIndex:self.a] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float l = [[lValue objectAtIndex:self.a] floatValue];
                float h = [[hValue objectAtIndex:self.a] floatValue];
                clearObstacle = (((h - l)*y)+l);
            }
            
        }
        if (temp > 50) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data of 50 degrees Celsius" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
            [alertView show];
        }
    }
    if (pressureAltitude == 10000) {
        if (temp < 0) {
            NSArray *lowerValue = [dictionaryValues objectForKey:self.key1];
            if ([lowerValue objectAtIndex:self.a] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float lower = [[lowerValue objectAtIndex:self.a] floatValue];
                groundRoll = lower;
            }
            
            
            NSArray *lValue = [dictionaryObstacleValues objectForKey:self.key1];
            if ([lValue objectAtIndex:self.a] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float l = [[lValue objectAtIndex:self.a] floatValue];
                clearObstacle = l;
            }
        }
        if (temp >= 0 && temp < 50) {
            float x = ((temp - self.tempLower)/10);
            NSArray *lowerValue = [dictionaryValues objectForKey:self.key1];
            if ([lowerValue objectAtIndex:self.a] == nil || [lowerValue objectAtIndex:self.b] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float lower0 = [[lowerValue objectAtIndex:self.a] floatValue];
                float lower1 = [[lowerValue objectAtIndex:self.b] floatValue];
                float lower = (((lower1 - lower0)*x)+lower0);
                groundRoll = lower;
            }
            
            
            NSArray *lValue = [dictionaryObstacleValues objectForKey:self.key1];
            if ([lValue objectAtIndex:self.a] == nil || [lValue objectAtIndex:self.b] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float l0 = [[lValue objectAtIndex:self.a] floatValue];
                float l1 = [[lValue objectAtIndex:self.b] floatValue];
                float l = (((l1 - l0)*x)+l0);
                clearObstacle = l;
            }
        }
        if (temp == 50) {
            NSArray *lowerValue = [dictionaryValues objectForKey:self.key1];
            if ([lowerValue objectAtIndex:self.a] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float lower = [[lowerValue objectAtIndex:self.a] floatValue];
                groundRoll = lower;
            }
            
            NSArray *lValue = [dictionaryObstacleValues objectForKey:self.key1];
            if ([lValue objectAtIndex:self.a] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float l = [[lValue objectAtIndex:self.a] floatValue];
                clearObstacle = l;
            }
        }
        if (temp > 50) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data of 50 degrees Celsius" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
            [alertView show];
        }
    }
    if (pressureAltitude > 10000) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Pressure Exceeds POH Data of 10,000 Feet" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
        [alertView show];
    }
    
    if (weight < 2900) {
        self.groundroll = groundRoll;
        self.clearObstacle = clearObstacle;
    }
    if (weight == 2900) {
        self.groundroll2500 = groundRoll;
        self.clearObstacle2500 = clearObstacle;
    }
    else {
        self.groundroll3000 = groundRoll;
        self.clearObstacle3000 = clearObstacle;
    }
    
    [self.view endEditing:YES];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"landingData"]) {
        CDCirrusSR22TM3600LandingDataViewController *controller = segue.destinationViewController;
        controller.nNumberText = self.nNumberText;
        controller.emptyWeightText = self.emptyWeightText;
        controller.pilotWeightText = self.pilotWeightText;
        controller.frontSeatWeightText = self.frontSeatWeightText;
        controller.rearSeat1WeightText = self.rearSeat1WeightText;
        controller.rearSeat2WeightText = self.rearSeat2WeightText;
        controller.bagAreaWeightText = self.bagAreaWeightText;
        controller.fuelGalText = self.fuelGalText;
        controller.fuelWeightText = self.fuelWeightText;
        controller.taxiBurnWeightText = self.taxiBurnWeightText;
        controller.totalWeightText = self.totalWeightLabel.text;
        controller.emptyArmText = self.emptyArmText;
        controller.pilotArmText = self.pilotArmText;
        controller.frontSeatArmText = self.frontSeatArmText;
        controller.rearSeat1ArmText = self.rearSeat1ArmText;
        controller.rearSeat2ArmText = self.rearSeat2ArmText;
        controller.bagAreaArmText = self.bagAreaArmText;
        controller.fuelArmText = self.fuelArmText;
        controller.taxiBurnArmText = self.taxiBurnArmText;
        controller.totalArmText = self.totalArmText;
        controller.emptyMomentText = self.emptyMomentText;
        controller.pilotMomentText = self.pilotMomentText;
        controller.frontSeatMomentText = self.frontSeatMomentText;
        controller.rearSeat1MomentText = self.rearSeat1MomentText;
        controller.rearSeat2MomentText = self.rearSeat2MomentText;
        controller.bagAreaMomentText = self.bagAreaMomentText;
        controller.fuelMomentText = self.fuelMomentText;
        controller.taxiBurnMomentText = self.taxiBurnMomentText;
        controller.totalMomentText = self.totalMomentText;
        controller.takeoffCGLimitsText = self.takeoffCGLimitsText;
        controller.takeoffManeuveringSpeedText = self.takeoffManeuveringSpeedText;
        controller.takeoffFieldElevation = self.fieldElevationTextField.text;
        controller.takeoffPressure = self.pressureTextField.text;
        controller.takeoffTemperature = self.temperatureTextField.text;
        controller.takeoffWindDirection = self.windDirectionTextField.text;
        controller.takeoffWindSpeed = self.windSpeedTextField.text;
        controller.takeoffRunway = self.runwayTextField.text;
        controller.takeoffGroundRoll = self.takeoffGroundRollLabel.text;
        controller.takeoffClearObstacle = self.takeoffClearObstacleLabel.text;
        controller.takeoffDensityAltitude = self.densityAltitudeLabel.text;
        controller.takeoffHeadWindComponent = self.headwindComponent;
        controller.takeoffCrosswindComponent = self.takeoffCrossWindComponent;
        
    }
}




@end
