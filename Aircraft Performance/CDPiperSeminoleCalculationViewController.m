//
//  CDPiperSeminoleCalculationViewController.m
//  Aircraft Performance
//
//  Created by Chip Duffield on 10/9/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import "CDPiperSeminoleCalculationViewController.h"
#import "CDPiperSeminoleTakeoffDataViewController.h"
#import "CDPiperSeminoleTakeoffGraphViewController.h"

@interface CDPiperSeminoleCalculationViewController ()

@end

@implementation CDPiperSeminoleCalculationViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.automaticallyAdjustsScrollViewInsets = false;
    
    self.emptyWeightLabel.text = self.emptyWeightText;
    self.emptyArmLabel.text = self.emptyArmText;
    self.pilotWeightLabel.text = self.pilotWeightText;
    self.frontSeatWeightLabel.text = self.frontSeatWeightText;
    self.rearSeat1WeightLabel.text = self.rearSeat1WeightText;
    self.rearSeat2WeightLabel.text = self.rearSeat2WeightText;
    self.bagAreaWeightLabel.text = self.bagAreaWeightText;
    self.fuelGalLabel.text = self.fuelGalText;
    
    //transfer label text to floats for weights
    float empty = [self.emptyWeightLabel.text floatValue];
    float pilot = [self.pilotWeightLabel.text floatValue];
    float frontseat = [self.frontSeatWeightLabel.text floatValue];
    float rearseatL = [self.rearSeat1WeightLabel.text floatValue];
    float rearseatR = [self.rearSeat2WeightLabel.text floatValue];
    float baggage = [self.bagAreaWeightLabel.text floatValue];
    float fuelgal = [self.fuelGalLabel.text floatValue];
    float fuelweight = fuelgal * 6;
    float taxiBurn = [self.taxiBurnLabel.text floatValue];
    float totalWeight = empty + pilot + frontseat + rearseatL + rearseatR + baggage + fuelweight + taxiBurn;
    
    //transfer label text to floats for arms
    float emptyA = [self.emptyArmLabel.text floatValue];
    float pilotA = [self.pilotArmLabel.text floatValue];
    float frontseatA = [self.frontSeatArmLabel.text floatValue];
    float rearseatLA = [self.rearSeat1ArmLabel.text floatValue];
    float rearseatRA = [self.rearSeat2ArmLabel.text floatValue];
    float baggageA = [self.bagAreaArmLabel.text floatValue];
    float fuelA = [self.fuelArmLabel.text floatValue];
    
    //calculate moments
    float emptyM = ((empty * emptyA)/1000);
    float pilotM = ((pilot * pilotA)/1000);
    float frontseatM = ((frontseat * frontseatA)/1000);
    float rearseatLM = ((rearseatL * rearseatLA)/1000);
    float rearseatRM = ((rearseatR * rearseatRA)/1000);
    float baggageM = ((baggage * baggageA)/1000);
    float fuelM = ((fuelweight * fuelA)/1000);
    float taxiBurnM = [self.taxiBurnMomentLabel.text floatValue];
    float totalM = emptyM + pilotM + frontseatM + rearseatLM + rearseatRM + baggage + fuelM + taxiBurnM;
    
    //calculate CG
    float totalA = ((totalM*1000)/totalWeight);
    
    //set total weight lable
    self.fuelWeightLabel.text = [[NSString alloc] initWithFormat:@"%.f",fuelweight];
    
    //set moment text fields
    self.emptyMomentLabel.text = [[NSString alloc] initWithFormat:@"%.02f",emptyM];
    self.pilotMomentLabel.text = [[NSString alloc] initWithFormat:@"%.02f",pilotM];
    self.frontSeatMomentLabel.text = [[NSString alloc] initWithFormat:@"%.02f",frontseatM];
    self.rearSeat1MomentLabel.text = [[NSString alloc] initWithFormat:@"%.02f",rearseatLM];
    self.rearSeat2MomentLabel.text = [[NSString alloc] initWithFormat:@"%.02f",rearseatRM];
    self.bagAreaMomentLabel.text = [[NSString alloc] initWithFormat:@"%.02f",baggageM];
    self.fuelMomentLabel.text = [[NSString alloc] initWithFormat:@"%.02f",fuelM];
    
    
    //set total fields
    self.totalWeightLabel.text = [[NSString alloc] initWithFormat:@"%.f",totalWeight];
    self.totalArmLabel.text = [[NSString alloc] initWithFormat:@"%.1f",totalA];
    self.totalMomentLabel.text = [[NSString alloc] initWithFormat:@"%.02f",totalM];
    
    //Max weight calculations
    float aircraftMaxWeight = 3800.0;
    if (totalA < 85.0) {
        self.m = 600;
        self.b = 47600;
    } else if (totalA >= 85.0 && totalA < 88.0) {
        self.m = 100;
        self.b = 5100;
    } else {
        self.m = 0;
        self.b = 0;
    }

    float (^maxWeightAtCG)(float,float,float) = ^(float totalA, float m, float b) {
        if (totalA <88.0) {
            float weight = ((m * totalA) - b);
            return weight;
        }
        else {
            float weight = 3800.0;
            return weight;
        }
        
    };
    
    float maxWeight = maxWeightAtCG(totalA, self.m, self.b);
    
    // Maneuvering Speed Calculation
    float manSpeedMax = 111;
    float manSpeed = manSpeedMax * sqrtf(totalWeight/aircraftMaxWeight);
    self.maneuveringSpeedLabel.text = [[NSString alloc] initWithFormat:@"%.f",manSpeed];
    
    //set cg response
    NSString *cglimits;
    if (totalA >= 83 && totalA <= 93) {
        if (totalWeight <= maxWeight) {
            cglimits = @"Within Limits";
        }
        else if (totalWeight > aircraftMaxWeight) {
            float overWeight = totalWeight - aircraftMaxWeight;
            cglimits = [[NSString alloc] initWithFormat:@"Overweight by %.f lbs",overWeight];
        }
        else {
            cglimits = @"Outside of Limits";
        }
    }
    else {
        cglimits = @"Outside of Limits";
    }
    self.CGLimitsLabel.text = cglimits;
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"takeoffData"]) {
        CDPiperSeminoleTakeoffDataViewController *controller = segue.destinationViewController;
        controller.nNumberText = self.nNumberText;
        controller.emptyWeightText = self.emptyWeightText;
        controller.pilotWeightText = self.pilotWeightText;
        controller.frontSeatWeightText = self.frontSeatWeightText;
        controller.rearSeat1WeightText = self.rearSeat1WeightText;
        controller.rearSeat2WeightText = self.rearSeat2WeightText;
        controller.bagAreaWeightText = self.bagAreaWeightText;
        controller.fuelGalText = self.fuelGalText;
        controller.fuelWeightText = self.fuelWeightLabel.text;
        controller.totalWeightText = self.totalWeightLabel.text;
        controller.emptyArmText = self.emptyArmText;
        controller.pilotArmText = self.pilotArmLabel.text;
        controller.frontSeatArmText = self.frontSeatArmLabel.text;
        controller.rearSeat1ArmText = self.rearSeat1ArmLabel.text;
        controller.rearSeat2ArmText = self.rearSeat2ArmLabel.text;
        controller.bagAreaArmText = self.bagAreaArmLabel.text;
        controller.fuelArmText = self.fuelArmLabel.text;
        controller.totalArmText = self.totalArmLabel.text;
        controller.emptyMomentText = self.emptyMomentLabel.text;
        controller.pilotMomentText = self.pilotMomentLabel.text;
        controller.frontSeatMomentText = self.frontSeatMomentLabel.text;
        controller.rearSeat1MomentText = self.rearSeat1MomentLabel.text;
        controller.rearSeat2MomentText = self.rearSeat2MomentLabel.text;
        controller.bagAreaMomentText = self.bagAreaMomentLabel.text;
        controller.fuelMomentText = self.fuelMomentLabel.text;
        controller.totalMomentText = self.totalMomentLabel.text;
        controller.takeoffCGLimitsText = self.CGLimitsLabel.text;
        controller.takeoffManeuveringSpeedText = self.maneuveringSpeedLabel.text;
        controller.taxiBurnWeightText = self.taxiBurnLabel.text;
        controller.taxiBurnArmText = self.taxiBurnArmLabel.text;
        controller.taxiBurnMomentText = self.taxiBurnMomentLabel.text;
    }
    
    
    if ([segue.identifier isEqualToString:@"takeoffGraph"]) {
        CDPiperSeminoleTakeoffGraphViewController *controller = (CDPiperSeminoleTakeoffGraphViewController *)segue.destinationViewController;
        controller.totalWeight = self.totalWeightLabel.text;
        controller.totalArm = self.totalArmLabel.text;
        controller.cg = self.CGLimitsLabel.text;
    }
    
}


@end
