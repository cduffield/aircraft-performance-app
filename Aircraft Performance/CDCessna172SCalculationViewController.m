//
//  CDCessna172SCalculationViewController.m
//  Aircraft Performance
//
//  Created by Chip Duffield on 8/16/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import "CDCessna172SCalculationViewController.h"
#import "CDCessna172STakeoffDataViewController.h"
#import "CDCessna172STakeoffGraphViewController.h"

@interface CDCessna172SCalculationViewController ()

@end

@implementation CDCessna172SCalculationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = false;
    //self.view.backgroundColor = [UIColor colorWithRed:(126/255.0) green:(194/255.0) blue:(241/255.0) alpha:1.0];
    
    self.emptyWeightContent.text = self.emptyWeight;
    if ([self.pilotWeight isEqualToString:@""]) {
        self.pilotWeightContent.text = @"0";
    } else {
        self.pilotWeightContent.text = self.pilotWeight;
    }
    if ([self.frontSeatWeight isEqualToString:@""]) {
        self.frontSeatWeightContent.text = @"0";
    } else {
        self.frontSeatWeightContent.text = self.frontSeatWeight;
    }
    if ([self.rearSeat1Weight isEqualToString:@""]) {
        self.rearSeat1WeightContent.text = @"0";
    } else {
        self.rearSeat1WeightContent.text = self.rearSeat1Weight;
    }
    if ([self.rearSeat2Weight isEqualToString:@""]) {
        self.rearSeat2WeightContent.text = @"0";
    } else {
        self.rearSeat2WeightContent.text = self.rearSeat2Weight;
    }
    if ([self.bagArea1Weight isEqualToString:@""]) {
        self.bagArea1Content.text = @"0";
    } else {
        self.bagArea1Content.text = self.bagArea1Weight;
    }
    if ([self.bagArea2Weight isEqualToString:@""]) {
        self.bagArea2Content.text = @"0";
    } else {
        self.bagArea2Content.text = self.bagArea2Weight;
    }
    if ([self.fuelGal isEqualToString:@""]) {
        self.fuelGalContent.text = @"0";
    } else {
        self.fuelGalContent.text = self.fuelGal;
    }
    self.emptyArmContent.text = self.arm;
    
    //transfer label text to floats for weights
    float empty = [self.emptyWeightContent.text floatValue];
    float pilot = [self.pilotWeightContent.text floatValue];
    float frontseat = [self.frontSeatWeightContent.text floatValue];
    float rearseat1 = [self.rearSeat1WeightContent.text floatValue];
    float rearseat2 = [self.rearSeat2WeightContent.text floatValue];
    float bagarea1 = [self.bagArea1Content.text floatValue];
    float bagarea2 = [self.bagArea2Content.text floatValue];
    float fuelgal = [self.fuelGalContent.text floatValue];
    float fuelweight = fuelgal * 6;
    float taxiBurn = [self.taxiBurnWeightContent.text floatValue];
    float totalWeight = empty + pilot + frontseat + rearseat1 + rearseat2 + bagarea1 + bagarea2 + fuelweight + taxiBurn;
    
    //transfer label text to floats for arms
    float pilotA = [self.pilotArmContent.text floatValue];
    float frontseatA = [self.frontSeatArmContent.text floatValue];
    float rearseat1A = [self.rearSeat1ArmContent.text floatValue];
    float rearseat2A = [self.rearSeat2ArmContent.text floatValue];
    float bagarea1A = [self.bagArea1ArmContent.text floatValue];
    float bagarea2A = [self.bagArea2ArmContent.text floatValue];
    float fuelA = [self.fuelArmContent.text floatValue];
    float emptyA = [self.emptyArmContent.text floatValue];
    
    //calculate moments
    float emptyM = ((empty * emptyA)/1000);
    float pilotM = ((pilot * pilotA)/1000);
    float frontseatM = ((frontseat * frontseatA)/1000);
    float rearseat1M = ((rearseat1 * rearseat1A)/1000);
    float rearseat2M = ((rearseat2 * rearseat2A)/1000);
    float bagarea1M = ((bagarea1 * bagarea1A)/1000);
    float bagarea2M = ((bagarea2 * bagarea2A)/1000);
    float fuelM = ((fuelweight * fuelA)/1000);
    float taxiBurnM = [self.taxiBurnMomentContent.text floatValue];
    float totalM = emptyM + pilotM + frontseatM + rearseat1M + rearseat2M + bagarea1M + bagarea2M + fuelM + taxiBurnM;
    
    //calculate CG
    float totalA = ((totalM*1000)/totalWeight);
    
    //set total weight lable
    self.fuelWeightContent.text = [[NSString alloc] initWithFormat:@"%.f",fuelweight];
    
    //set moment text fields
    self.emtpyMomentContent.text = [[NSString alloc] initWithFormat:@"%.02f",emptyM];
    self.pilotMomentContent.text = [[NSString alloc] initWithFormat:@"%.02f",pilotM];
    self.frontSeatMomentContent.text = [[NSString alloc] initWithFormat:@"%.02f",frontseatM];
    self.rearSeat1MomentContent.text = [[NSString alloc] initWithFormat:@"%.02f",rearseat1M];
    self.rearSeat2MomentContent.text = [[NSString alloc] initWithFormat:@"%.02f",rearseat2M];
    self.bagArea1MomentContent.text = [[NSString alloc] initWithFormat:@"%.02f",bagarea1M];
    self.bagArea2MomentContent.text = [[NSString alloc] initWithFormat:@"%.02f",bagarea2M];
    self.fuelMomentContent.text = [[NSString alloc] initWithFormat:@"%.02f",fuelM];
    
    
    //set total fields
    self.totalWeightContent.text = [[NSString alloc] initWithFormat:@"%.f",totalWeight];
    self.totalWeightText = [[NSString alloc] initWithFormat:@"%.f",totalWeight];
    self.totalArmContent.text = [[NSString alloc] initWithFormat:@"%.1f",totalA];
    self.totalMomentContent.text = [[NSString alloc] initWithFormat:@"%.02f",totalM];
    
    //Max weight calculations
    float aircraftMaxWeight = 2550.0;
    float m = 120;
    float b = 2250;
    float (^maxWeightAtCG)(float,float,float) = ^(float totalA, float m, float b) {
        if (totalA <40) {
            float weight = ((m * totalA) - b);
            return weight;
        }
        else {
            float weight = 2550.0;
            return weight;
        }
        
    };
    float maxWeight = maxWeightAtCG(totalA, m, b);
    
    //manuevering speed calculation
    float manSpeedMax = 102;
    float manSpeed = manSpeedMax * sqrtf(totalWeight/aircraftMaxWeight);
    self.maneuveringSpeedContent.text = [[NSString alloc] initWithFormat:@"%.f",manSpeed];
    
    //set cg response
    if (totalA >= 35 && totalA <= 47.3) {
        if (totalWeight <= maxWeight) {
            self.cgLimits = @"Within Limits";
        }
        else if (totalWeight > aircraftMaxWeight) {
            float overWeight = totalWeight - aircraftMaxWeight;
            self.cgLimits = [[NSString alloc] initWithFormat:@"Overweight by %.f lbs",overWeight];
        }
        else {
            self.cgLimits = @"Outside of Limits";
        }
    }
    else {
        self.cgLimits = @"Outside of Limits";
    }
    self.cgLabelContent.text = self.cgLimits;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if ([identifier isEqualToString:@"cessna172RTakeoff"]) {
        if ([self.cgLimits  isEqual: @"Too Heavy"]) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Overweight" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
            [alertView show];
            
            return NO;
        }
        if ([self.cgLimits isEqualToString: @"Outside of Limits"]) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Outside of Limits" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
            [alertView show];
            
            return NO;
        }
    }
    
    return YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"takeoffData"]) {
        CDCessna172STakeoffDataViewController *controller = (CDCessna172STakeoffDataViewController *)segue.destinationViewController;
        controller.totalWeight = self.totalWeightText;
        controller.fuelGallons = self.fuelGal;
        controller.totalMoment = self.totalMomentContent.text;
        controller.fuelArm= self.fuelArmContent.text;
        controller.pilotWeight = self.pilotWeightContent.text;
        controller.frontSeatWeight = self.frontSeatWeightContent.text;
        controller.rearSeat1 = self.rearSeat1WeightContent.text;
        controller.rearSeat2 = self.rearSeat2WeightContent.text;
        controller.bagArea1 = self.bagArea1Content.text;
        controller.bagArea2 = self.bagArea2Content.text;
        controller.emptyWeight = self.emptyWeight;
        controller.emtpyArm = self.emptyArmContent.text;
        controller.totalArm = self.totalArmContent.text;
        controller.takeoffCG = self.cgLabelContent.text;
        controller.nNumber = self.nNumber;
        controller.toManeuveringSpeed = self.maneuveringSpeedContent.text;
    }
    
    if ([segue.identifier isEqualToString:@"takeoffGraph"]) {
        CDCessna172STakeoffGraphViewController *controller = (CDCessna172STakeoffGraphViewController *)segue.destinationViewController;
        controller.totalWeight = self.totalWeightText;
        controller.totalArm = self.totalArmContent.text;
        controller.cg = self.cgLimits;
    }
    
}


@end
