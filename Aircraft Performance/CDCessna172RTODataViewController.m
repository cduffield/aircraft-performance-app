//
//  CDCessna172RTODataViewController.m
//  Aircraft Performance
//
//  Created by Chip Duffield on 7/3/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import "CDCessna172RTODataViewController.h"
#import "CDCessna172RLandingDataViewController.h"

@interface CDCessna172RTODataViewController ()

@end

@implementation CDCessna172RTODataViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = false;
    
    
    //set total weight
    self.totalWeightLabel.text = self.totalWeight;
    
    
    NSArray *psealevel = [[NSArray alloc] initWithObjects:@845.0,@910.0,@980.0,@1055.0,@1135.0,nil];
    NSArray *p1000 = [[NSArray alloc] initWithObjects:@925.0,@1000.0,@1075.0,@1160.0,@1245.0,nil];
    NSArray *p2000 = [[NSArray alloc] initWithObjects:@1015.0,@1095.0,@1185.0,@1275.0,@1365.0,nil];
    NSArray *p3000 = [[NSArray alloc] initWithObjects:@1115.0,@1205.0,@1305.0,@1400.0,@1505.0, nil];
    NSArray *p4000 = [[NSArray alloc] initWithObjects:@1230.0,@1330.0,@1435.0,@1545.0,@1655.0, nil];
    NSArray *p5000 = [[NSArray alloc] initWithObjects:@1355.0,@1470.0,@1585.0,@1705.0,@1830.0, nil];
    NSArray *p6000 = [[NSArray alloc] initWithObjects:@1500.0,@1625.0,@1750.0,@1880.0,@2020.0, nil];
    NSArray *p7000 = [[NSArray alloc] initWithObjects:@1660.0,@1795.0,@1935.0,@2085.0,@2240.0, nil];
    NSArray *p8000 = [[NSArray alloc] initWithObjects:@1840.0,@1995.0,@2150.0,@2315.0,nil, nil];
    self.toDistance = [[NSDictionary alloc] initWithObjectsAndKeys:psealevel,@0,p1000,@1000,p2000,@2000,p3000,@3000,p4000,@4000,p5000,@5000,p6000,@6000,p7000,@7000,p8000,@8000, nil];
    
    NSArray *psealevelat50 = [[NSArray alloc] initWithObjects:@1510.0,@1625.0,@1745.0,@1875.0,@2015.0,nil];
    NSArray *p1000at50 = [[NSArray alloc] initWithObjects:@1660.0,@1790.0,@1925.0,@2070.0,@2220.0,nil];
    NSArray *p2000at50 = [[NSArray alloc] initWithObjects:@1830.0,@1970.0,@2125.0,@2290.0,@2455.0,nil];
    NSArray *p3000at50 = [[NSArray alloc] initWithObjects:@2020.0,@2185.0,@2360.0,@2540.0,@2730.0,nil];
    NSArray *p4000at50 = [[NSArray alloc] initWithObjects:@2245.0,@2430.0,@2630.0,@2830.0,@3045.0,nil];
    NSArray *p5000at50 = [[NSArray alloc] initWithObjects:@2500.0,@2715.0,@2945.0,@3175.0,@3430.0,nil];
    NSArray *p6000at50 = [[NSArray alloc] initWithObjects:@2805.0,@3060.0,@3315.0,@3590.0,@3895.0,nil];
    NSArray *p7000at50 = [[NSArray alloc] initWithObjects:@3170.0,@3470,0,@3770.0,@4105.0,@4485.0,nil];
    NSArray *p8000at50 = [[NSArray alloc] initWithObjects:@3620.0,@3975.0,@4345.0,@4775.0,nil,nil];
    self.toDistanceObstacle = [[NSDictionary alloc] initWithObjectsAndKeys:psealevelat50,@0,p1000at50,@1000,p2000at50,@2000,p3000at50,@3000,p4000at50,@4000,p5000at50,@5000,p6000at50,@6000,p7000at50,@7000,p8000at50,@8000, nil];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)calculate:(id)sender {
    
    float windDirection = [self.windDirectionTextField.text floatValue];
    float windSpeed = [self.windSpeedTextField.text floatValue];
    float runwayDirection = ([self.runwayTextField.text floatValue]*10);
    
    if (runwayDirection > 360) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please enter runway without last Digit. Use 9 instead of 090" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
        [alertView show];
        
    } else if (windDirection > 360) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error'" message:@"Wind Direction can not be greater that 360" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
        [alertView show];
    } else {
        
        //constants for both
        float pressure = [self.pressureTextField.text floatValue];
        float temp = [self.temperatureTextField.text floatValue];
        float fieldElevation = [self.fieldElevationTextField.text floatValue];
        float pressureAltitude = (((29.92 - pressure)*1000) + fieldElevation);
        float isaTemp = (15 -((pressureAltitude/1000) * 1.98));
        
        
        float groundRollCorrectedForWind;
        float clearObstacleCorrectedForWind;
        
        //calculate density altitude
        float densityAltitude = (pressureAltitude + (118.8 * (temp - isaTemp)));
        self.densityAltitudeLabel.text = [[NSString alloc] initWithFormat:@"%.f",densityAltitude];
        
        //calculate ground roll
        
        
        [self calculateTempIndex:temp];
        [self pressureAltitudeKeys:pressureAltitude];
        
        if (pressureAltitude < 0) {
            if (temp < 0) {
                NSArray *lowerValue = [self.toDistance objectForKey:self.key1];
                if ([lowerValue objectAtIndex:self.a] == nil) {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                    [alertView show];
                } else {
                    float lower = [[lowerValue objectAtIndex:self.a] floatValue];
                    self.groundroll = lower;
                }
                
                
                NSArray *lValue = [self.toDistanceObstacle objectForKey:self.key1];
                if ([lValue objectAtIndex:self.a] == nil) {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                    [alertView show];
                } else {
                    float l = [[lValue objectAtIndex:self.a] floatValue];
                    self.clearObstacle = l;
                }
            }
            if (temp >= 0 && temp < 40) {
                float x = ((temp - self.tempLower)/10);
                NSArray *lowerValue = [self.toDistance objectForKey:self.key1];
                if ([lowerValue objectAtIndex:self.a] == nil || [lowerValue objectAtIndex:self.b] == nil) {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                    [alertView show];
                } else {
                    float lower0 = [[lowerValue objectAtIndex:self.a] floatValue];
                    float lower1 = [[lowerValue objectAtIndex:self.b] floatValue];
                    float lower = (((lower1 - lower0)*x)+lower0);
                    self.groundroll = lower;
                }
                
                
                NSArray *lValue = [self.toDistanceObstacle objectForKey:self.key1];
                if ([lValue objectAtIndex:self.a] == nil || [lValue objectAtIndex:self.b] == nil) {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                    [alertView show];
                } else {
                    float l0 = [[lValue objectAtIndex:self.a] floatValue];
                    float l1 = [[lValue objectAtIndex:self.b] floatValue];
                    float l = (((l1 - l0)*x)+l0);
                    self.clearObstacle = l;
                }
               
            }
            if (temp == 40) {
                NSArray *lowerValue = [self.toDistance objectForKey:self.key1];
                if ([lowerValue objectAtIndex:self.a] == nil) {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                    [alertView show];
                } else {
                    float lower = [[lowerValue objectAtIndex:self.a] floatValue];
                    self.groundroll = lower;
                }
                
                NSArray *lValue = [self.toDistanceObstacle objectForKey:self.key1];
                if ([lValue objectAtIndex:self.a] == nil) {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                    [alertView show];
                } else {
                    float l = [[lValue objectAtIndex:self.a] floatValue];
                    self.clearObstacle = l;
                }
            }
            if (temp > 40) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data of 40 degrees Celsius" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            }
        }
      
        if (pressureAltitude >= 0 && pressureAltitude < 8000) {
            if (temp < 0 ) {
                float y = ((pressureAltitude - self.pLower)/1000);
                NSArray *lowerValue = [self.toDistance objectForKey:self.key1];
                NSArray *higherValue = [self.toDistance objectForKey:self.key2];
                if ([lowerValue objectAtIndex:0] == nil || [higherValue objectAtIndex:0] == nil) {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                    [alertView show];
                } else {
                    float lower = [[lowerValue objectAtIndex:0] floatValue];
                    float higher = [[higherValue objectAtIndex:0] floatValue];
                    self.groundroll = (((higher - lower)*y)+lower);
                }
                
                
                NSArray *lValue = [self.toDistanceObstacle objectForKey:self.key1];
                NSArray *hValue = [self.toDistanceObstacle objectForKey:self.key2];
                if ([lValue objectAtIndex:0] == nil || [hValue objectAtIndex:0] == nil) {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                    [alertView show];
                } else {
                    float l = [[lValue objectAtIndex:0] floatValue];
                    float h = [[hValue objectAtIndex:0] floatValue];
                    self.clearObstacle = (((h - l)*y)+l);
                }
            }
            
            
            if (temp >= 0 && temp < 40 ) {
                
                float y = ((pressureAltitude - self.pLower)/1000);
                float x = ((temp - self.tempLower)/10);
                NSArray *lowerValue = [self.toDistance objectForKey:self.key1];
                NSArray *higherValue = [self.toDistance objectForKey:self.key2];
                if ([lowerValue objectAtIndex:self.a] == nil || [lowerValue objectAtIndex:self.b] == nil || [higherValue objectAtIndex:self.a] == nil || [higherValue objectAtIndex:self.b] == nil) {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                    [alertView show];
                } else {
                    float lower0 = [[lowerValue objectAtIndex:self.a] floatValue];
                    float lower1 = [[lowerValue objectAtIndex:self.b] floatValue];
                    float lower = (((lower1 - lower0)*x)+lower0);
                    float higher0 = [[higherValue objectAtIndex:self.a] floatValue];
                    float higher1 = [[higherValue objectAtIndex:self.b] floatValue];
                    float higher = (((higher1 - higher0)*x)+higher0);
                    self.groundroll = (((higher - lower)*y)+lower);
                }
                
                NSArray *lValue = [self.toDistanceObstacle objectForKey:self.key1];
                NSArray *hValue = [self.toDistanceObstacle objectForKey:self.key2];
                if ([lValue objectAtIndex:self.a] == nil || [lValue objectAtIndex:self.b] == nil || [hValue objectAtIndex:self.a] == nil || [hValue objectAtIndex:self.b] == nil) {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                    [alertView show];
                } else {
                    float l0 = [[lValue objectAtIndex:self.a] floatValue];
                    float l1 = [[lValue objectAtIndex:self.b] floatValue];
                    float l = (((l1 - l0)*x)+l0);
                    float h0 = [[hValue objectAtIndex:self.a] floatValue];
                    float h1 = [[hValue objectAtIndex:self.b] floatValue];
                    float h = (((h1 - h0)*x)+h0);
                    self.clearObstacle = (((h - l)*y)+l);
                }
            }
            if (temp == 40) {
                float y = ((pressureAltitude - self.pLower)/1000);
                NSArray *lowerValue = [self.toDistance objectForKey:self.key1];
                NSArray *higherValue = [self.toDistance objectForKey:self.key2];
                if ([lowerValue objectAtIndex:self.a] == nil || [higherValue objectAtIndex:self.a]){
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                    [alertView show];
                } else {
                    float lower = [[lowerValue objectAtIndex:self.a] floatValue];
                    float higher = [[higherValue objectAtIndex:self.a] floatValue];
                    self.groundroll = (((higher - lower)*y)+lower);
                }
                
                
                NSArray *lValue = [self.toDistanceObstacle objectForKey:self.key1];
                
                NSArray *hValue = [self.toDistanceObstacle objectForKey:self.key2];
                if ([lValue objectAtIndex:self.a] == nil || [hValue objectAtIndex:self.a] == nil) {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                    [alertView show];
                } else {
                    float l = [[lValue objectAtIndex:self.a] floatValue];
                    float h = [[hValue objectAtIndex:self.a] floatValue];
                    self.clearObstacle = (((h - l)*y)+l);
                }
                
            }
            if (temp > 40) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data of 40 degrees Celsius" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            }
        }
        if (pressureAltitude == 8000) {
            if (temp < 0) {
                NSArray *lowerValue = [self.toDistance objectForKey:self.key1];
                if ([lowerValue objectAtIndex:self.a] == nil) {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                    [alertView show];
                } else {
                    float lower = [[lowerValue objectAtIndex:self.a] floatValue];
                    self.groundroll = lower;
                }
                
                
                NSArray *lValue = [self.toDistanceObstacle objectForKey:self.key1];
                if ([lValue objectAtIndex:self.a] == nil) {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                    [alertView show];
                } else {
                    float l = [[lValue objectAtIndex:self.a] floatValue];
                    self.clearObstacle = l;
                }
            }
            if (temp >= 0 && temp < 40) {
                float x = ((temp - self.tempLower)/10);
                NSArray *lowerValue = [self.toDistance objectForKey:self.key1];
                if ([lowerValue objectAtIndex:self.a] == nil || [lowerValue objectAtIndex:self.b] == nil) {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                    [alertView show];
                } else {
                    float lower0 = [[lowerValue objectAtIndex:self.a] floatValue];
                    float lower1 = [[lowerValue objectAtIndex:self.b] floatValue];
                    float lower = (((lower1 - lower0)*x)+lower0);
                    self.groundroll = lower;
                }
                
                
                NSArray *lValue = [self.toDistanceObstacle objectForKey:self.key1];
                if ([lValue objectAtIndex:self.a] == nil || [lValue objectAtIndex:self.b] == nil) {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                    [alertView show];
                } else {
                    float l0 = [[lValue objectAtIndex:self.a] floatValue];
                    float l1 = [[lValue objectAtIndex:self.b] floatValue];
                    float l = (((l1 - l0)*x)+l0);
                    self.clearObstacle = l;
                }
            }
            if (temp == 40) {
                NSArray *lowerValue = [self.toDistance objectForKey:self.key1];
                if ([lowerValue objectAtIndex:self.a] == nil) {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                    [alertView show];
                } else {
                    float lower = [[lowerValue objectAtIndex:self.a] floatValue];
                    self.groundroll = lower;
                }
                
                NSArray *lValue = [self.toDistanceObstacle objectForKey:self.key1];
                if ([lValue objectAtIndex:self.a] == nil) {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                    [alertView show];
                } else {
                    float l = [[lValue objectAtIndex:self.a] floatValue];
                    self.clearObstacle = l;
                }
            }
            if (temp > 40) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data of 40 degrees Celsius" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            }
        }
        if (pressureAltitude > 8000) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Pressure Exceeds POH Data of 8,000 Feet" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
            [alertView show];
        }
        
        if ((windDirection - runwayDirection) <= 90) {
            float angle = windDirection - runwayDirection;
            float windComponent = cosf(angle*M_PI/180);
            float headwind = windSpeed * windComponent;
            float crosswindComponent = sinf(angle*M_PI/180);
            float crosswind = windSpeed * crosswindComponent;
            float crosswindAbs = fabsf(crosswind);
            self.takeoffCrossWindComponent = [[NSString alloc] initWithFormat:@"%.f",crosswindAbs];
            self.headwindCompoent = [[NSString alloc] initWithFormat:@"%.f",headwind];
            int headwindInt = (int)floorf(headwind);
            int headwindCorrection = ((headwindInt/9)*.1);
            groundRollCorrectedForWind = self.groundroll * (1 - headwindCorrection);
            clearObstacleCorrectedForWind = self.clearObstacle * (1- headwindCorrection);
            self.takeoffGroundRollLabel.text = [[NSString alloc] initWithFormat:@"%.f",groundRollCorrectedForWind];
            self.takeoffObstacleLabel.text = [[NSString alloc] initWithFormat:@"%.f",clearObstacleCorrectedForWind];
        } else {
            float angle = windDirection - runwayDirection;
            float windComponent = cosf(angle*M_PI/180);
            float tailwind = windSpeed * windComponent;
            float crosswindComponent = sinf(angle*M_PI/180);
            float crosswind = windSpeed * crosswindComponent;
            self.takeoffCrossWindComponent = [[NSString alloc] initWithFormat:@"%.f",crosswind];
            self.headwindCompoent = [[NSString alloc] initWithFormat:@"%.f",tailwind];
            int tailwindInt = (int)floorf(tailwind);
            int tailwindCorrection = ((tailwindInt/2)*.1);
            groundRollCorrectedForWind = self.groundroll * (1 + tailwindCorrection);
            clearObstacleCorrectedForWind = self.clearObstacle * (1 + tailwindCorrection);
            self.takeoffGroundRollLabel.text = [[NSString alloc] initWithFormat:@"%.f",groundRollCorrectedForWind];
            self.takeoffObstacleLabel.text = [[NSString alloc] initWithFormat:@"%.f",clearObstacleCorrectedForWind];
            
        }
        [self.view endEditing:YES];

    }
    
    
    
    
}



- (void)calculateTempIndex:(float)temp {
    if (temp < 0) {
        //fix this
        self.a = 0;
    }
    if (temp >= 0 && temp < 10) {
        self.a = 0;
        self.b = 1;
        self.tempLower = 0;
    }
    if (temp >= 10 && temp < 20) {
        self.a = 1;
        self.b = 2;
        self.tempLower = 10;
    }
    if (temp >= 20 && temp < 30) {
        self.a = 2;
        self.b = 3;
        self.tempLower = 20;
    }
    if (temp >= 30 && temp < 40) {
        self.a = 3;
        self.b = 4;
        self.tempLower = 30;
    }
    if (temp == 40) {
        self.a = 4;
    }
    // Put in error if over 40
}

- (void)pressureAltitudeKeys:(int)pressureAltitude {
    if (pressureAltitude < 0) {
        self.key1 = @0;
    }
    if (pressureAltitude >= 0 && pressureAltitude < 1000) {
        self.key1 = @0;
        self.key2 = @1000;
        self.pLower = 0;
        self.pHigher = 1000;
    }
    if (pressureAltitude >= 1000 && pressureAltitude < 2000) {
        self.key1 = @1000;
        self.key2 = @2000;
        self.pLower = 1000;
        self.pHigher = 2000;
    }
    if (pressureAltitude >= 2000 && pressureAltitude < 3000) {
        self.key1 = @2000;
        self.key2 = @3000;
        self.pLower = 2000;
        self.pHigher = 3000;
    }
    if (pressureAltitude >= 3000 && pressureAltitude < 4000) {
        self.key1 = @3000;
        self.key2 = @4000;
        self.pLower = 3000;
        self.pHigher = 4000;
    }
    if (pressureAltitude >= 4000 && pressureAltitude < 5000) {
        self.key1 = @4000;
        self.key2 = @5000;
        self.pLower = 4000;
        self.pHigher = 5000;
    }
    if (pressureAltitude >= 5000 && pressureAltitude < 6000) {
        self.key1 = @5000;
        self.key2 = @6000;
        self.pLower = 5000;
        self.pHigher = 6000;
    }
    if (pressureAltitude >= 6000 && pressureAltitude < 7000) {
        self.key1 = @6000;
        self.key2 = @7000;
        self.pLower = 6000;
        self.pHigher = 7000;
    }
    if (pressureAltitude >= 7000 && pressureAltitude < 8000) {
        self.key1 = @7000;
        self.key2 = @8000;
        self.pLower = 7000;
        self.pHigher = 8000;
    }
    if (pressureAltitude == 8000) {
        self.key1 = @8000;
    }
    else {
        
    }
}

- (void)calculateDistances:(float)temp pressure:(float)pressureAltitude windDirection:(float)windDirection windSpeed:(float)windSpeed runwayDirection:(float)runwayDirection {
    [self calculateTempIndex:temp];
    [self pressureAltitudeKeys:pressureAltitude];
    
    float groundRollCorrectedForWind;
    float clearObstacleCorrectedForWind;
    
    if (pressureAltitude < 0) {
        if (temp < 0) {
            NSArray *lowerValue = [self.toDistance objectForKey:self.key1];
            if ([lowerValue objectAtIndex:self.a] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float lower = [[lowerValue objectAtIndex:self.a] floatValue];
                self.groundroll = lower;
            }
            
            
            NSArray *lValue = [self.toDistanceObstacle objectForKey:self.key1];
            if ([lValue objectAtIndex:self.a] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float l = [[lValue objectAtIndex:self.a] floatValue];
                self.clearObstacle = l;
            }
        }
        if (temp >= 0 && temp < 40) {
            float x = ((temp - self.tempLower)/10);
            NSArray *lowerValue = [self.toDistance objectForKey:self.key1];
            if ([lowerValue objectAtIndex:self.a] == nil || [lowerValue objectAtIndex:self.b] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float lower0 = [[lowerValue objectAtIndex:self.a] floatValue];
                float lower1 = [[lowerValue objectAtIndex:self.b] floatValue];
                float lower = (((lower1 - lower0)*x)+lower0);
                self.groundroll = lower;
            }
            
            
            NSArray *lValue = [self.toDistanceObstacle objectForKey:self.key1];
            if ([lValue objectAtIndex:self.a] == nil || [lValue objectAtIndex:self.b] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float l0 = [[lValue objectAtIndex:self.a] floatValue];
                float l1 = [[lValue objectAtIndex:self.b] floatValue];
                float l = (((l1 - l0)*x)+l0);
                self.clearObstacle = l;
            }
            
        }
        if (temp == 40) {
            NSArray *lowerValue = [self.toDistance objectForKey:self.key1];
            if ([lowerValue objectAtIndex:self.a] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float lower = [[lowerValue objectAtIndex:self.a] floatValue];
                self.groundroll = lower;
            }
            
            NSArray *lValue = [self.toDistanceObstacle objectForKey:self.key1];
            if ([lValue objectAtIndex:self.a] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float l = [[lValue objectAtIndex:self.a] floatValue];
                self.clearObstacle = l;
            }
        }
        if (temp > 40) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data of 40 degrees Celsius" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
            [alertView show];
        }
    }
    
    if (pressureAltitude >= 0 && pressureAltitude < 8000) {
        if (temp < 0 ) {
            float y = ((pressureAltitude - self.pLower)/1000);
            NSArray *lowerValue = [self.toDistance objectForKey:@0];
            NSArray *higherValue = [self.toDistance objectForKey:@1000];
            if ([lowerValue objectAtIndex:0] == nil || [higherValue objectAtIndex:0] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float lower = [[lowerValue objectAtIndex:0] floatValue];
                float higher = [[higherValue objectAtIndex:0] floatValue];
                self.groundroll = (((higher - lower)*y)+lower);
            }
            
            
            NSArray *lValue = [self.toDistanceObstacle objectForKey:@0];
            NSArray *hValue = [self.toDistanceObstacle objectForKey:@1000];
            if ([lValue objectAtIndex:0] == nil || [hValue objectAtIndex:0] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float l = [[lValue objectAtIndex:0] floatValue];
                float h = [[hValue objectAtIndex:0] floatValue];
                self.clearObstacle = (((h - l)*y)+l);
            }
        }
        
        
        if (temp >= 0 && temp < 40 ) {
            
            float y = ((pressureAltitude - self.pLower)/1000);
            float x = ((temp - self.tempLower)/10);
            NSArray *lowerValue = [self.toDistance objectForKey:self.key1];
            NSArray *higherValue = [self.toDistance objectForKey:self.key2];
            if ([lowerValue objectAtIndex:self.a] == nil || [lowerValue objectAtIndex:self.b] == nil || [higherValue objectAtIndex:self.a] == nil || [higherValue objectAtIndex:self.b] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float lower0 = [[lowerValue objectAtIndex:self.a] floatValue];
                float lower1 = [[lowerValue objectAtIndex:self.b] floatValue];
                float lower = (((lower1 - lower0)*x)+lower0);
                float higher0 = [[higherValue objectAtIndex:self.a] floatValue];
                float higher1 = [[higherValue objectAtIndex:self.b] floatValue];
                float higher = (((higher1 - higher0)*x)+higher0);
                self.groundroll = (((higher - lower)*y)+lower);
            }
            
            NSArray *lValue = [self.toDistanceObstacle objectForKey:self.key1];
            NSArray *hValue = [self.toDistanceObstacle objectForKey:self.key2];
            if ([lValue objectAtIndex:self.a] == nil || [lValue objectAtIndex:self.b] == nil || [hValue objectAtIndex:self.a] == nil || [hValue objectAtIndex:self.b] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float l0 = [[lValue objectAtIndex:self.a] floatValue];
                float l1 = [[lValue objectAtIndex:self.b] floatValue];
                float l = (((l1 - l0)*x)+l0);
                float h0 = [[hValue objectAtIndex:self.a] floatValue];
                float h1 = [[hValue objectAtIndex:self.b] floatValue];
                float h = (((h1 - h0)*x)+h0);
                self.clearObstacle = (((h - l)*y)+l);
            }
        }
        if (temp == 40) {
            float y = ((pressureAltitude - self.pLower)/1000);
            NSArray *lowerValue = [self.toDistance objectForKey:self.key1];
            NSArray *higherValue = [self.toDistance objectForKey:self.key2];
            if ([lowerValue objectAtIndex:self.a] == nil || [higherValue objectAtIndex:self.a]){
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float lower = [[lowerValue objectAtIndex:self.a] floatValue];
                float higher = [[higherValue objectAtIndex:self.a] floatValue];
                self.groundroll = (((higher - lower)*y)+lower);
            }
            
            
            NSArray *lValue = [self.toDistanceObstacle objectForKey:self.key1];
            
            NSArray *hValue = [self.toDistanceObstacle objectForKey:self.key2];
            if ([lValue objectAtIndex:self.a] == nil || [hValue objectAtIndex:self.a] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float l = [[lValue objectAtIndex:self.a] floatValue];
                float h = [[hValue objectAtIndex:self.a] floatValue];
                self.clearObstacle = (((h - l)*y)+l);
            }
            
        }
        if (temp > 40) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data of 40 degrees Celsius" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
            [alertView show];
        }
    }
    if (pressureAltitude == 8000) {
        if (temp < 0) {
            NSArray *lowerValue = [self.toDistance objectForKey:self.key1];
            if ([lowerValue objectAtIndex:self.a] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float lower = [[lowerValue objectAtIndex:self.a] floatValue];
                self.groundroll = lower;
            }
            
            
            NSArray *lValue = [self.toDistanceObstacle objectForKey:self.key1];
            if ([lValue objectAtIndex:self.a] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float l = [[lValue objectAtIndex:self.a] floatValue];
                self.clearObstacle = l;
            }
        }
        if (temp >= 0 && temp < 40) {
            float x = ((temp - self.tempLower)/10);
            NSArray *lowerValue = [self.toDistance objectForKey:self.key1];
            if ([lowerValue objectAtIndex:self.a] == nil || [lowerValue objectAtIndex:self.b] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float lower0 = [[lowerValue objectAtIndex:self.a] floatValue];
                float lower1 = [[lowerValue objectAtIndex:self.b] floatValue];
                float lower = (((lower1 - lower0)*x)+lower0);
                self.groundroll = lower;
            }
            
            
            NSArray *lValue = [self.toDistanceObstacle objectForKey:self.key1];
            if ([lValue objectAtIndex:self.a] == nil || [lValue objectAtIndex:self.b] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float l0 = [[lValue objectAtIndex:self.a] floatValue];
                float l1 = [[lValue objectAtIndex:self.b] floatValue];
                float l = (((l1 - l0)*x)+l0);
                self.clearObstacle = l;
            }
        }
        if (temp == 40) {
            NSArray *lowerValue = [self.toDistance objectForKey:self.key1];
            if ([lowerValue objectAtIndex:self.a] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float lower = [[lowerValue objectAtIndex:self.a] floatValue];
                self.groundroll = lower;
            }
            
            NSArray *lValue = [self.toDistanceObstacle objectForKey:self.key1];
            if ([lValue objectAtIndex:self.a] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float l = [[lValue objectAtIndex:self.a] floatValue];
                self.clearObstacle = l;
            }
        }
        if (temp > 40) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data of 40 degrees Celsius" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
            [alertView show];
        }
    }
    if (pressureAltitude > 8000) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Pressure Exceeds POH Data of 8,000 Feet" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
        [alertView show];
    }
    
    if ((windDirection - runwayDirection) <= 90) {
        float angle = windDirection - runwayDirection;
        float windComponent = cosf(angle*M_PI/180);
        float headwind = windSpeed * windComponent;
        float crosswindComponent = sinf(angle*M_PI/180);
        float crosswind = windSpeed * crosswindComponent;
        float crosswindAbs = fabsf(crosswind);
        self.takeoffCrossWindComponent = [[NSString alloc] initWithFormat:@"%.f",crosswindAbs];
        self.headwindCompoent = [[NSString alloc] initWithFormat:@"%.f",headwind];
        int headwindInt = (int)floorf(headwind);
        int headwindCorrection = ((headwindInt/9)*.1);
        groundRollCorrectedForWind = self.groundroll * (1 - headwindCorrection);
        clearObstacleCorrectedForWind = self.clearObstacle * (1- headwindCorrection);
        self.takeoffGroundRollLabel.text = [[NSString alloc] initWithFormat:@"%.f",groundRollCorrectedForWind];
        self.takeoffObstacleLabel.text = [[NSString alloc] initWithFormat:@"%.f",clearObstacleCorrectedForWind];
    } else {
        float angle = windDirection - runwayDirection;
        float windComponent = cosf(angle*M_PI/180);
        float tailwind = windSpeed * windComponent;
        float crosswindComponent = sinf(angle*M_PI/180);
        float crosswind = windSpeed * crosswindComponent;
        self.takeoffCrossWindComponent = [[NSString alloc] initWithFormat:@"%.f",crosswind];
        self.headwindCompoent = [[NSString alloc] initWithFormat:@"%.f",tailwind];
        int tailwindInt = (int)floorf(tailwind);
        int tailwindCorrection = ((tailwindInt/2)*.1);
        groundRollCorrectedForWind = self.groundroll * (1 + tailwindCorrection);
        clearObstacleCorrectedForWind = self.clearObstacle * (1 + tailwindCorrection);
        self.takeoffGroundRollLabel.text = [[NSString alloc] initWithFormat:@"%.f",groundRollCorrectedForWind];
        self.takeoffObstacleLabel.text = [[NSString alloc] initWithFormat:@"%.f",clearObstacleCorrectedForWind];
        
    }

}
// Why is this not working!!!!!!
- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if ([identifier isEqualToString:@"cessna172RLanding"]) {
       
        
        if (self.fieldElevationTextField.text.length == 0) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please enter a Field Elevation" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
            [alertView show];
            
            return NO;
        }
        if (self.pressureTextField.text.length == 0) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please enter an Altimeter Setting" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
            [alertView show];
            
            return  NO;
        }
        if (self.temperatureTextField.text.length == 0) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please Enter a temperature" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
            [alertView show];
            
            return  NO;
        }
        
    }
    
    return  YES;
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"cessna172RLanding"]) {
        CDCessna172RLandingDataViewController *controller = (CDCessna172RLandingDataViewController *)segue.destinationViewController;
        controller.totalWeightBefore = self.totalWeight;
        controller.fuelGallongsBefore = self.fuelGallons;
        controller.totalMomentBefore = self.totalMoment;
        controller.fuelARM = self.fuelArm;
        controller.fieldElevationTO = self.fieldElevationTextField.text;
        controller.pressureTO = self.pressureTextField.text;
        controller.temperatureTO = self.temperatureTextField.text;
        controller.takeoffWindComponent = self.headwindCompoent;
        controller.pilotWeight = self.pilotWeight;
        controller.frontSeatWeight = self.frontSeatWeight;
        controller.rearSeat1Weight = self.rearSeat1;
        controller.rearSeat2Weight = self.rearSeat2;
        controller.bagArea1Weight = self.bagArea1;
        controller.bagArea2Weight = self.bagArea2;
        controller.emptyWeight = self.emptyWeight;
        controller.emptyArm = self.emtpyArm; 
        controller.totalArmTakeoff = self.totalArm;
        controller.takeoffCG = self.takeoffCG;
        controller.takeoffGroundRoll = self.takeoffGroundRollLabel.text;
        controller.takeoffClearObstacle = self.takeoffObstacleLabel.text;
        controller.takeoffDensityAltitude = self.densityAltitudeLabel.text;
        controller.nNumber = self.nNumber;
        controller.toManeurveringSpeed = self.toManeuveringSpeed;
        controller.takeoffWindDirection = self.windDirectionTextField.text;
        controller.takeoffWindSpeed = self.windSpeedTextField.text;
        controller.takeoffRunway = self.runwayTextField.text;
        controller.takeoffWindComponent = self.headwindCompoent;
        controller.takeoffCrossWindComponent = self.takeoffCrossWindComponent; 
    }
}


@end
