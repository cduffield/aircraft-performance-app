//
//  CDCirrusSR20M3050TakeoffGraphViewController.h
//  Aircraft Performance
//
//  Created by Chip Duffield on 8/18/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CorePlot-CocoaTouch.h"

@interface CDCirrusSR20M3050TakeoffGraphViewController : UIViewController <CPTPlotDataSource>

@property (nonatomic) NSString *totalArm;
@property (nonatomic) NSString *totalWeight;
@property (nonatomic) NSString *cg;

@end
