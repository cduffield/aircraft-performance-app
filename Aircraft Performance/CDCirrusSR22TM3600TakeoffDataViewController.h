//
//  CDCirrusSR22TM3600TakeoffDataViewController.h
//  Aircraft Performance
//
//  Created by Chip Duffield on 8/20/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDCirrusSR22TM3600TakeoffDataViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *fieldElevationTextField;
@property (weak, nonatomic) IBOutlet UITextField *pressureTextField;
@property (weak, nonatomic) IBOutlet UITextField *temperatureTextField;
@property (weak, nonatomic) IBOutlet UITextField *windDirectionTextField;
@property (weak, nonatomic) IBOutlet UITextField *windSpeedTextField;
@property (weak, nonatomic) IBOutlet UITextField *runwayTextField;
@property (weak, nonatomic) IBOutlet UILabel *totalWeightLabel;
@property (weak, nonatomic) IBOutlet UILabel *densityAltitudeLabel;
@property (weak, nonatomic) IBOutlet UILabel *takeoffGroundRollLabel;
@property (weak, nonatomic) IBOutlet UILabel *takeoffClearObstacleLabel;

- (IBAction)calculate:(id)sender;

@property (nonatomic) NSDictionary *toDistance2900;
@property (nonatomic) NSDictionary *toDistance3600;
@property (nonatomic) NSDictionary *toDistanceObstacle2900;
@property (nonatomic) NSDictionary *toDistanceObstacle3600;

@property (nonatomic) NSString *nNumberText;
@property (nonatomic) NSString *emptyWeightText;
@property (nonatomic) NSString *emptyArmText;
@property (nonatomic) NSString *pilotWeightText;
@property (nonatomic) NSString *frontSeatWeightText;
@property (nonatomic) NSString *rearSeat1WeightText;
@property (nonatomic) NSString *rearSeat2WeightText;
@property (nonatomic) NSString *bagAreaWeightText;
@property (nonatomic) NSString *fuelGalText;
@property (nonatomic) NSString *fuelWeightText;
@property (nonatomic) NSString *taxiBurnWeightText;
@property (nonatomic) NSString *totalWeightText;
@property (nonatomic) NSString *pilotArmText;
@property (nonatomic) NSString *frontSeatArmText;
@property (nonatomic) NSString *rearSeat1ArmText;
@property (nonatomic) NSString *rearSeat2ArmText;
@property (nonatomic) NSString *bagAreaArmText;
@property (nonatomic) NSString *fuelArmText;
@property (nonatomic) NSString *taxiBurnArmText;
@property (nonatomic) NSString *totalArmText;
@property (nonatomic) NSString *emptyMomentText;
@property (nonatomic) NSString *pilotMomentText;
@property (nonatomic) NSString *frontSeatMomentText;
@property (nonatomic) NSString *rearSeat1MomentText;
@property (nonatomic) NSString *rearSeat2MomentText;
@property (nonatomic) NSString *bagAreaMomentText;
@property (nonatomic) NSString *fuelMomentText;
@property (nonatomic) NSString *taxiBurnMomentText;
@property (nonatomic) NSString *totalMomentText;
@property (nonatomic) NSString *takeoffCGLimitsText;
@property (nonatomic) NSString *takeoffManeuveringSpeedText;

@property (nonatomic) float a;
@property (nonatomic) float b;
@property (nonatomic) float tempLower;
@property (nonatomic) float pLower;
@property (nonatomic) float pHigher;
@property (nonatomic) NSNumber *key1;
@property (nonatomic) NSNumber *key2;

@property (nonatomic) float groundroll2500;
@property (nonatomic) float clearObstacle2500;
@property (nonatomic) float groundroll3000;
@property (nonatomic) float clearObstacle3000;
@property (nonatomic) float groundroll;
@property (nonatomic) float clearObstacle;

@property (nonatomic) NSString *headwindComponent;
@property (nonatomic) NSString *takeoffCrossWindComponent;


@end
