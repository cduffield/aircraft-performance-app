//
//  CDCirrusSR22M3400LandingGraphViewController.h
//  Aircraft Performance
//
//  Created by Chip Duffield on 8/18/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CorePlot-CocoaTouch.h"

@interface CDCirrusSR22M3400LandingGraphViewController : UIViewController <CPTPlotDataSource>

@property (nonatomic) NSString *landingWeight;
@property (nonatomic) NSString *landingArm;
@property (nonatomic) NSString *landingCG;

@end
