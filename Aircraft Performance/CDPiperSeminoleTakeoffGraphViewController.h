//
//  CDPiperSeminoleTakeoffGraphViewController.h
//  Aircraft Performance
//
//  Created by Chip Duffield on 10/9/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDPiperSeminoleTakeoffGraphViewController : UIViewController

@property (nonatomic) NSString *totalArm;
@property (nonatomic) NSString *totalWeight;
@property (nonatomic) NSString *cg;

@end
