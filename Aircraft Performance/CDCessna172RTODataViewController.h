//
//  CDCessna172RTODataViewController.h
//  Aircraft Performance
//
//  Created by Chip Duffield on 7/3/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDCessna172RTODataViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *totalWeightLabel;
@property (weak, nonatomic) IBOutlet UITextField *fieldElevationTextField;
@property (weak, nonatomic) IBOutlet UITextField *pressureTextField;
@property (weak, nonatomic) IBOutlet UITextField *temperatureTextField;
@property (weak, nonatomic) IBOutlet UILabel *densityAltitudeLabel;
@property (weak, nonatomic) IBOutlet UILabel *takeoffGroundRollLabel;
@property (weak, nonatomic) IBOutlet UILabel *takeoffObstacleLabel;
@property (weak, nonatomic) IBOutlet UITextField *windDirectionTextField;
@property (weak, nonatomic) IBOutlet UITextField *windSpeedTextField;
@property (weak, nonatomic) IBOutlet UITextField *runwayTextField;


@property (nonatomic) NSDictionary *toDistance;
@property (nonatomic) NSDictionary *toDistanceObstacle; 

@property (nonatomic) NSString *totalArm;
@property (nonatomic) NSString *totalWeight;
@property (nonatomic) NSString *fuelGallons;
@property (nonatomic) NSString *totalMoment;
@property (nonatomic) NSString *fuelArm;
@property (nonatomic) NSString *pilotWeight;
@property (nonatomic) NSString *frontSeatWeight;
@property (nonatomic) NSString *rearSeat1;
@property (nonatomic) NSString *rearSeat2;
@property (nonatomic) NSString *bagArea1;
@property (nonatomic) NSString *bagArea2;
@property (nonatomic) NSString *nNumber;
@property (nonatomic) NSString *toManeuveringSpeed;
@property (nonatomic) NSString *takeoffWindDirection;
@property (nonatomic) NSString *takeoffWindSpeed;
@property (nonatomic) NSString *takeoffRunway;
@property (nonatomic) NSString *takeoffCrossWindComponent; 

@property (nonatomic) NSString *emptyWeight;
@property (nonatomic) NSString *emtpyArm;

@property (nonatomic) NSString *takeoffCG;
@property (nonatomic) NSString *headwindCompoent;


@property (nonatomic) float groundroll;
@property (nonatomic) float clearObstacle;

@property (nonatomic) float a;
@property (nonatomic) float b;
@property (nonatomic) NSNumber *key1;
@property (nonatomic) NSNumber *key2;
@property (nonatomic) float pLower;
@property (nonatomic) float pHigher;
@property (nonatomic) float tempLower;


- (IBAction)calculate:(id)sender;

@end
