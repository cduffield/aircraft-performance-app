//
//  CDCirrusSR20M3000ViewController.h
//  Aircraft Performance
//
//  Created by Chip Duffield on 7/30/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDCirrusSR20M3000ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *pilotTextField;
@property (weak, nonatomic) IBOutlet UITextField *frontSeatTextField;
@property (weak, nonatomic) IBOutlet UITextField *rearSeat1TextField;
@property (weak, nonatomic) IBOutlet UITextField *rearSeat2TextField;
@property (weak, nonatomic) IBOutlet UITextField *baggageAreaTextField;
@property (weak, nonatomic) IBOutlet UITextField *fuelGalTextField;

@property (nonatomic) NSString *nNumberText;
@property (nonatomic) NSString *emptyWeightText;
@property (nonatomic) NSString *emptyArmText;
@property (nonatomic) float bagArea1;
@property (nonatomic) float fuelGal;

@end
