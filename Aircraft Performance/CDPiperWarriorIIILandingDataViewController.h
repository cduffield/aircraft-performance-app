//
//  CDPiperWarriorIIILandingDataViewController.h
//  Aircraft Performance
//
//  Created by Chip Duffield on 10/6/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDPiperWarriorIIILandingDataViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *gphTextField;
@property (weak, nonatomic) IBOutlet UITextField *timeEnrouteTextField;
@property (weak, nonatomic) IBOutlet UITextField *landingFieldElevationTextField;
@property (weak, nonatomic) IBOutlet UITextField *landingPressureTextField;
@property (weak, nonatomic) IBOutlet UITextField *landingTemperatureTextField;
@property (weak, nonatomic) IBOutlet UITextField *landingWindDirectionTextField;
@property (weak, nonatomic) IBOutlet UITextField *landingWindSpeedTextField;
@property (weak, nonatomic) IBOutlet UITextField *landingRunwayTextField;
@property (weak, nonatomic) IBOutlet UILabel *landingTotalWeightLabel;
@property (weak, nonatomic) IBOutlet UILabel *landingTotalArmLabel;
@property (weak, nonatomic) IBOutlet UILabel *landingTotalMomentLabel;
@property (weak, nonatomic) IBOutlet UILabel *cgLimitsLabel;
@property (weak, nonatomic) IBOutlet UILabel *landingGroundRollLabel;
@property (weak, nonatomic) IBOutlet UILabel *landingClearObstacleLabel;

@property (nonatomic) NSString *nNumberText;
@property (nonatomic) NSString *emptyWeightText;
@property (nonatomic) NSString *emptyArmText;
@property (nonatomic) NSString *pilotWeightText;
@property (nonatomic) NSString *frontSeatWeightText;
@property (nonatomic) NSString *rearSeat1WeightText;
@property (nonatomic) NSString *rearSeat2WeightText;
@property (nonatomic) NSString *bagAreaWeightText;
@property (nonatomic) NSString *fuelGalText;
@property (nonatomic) NSString *fuelWeightText;
@property (nonatomic) NSString *taxiBurnWeightText;
@property (nonatomic) NSString *totalWeightText;
@property (nonatomic) NSString *pilotArmText;
@property (nonatomic) NSString *frontSeatArmText;
@property (nonatomic) NSString *rearSeat1ArmText;
@property (nonatomic) NSString *rearSeat2ArmText;
@property (nonatomic) NSString *bagAreaArmText;
@property (nonatomic) NSString *fuelArmText;
@property (nonatomic) NSString *taxiBurnArmText;
@property (nonatomic) NSString *totalArmText;
@property (nonatomic) NSString *emptyMomentText;
@property (nonatomic) NSString *pilotMomentText;
@property (nonatomic) NSString *frontSeatMomentText;
@property (nonatomic) NSString *rearSeat1MomentText;
@property (nonatomic) NSString *rearSeat2MomentText;
@property (nonatomic) NSString *bagAreaMomentText;
@property (nonatomic) NSString *fuelMomentText;
@property (nonatomic) NSString *taxiBurnMomentText;
@property (nonatomic) NSString *totalMomentText;
@property (nonatomic) NSString *takeoffCGLimitsText;
@property (nonatomic) NSString *takeoffManeuveringSpeedText;
@property (nonatomic) NSString *takeoffFieldElevation;
@property (nonatomic) NSString *takeoffPressure;
@property (nonatomic) NSString *takeoffTemperature;
@property (nonatomic) NSString *takeoffWindDirection;
@property (nonatomic) NSString *takeoffWindSpeed;
@property (nonatomic) NSString *takeoffRunway;
@property (nonatomic) NSString *takeoffGroundRoll;
@property (nonatomic) NSString *takeoffClearObstacle;
@property (nonatomic) NSString *takeoffDensityAltitude;
@property (nonatomic) NSString *takeoffHeadWindComponent;
@property (nonatomic) NSString *takeoffCrosswindComponent;

@property (nonatomic) NSString *takeoffFuelWeight;
@property (nonatomic) NSString *landingFuelGallons;
@property (nonatomic) NSString *landingFuelWeight;
@property (nonatomic) NSString *landingTotalWeight;
@property (nonatomic) NSString *landingManeuveringSpeedText;
@property (nonatomic) NSString *landingDensityAltitude;
@property (nonatomic) float landingGroundRoll;
@property (nonatomic) float landingClearObstacle;
@property (nonatomic) NSString *landingCrossWindComponent;
@property (nonatomic) NSString *landingHeadWindComponent;

@property (nonatomic) float standardWeight;
@property (nonatomic) float constant;
@property (nonatomic) float constantObstacle;
@property (nonatomic) float newdistance;
@property (nonatomic) float dRatio;
@property (nonatomic) float weight;
@property (nonatomic) float groundRollDistance;
@property (nonatomic) float clearObstacleDistance;
@property (nonatomic) float headwindPercentage;
@property (nonatomic) float headwindPercentageObstacle;
@property (nonatomic) float tailwindPercentage;
@property (nonatomic) float tailwindPercentageObstacle;

- (IBAction)calculate:(id)sender;


@end
