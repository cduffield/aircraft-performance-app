//
//  CDCirrusSR22TM3400TakeoffDataViewController.m
//  Aircraft Performance
//
//  Created by Chip Duffield on 8/19/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import "CDCirrusSR22TM3400TakeoffDataViewController.h"
#import "CDCirrusSR22TM3400LandingDataViewController.h"

@interface CDCirrusSR22TM3400TakeoffDataViewController ()

@end

@implementation CDCirrusSR22TM3400TakeoffDataViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = false;
    
    self.totalWeightLabel.text = self.totalWeightText;
    
    NSArray *seaLevel = [[NSArray alloc] initWithObjects:@610,@659,@710,@763,@818,nil];
    NSArray *p1000 = [[NSArray alloc] initWithObjects:@673,@727,@783,@841,@902,nil];
    NSArray *p2000 = [[NSArray alloc] initWithObjects:@743,@802,@864,@929,@995,nil];
    NSArray *p3000 = [[NSArray alloc] initWithObjects:@821,@887,@995,@1026,@1100,nil];
    NSArray *p4000 = [[NSArray alloc] initWithObjects:@908,@981,@1057,@1135,@1217,nil];
    NSArray *p5000 = [[NSArray alloc] initWithObjects:@1006,@1086,@1170,@1257,@1348,nil];
    NSArray *p6000 = [[NSArray alloc] initWithObjects:@1116,@1205,@1298,@1394,@1494,nil];
    NSArray *p7000 = [[NSArray alloc] initWithObjects:@1238,@1337,@1440,@1547,@1659,nil];
    NSArray *p8000 = [[NSArray alloc] initWithObjects:@1376,@1486,@1601,@1720,@1843,nil];
    NSArray *p9000 = [[NSArray alloc] initWithObjects:@1532,@1654,@1781,@1914,@2051,nil];
    NSArray *p10000 = [[NSArray alloc] initWithObjects:@1707,@1843,@1985,@2132,@2285,nil];
    self.toDistance2900 = [[NSDictionary alloc] initWithObjectsAndKeys:seaLevel,@0,p1000,@1000,p2000,@2000,p3000,@3000,p4000,@4000,p5000,@5000,p6000,@6000,p7000,@7000,p8000,@8000,p9000,@9000,p10000,@10000, nil];
    
    NSArray *seaLevelat3400 = [[NSArray alloc] initWithObjects:@917,@990,@1067,@1146,@1229,nil];
    NSArray *p1000at3400 = [[NSArray alloc] initWithObjects:@1011,@1092,@1176,@1264,@1355,nil];
    NSArray *p2000at3400 = [[NSArray alloc] initWithObjects:@1116,@1206,@1299,@1395,@1496,nil];
    NSArray *p3000at3400 = [[NSArray alloc] initWithObjects:@1234,@1332,@1435,@1542,@1653,nil];
    NSArray *p4000at3400 = [[NSArray alloc] initWithObjects:@1365,@1474,@1588,@1706,@1829,nil];
    NSArray *p5000at3400 = [[NSArray alloc] initWithObjects:@1512,@1633,@1758,@1889,@2025,nil];
    NSArray *p6000at3400 = [[NSArray alloc] initWithObjects:@1676,@1810,@1950,@2095,@2245,nil];
    NSArray *p7000at3400 = [[NSArray alloc] initWithObjects:@1861,@2009,@2164,@2325,@2492,nil];
    NSArray *p8000at3400 = [[NSArray alloc] initWithObjects:@2068,@2233,@2405,@2584,@2770,nil];
    NSArray *p9000at3400 = [[NSArray alloc] initWithObjects:@2302,@2485,@2677,@2875,@3082,nil];
    NSArray *p10000at3400 = [[NSArray alloc] initWithObjects:@2564,@2769,@2982,@3204,@3434,nil];
    self.toDistance3400 = [[NSDictionary alloc] initWithObjectsAndKeys:seaLevelat3400,@0,p1000at3400,@1000,p2000at3400,@2000,p3000at3400,@3000,p4000at3400,@4000,p5000at3400,@5000,p6000at3400,@6000,p7000at3400,@7000,p8000at3400,@8000,p9000at3400,@9000,p10000at3400,@10000, nil];
    
    NSArray *pressureSeaLevel = [[NSArray alloc] initWithObjects:@971,@1043,@1118,@1195,@1275,nil];
    NSArray *pressure1000 = [[NSArray alloc] initWithObjects:@1066,@1146,@1228,@1313,@1401,nil];
    NSArray *pressure2000 = [[NSArray alloc] initWithObjects:@1173,@1260,@1351,@1444,@1541,nil];
    NSArray *pressure3000 = [[NSArray alloc] initWithObjects:@1292,@1388,@1487,@1590,@1697,nil];
    NSArray *pressure4000 = [[NSArray alloc] initWithObjects:@1424,@1530,@1639,@1753,@1871,nil];
    NSArray *pressure5000 = [[NSArray alloc] initWithObjects:@1571,@1688,@1809,@1935,@2065,nil];
    NSArray *pressure6000 = [[NSArray alloc] initWithObjects:@1736,@1865,@1999,@2138,@2281,nil];
    NSArray *pressure7000 = [[NSArray alloc] initWithObjects:@1920,@2063,@2211,@2365,@2523,nil];
    NSArray *pressure8000 = [[NSArray alloc] initWithObjects:@2127,@2285,@2449,@2619,@2795,nil];
    NSArray *pressure9000 = [[NSArray alloc] initWithObjects:@2359,@2534,@2716,@2904,@3099,nil];
    NSArray *pressure10000 = [[NSArray alloc] initWithObjects:@2619,@2814,@3016,@3225,@3441,nil];
    self.toDistanceObstacle2900 = [[NSDictionary alloc] initWithObjectsAndKeys:pressureSeaLevel,@0,pressure1000,@1000,pressure2000,@2000,pressure3000,@3000,pressure4000,@4000,pressure5000,@5000,pressure6000,@6000,pressure7000,@7000,pressure8000,@8000,pressure9000,@9000,pressure10000,@10000, nil];
    
    NSArray *pressureSeaLevelat3400 = [[NSArray alloc] initWithObjects:@1432,@1539,@1650,@1764,@1883,nil];
    NSArray *pressure1000at3400 = [[NSArray alloc] initWithObjects:@1574,@1691,@1813,@1939,@2069,nil];
    NSArray *pressure2000at3400 = [[NSArray alloc] initWithObjects:@1732,@1861,@1995,@2133,@2276,nil];
    NSArray *pressure3000at3400 = [[NSArray alloc] initWithObjects:@1907,@2049,@2196,@2349,@2507,nil];
    NSArray *pressure4000at3400 = [[NSArray alloc] initWithObjects:@2102,@2259,@2422,@2590,@2764,nil];
    NSArray *pressure5000at3400 = [[NSArray alloc] initWithObjects:@2320,@2493,@2673,@2858,@3051,nil];
    NSArray *pressure6000at3400 = [[NSArray alloc] initWithObjects:@2564,@2755,@2953,@3159,@3371,nil];
    NSArray *pressure7000at3400 = [[NSArray alloc] initWithObjects:@2837,@3048,@3267,@3494,@3729,nil];
    NSArray *pressure8000at3400 = [[NSArray alloc] initWithObjects:@3142,@3376,@3619,@3871,@4131,nil];
    NSArray *pressure9000at3400 = [[NSArray alloc] initWithObjects:@3485,@3744,@4014,@4293,@4581,nil];
    NSArray *pressure10000at3400 = [[NSArray alloc] initWithObjects:@3870,@4158,@4457,@4767,@5088,nil];
    self.toDistanceObstacle3400 = [[NSDictionary alloc] initWithObjectsAndKeys:pressureSeaLevelat3400,@0,pressure1000at3400,@1000,pressure2000at3400,@2000,pressure3000at3400,@3000,pressure4000at3400,@4000,pressure5000at3400,@5000,pressure6000at3400,@6000,pressure7000at3400,@7000,pressure8000at3400,@8000,pressure9000at3400,@9000,pressure10000at3400,@10000, nil];
    
    
}




- (IBAction)calculate:(id)sender {
    
    float windDirection = [self.windDirectionTextField.text floatValue];
    float windSpeed = [self.windSpeedTextField.text floatValue];
    float runwayDirection = ([self.runwayTextField.text floatValue]*10);
    
    if (runwayDirection > 360) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please enter runway without last Digit. Use 9 instead of 090" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
        [alertView show];
        
    } else if (windDirection > 360) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error'" message:@"Wind Direction can not be greater that 360" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
        [alertView show];
    } else {
        
        //constants for both
        float pressure = [self.pressureTextField.text floatValue];
        float temp = [self.temperatureTextField.text floatValue];
        float fieldElevation = [self.fieldElevationTextField.text floatValue];
        float pressureAltitude = (((29.92 - pressure)*1000) + fieldElevation);
        float isaTemp = (15 -((pressureAltitude/1000) * 1.98));
        float totalWeight = [self.totalWeightText floatValue];
        
        
        float groundRollCorrectedForWind;
        float clearObstacleCorrectedForWind;
        
        //calculate density altitude
        float densityAltitude = (pressureAltitude + (118.8 * (temp - isaTemp)));
        self.densityAltitudeLabel.text = [[NSString alloc] initWithFormat:@"%.f",densityAltitude];
        
        if (totalWeight <= 2500) {
            [self calculateDistances:temp pressure:pressureAltitude windDirection:windDirection windSpeed:windSpeed runwayDirection:runwayDirection dictionary:self.toDistance2900 dictionaryObstacle:self.toDistanceObstacle2900 weight:totalWeight];
            
            if ((windDirection - runwayDirection) <= 90) {
                float angle = windDirection - runwayDirection;
                float windComponent = cosf(angle*M_PI/180);
                float headwind = windSpeed * windComponent;
                float crosswindComponent = sinf(angle*M_PI/180);
                float crosswind = windSpeed * crosswindComponent;
                float crosswindAbs = fabsf(crosswind);
                self.takeoffCrossWindComponent = [[NSString alloc] initWithFormat:@"%.f",crosswindAbs];
                self.headwindComponent = [[NSString alloc] initWithFormat:@"%.f",headwind];
                int headwindInt = (int)floorf(headwind);
                int headwindCorrection = ((headwindInt/13)*.1);
                groundRollCorrectedForWind = self.groundroll * (1 - headwindCorrection);
                clearObstacleCorrectedForWind = self.clearObstacle * (1- headwindCorrection);
                self.takeoffGroundRollLabel.text = [[NSString alloc] initWithFormat:@"%.f",groundRollCorrectedForWind];
                self.takeoffClearObstacleLabel.text = [[NSString alloc] initWithFormat:@"%.f",clearObstacleCorrectedForWind];
            } else {
                float angle = windDirection - runwayDirection;
                float windComponent = cosf(angle*M_PI/180);
                float tailwind = windSpeed * windComponent;
                float crosswindComponent = sinf(angle*M_PI/180);
                float crosswind = windSpeed * crosswindComponent;
                self.takeoffCrossWindComponent = [[NSString alloc] initWithFormat:@"%.f",crosswind];
                self.headwindComponent = [[NSString alloc] initWithFormat:@"%.f",tailwind];
                int tailwindInt = (int)floorf(tailwind);
                int tailwindCorrection = ((tailwindInt/2)*.1);
                groundRollCorrectedForWind = self.groundroll * (1 + tailwindCorrection);
                clearObstacleCorrectedForWind = self.clearObstacle * (1 + tailwindCorrection);
                self.takeoffGroundRollLabel.text = [[NSString alloc] initWithFormat:@"%.f",groundRollCorrectedForWind];
                self.takeoffClearObstacleLabel.text = [[NSString alloc] initWithFormat:@"%.f",clearObstacleCorrectedForWind];
                
            }
            
        } else {
            [self calculateDistances:temp pressure:pressureAltitude windDirection:windDirection windSpeed:windSpeed runwayDirection:runwayDirection dictionary:self.toDistance2900 dictionaryObstacle:self.toDistanceObstacle2900 weight:2900];
            [self calculateDistances:temp pressure:pressureAltitude windDirection:windDirection windSpeed:windSpeed runwayDirection:runwayDirection dictionary:self.toDistance3400 dictionaryObstacle:self.toDistanceObstacle3400 weight:3400];
            
            float y = ((totalWeight - 2900)/500);
            self.groundroll = (((self.groundroll3000 - self.groundroll2500)*y)+self.groundroll2500);
            self.clearObstacle = (((self.clearObstacle3000 - self.clearObstacle2500)*y)+self.clearObstacle2500);
            
            if ((windDirection - runwayDirection) <= 90) {
                float angle = windDirection - runwayDirection;
                float windComponent = cosf(angle*M_PI/180);
                float headwind = windSpeed * windComponent;
                float crosswindComponent = sinf(angle*M_PI/180);
                float crosswind = windSpeed * crosswindComponent;
                float crosswindAbs = fabsf(crosswind);
                self.takeoffCrossWindComponent = [[NSString alloc] initWithFormat:@"%.f",crosswindAbs];
                self.headwindComponent = [[NSString alloc] initWithFormat:@"%.f",headwind];
                int headwindInt = (int)floorf(headwind);
                int headwindCorrection = ((headwindInt/9)*.1);
                groundRollCorrectedForWind = self.groundroll * (1 - headwindCorrection);
                clearObstacleCorrectedForWind = self.clearObstacle * (1- headwindCorrection);
                self.takeoffGroundRollLabel.text = [[NSString alloc] initWithFormat:@"%.f",groundRollCorrectedForWind];
                self.takeoffClearObstacleLabel.text = [[NSString alloc] initWithFormat:@"%.f",clearObstacleCorrectedForWind];
            } else {
                float angle = windDirection - runwayDirection;
                float windComponent = cosf(angle*M_PI/180);
                float tailwind = windSpeed * windComponent;
                float crosswindComponent = sinf(angle*M_PI/180);
                float crosswind = windSpeed * crosswindComponent;
                self.takeoffCrossWindComponent = [[NSString alloc] initWithFormat:@"%.f",crosswind];
                self.headwindComponent = [[NSString alloc] initWithFormat:@"%.f",tailwind];
                int tailwindInt = (int)floorf(tailwind);
                int tailwindCorrection = ((tailwindInt/2)*.1);
                groundRollCorrectedForWind = self.groundroll * (1 + tailwindCorrection);
                clearObstacleCorrectedForWind = self.clearObstacle * (1 + tailwindCorrection);
                self.takeoffGroundRollLabel.text = [[NSString alloc] initWithFormat:@"%.f",groundRollCorrectedForWind];
                self.takeoffClearObstacleLabel.text = [[NSString alloc] initWithFormat:@"%.f",clearObstacleCorrectedForWind];
                
            }
            
        }
        
        
        
        
    }
}

- (void)calculateTempIndex:(float)temp {
    if (temp < 0) {
        self.a = 0;
    }
    if (temp >= 0 && temp < 10) {
        self.a = 0;
        self.b = 1;
        self.tempLower = 0;
    }
    if (temp >= 10 && temp < 20) {
        self.a = 1;
        self.b = 2;
        self.tempLower = 10;
    }
    if (temp >= 20 && temp < 30) {
        self.a = 2;
        self.b = 3;
        self.tempLower = 20;
    }
    if (temp >= 30 && temp < 40) {
        self.a = 3;
        self.b = 4;
        self.tempLower = 30;
    }
    if (temp == 40) {
        self.a = 4;
    }
}

- (void)pressureAltitudeKeys:(int)pressureAltitude {
    if (pressureAltitude < 0) {
        self.key1 = @0;
    }
    if (pressureAltitude >= 0 && pressureAltitude < 1000) {
        self.key1 = @0;
        self.key2 = @1000;
        self.pLower = 0;
        self.pHigher = 1000;
    }
    if (pressureAltitude >= 1000 && pressureAltitude < 2000) {
        self.key1 = @1000;
        self.key2 = @2000;
        self.pLower = 1000;
        self.pHigher = 2000;
    }
    if (pressureAltitude >= 2000 && pressureAltitude < 3000) {
        self.key1 = @2000;
        self.key2 = @3000;
        self.pLower = 2000;
        self.pHigher = 3000;
    }
    if (pressureAltitude >= 3000 && pressureAltitude < 4000) {
        self.key1 = @3000;
        self.key2 = @4000;
        self.pLower = 3000;
        self.pHigher = 4000;
    }
    if (pressureAltitude >= 4000 && pressureAltitude < 5000) {
        self.key1 = @4000;
        self.key2 = @5000;
        self.pLower = 4000;
        self.pHigher = 5000;
    }
    if (pressureAltitude >= 5000 && pressureAltitude < 6000) {
        self.key1 = @5000;
        self.key2 = @6000;
        self.pLower = 5000;
        self.pHigher = 6000;
    }
    if (pressureAltitude >= 6000 && pressureAltitude < 7000) {
        self.key1 = @6000;
        self.key2 = @7000;
        self.pLower = 6000;
        self.pHigher = 7000;
    }
    if (pressureAltitude >= 7000 && pressureAltitude < 8000) {
        self.key1 = @7000;
        self.key2 = @8000;
        self.pLower = 7000;
        self.pHigher = 8000;
    }
    if (pressureAltitude >= 8000 && pressureAltitude < 9000) {
        self.key1 = @8000;
        self.key2 = @9000;
        self.pLower = 8000;
        self.pHigher = 9000;
    }
    if (pressureAltitude >= 9000 && pressureAltitude < 10000) {
        self.key1 = @9000;
        self.key2 = @10000;
        self.pLower = 9000;
        self.pHigher = 10000;
    }
    if (pressureAltitude == 10000) {
        self.key1 = @10000;
    }
    else {
        
    }
}

- (void)calculateDistances:(float)temp pressure:(float)pressureAltitude windDirection:(float)windDirection windSpeed:(float)windSpeed runwayDirection:(float)runwayDirection dictionary:(NSDictionary *)dictionaryValues dictionaryObstacle:(NSDictionary *)dictionaryObstacleValues  weight:(float)weight {
    [self calculateTempIndex:temp];
    [self pressureAltitudeKeys:pressureAltitude];
    float groundRoll;
    float clearObstacle;
    
    
    if (pressureAltitude < 0) {
        if (temp < 0) {
            NSArray *lowerValue = [dictionaryValues objectForKey:self.key1];
            if ([lowerValue objectAtIndex:self.a] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float lower = [[lowerValue objectAtIndex:self.a] floatValue];
                groundRoll = lower;
            }
            
            
            NSArray *lValue = [dictionaryObstacleValues objectForKey:self.key1];
            if ([lValue objectAtIndex:self.a] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float l = [[lValue objectAtIndex:self.a] floatValue];
                clearObstacle = l;
            }
        }
        if (temp >= 0 && temp < 40) {
            float x = ((temp - self.tempLower)/10);
            NSArray *lowerValue = [dictionaryValues objectForKey:self.key1];
            if ([lowerValue objectAtIndex:self.a] == nil || [lowerValue objectAtIndex:self.b] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float lower0 = [[lowerValue objectAtIndex:self.a] floatValue];
                float lower1 = [[lowerValue objectAtIndex:self.b] floatValue];
                float lower = (((lower1 - lower0)*x)+lower0);
                groundRoll = lower;
            }
            
            
            NSArray *lValue = [dictionaryObstacleValues objectForKey:self.key1];
            if ([lValue objectAtIndex:self.a] == nil || [lValue objectAtIndex:self.b] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float l0 = [[lValue objectAtIndex:self.a] floatValue];
                float l1 = [[lValue objectAtIndex:self.b] floatValue];
                float l = (((l1 - l0)*x)+l0);
                clearObstacle = l;
            }
            
        }
        if (temp == 40) {
            NSArray *lowerValue = [dictionaryValues objectForKey:self.key1];
            if ([lowerValue objectAtIndex:self.a] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float lower = [[lowerValue objectAtIndex:self.a] floatValue];
                groundRoll = lower;
            }
            
            NSArray *lValue = [dictionaryObstacleValues objectForKey:self.key1];
            if ([lValue objectAtIndex:self.a] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float l = [[lValue objectAtIndex:self.a] floatValue];
                clearObstacle = l;
            }
        }
        if (temp > 40) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data of 40 degrees Celsius" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
            [alertView show];
        }
    }
    
    if (pressureAltitude >= 0 && pressureAltitude < 10000) {
        if (temp < 0 ) {
            float y = ((pressureAltitude - self.pLower)/1000);
            NSArray *lowerValue = [dictionaryValues objectForKey:self.key1];
            NSArray *higherValue = [dictionaryValues objectForKey:self.key2];
            if ([lowerValue objectAtIndex:0] == nil || [higherValue objectAtIndex:0] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float lower = [[lowerValue objectAtIndex:0] floatValue];
                float higher = [[higherValue objectAtIndex:0] floatValue];
                groundRoll = (((higher - lower)*y)+lower);
            }
            
            
            NSArray *lValue = [dictionaryObstacleValues objectForKey:self.key1];
            NSArray *hValue = [dictionaryObstacleValues objectForKey:self.key2];
            if ([lValue objectAtIndex:0] == nil || [hValue objectAtIndex:0] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float l = [[lValue objectAtIndex:0] floatValue];
                float h = [[hValue objectAtIndex:0] floatValue];
                clearObstacle = (((h - l)*y)+l);
            }
        }
        
        
        if (temp >= 0 && temp < 40 ) {
            
            float y = ((pressureAltitude - self.pLower)/1000);
            float x = ((temp - self.tempLower)/10);
            NSArray *lowerValue = [dictionaryValues objectForKey:self.key1];
            NSArray *higherValue = [dictionaryValues objectForKey:self.key2];
            if ([lowerValue objectAtIndex:self.a] == nil || [lowerValue objectAtIndex:self.b] == nil || [higherValue objectAtIndex:self.a] == nil || [higherValue objectAtIndex:self.b] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float lower0 = [[lowerValue objectAtIndex:self.a] floatValue];
                float lower1 = [[lowerValue objectAtIndex:self.b] floatValue];
                float lower = (((lower1 - lower0)*x)+lower0);
                float higher0 = [[higherValue objectAtIndex:self.a] floatValue];
                float higher1 = [[higherValue objectAtIndex:self.b] floatValue];
                float higher = (((higher1 - higher0)*x)+higher0);
                groundRoll = (((higher - lower)*y)+lower);
            }
            
            NSArray *lValue = [dictionaryObstacleValues objectForKey:self.key1];
            NSArray *hValue = [dictionaryObstacleValues objectForKey:self.key2];
            if ([lValue objectAtIndex:self.a] == nil || [lValue objectAtIndex:self.b] == nil || [hValue objectAtIndex:self.a] == nil || [hValue objectAtIndex:self.b] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float l0 = [[lValue objectAtIndex:self.a] floatValue];
                float l1 = [[lValue objectAtIndex:self.b] floatValue];
                float l = (((l1 - l0)*x)+l0);
                float h0 = [[hValue objectAtIndex:self.a] floatValue];
                float h1 = [[hValue objectAtIndex:self.b] floatValue];
                float h = (((h1 - h0)*x)+h0);
                clearObstacle = (((h - l)*y)+l);
            }
        }
        if (temp == 40) {
            float y = ((pressureAltitude - self.pLower)/1000);
            NSArray *lowerValue = [dictionaryValues objectForKey:self.key1];
            NSArray *higherValue = [dictionaryValues objectForKey:self.key2];
            if ([lowerValue objectAtIndex:self.a] == nil || [higherValue objectAtIndex:self.a]){
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float lower = [[lowerValue objectAtIndex:self.a] floatValue];
                float higher = [[higherValue objectAtIndex:self.a] floatValue];
                groundRoll = (((higher - lower)*y)+lower);
            }
            
            
            NSArray *lValue = [dictionaryObstacleValues objectForKey:self.key1];
            NSArray *hValue = [dictionaryObstacleValues objectForKey:self.key2];
            if ([lValue objectAtIndex:self.a] == nil || [hValue objectAtIndex:self.a] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float l = [[lValue objectAtIndex:self.a] floatValue];
                float h = [[hValue objectAtIndex:self.a] floatValue];
                clearObstacle = (((h - l)*y)+l);
            }
            
        }
        if (temp > 40) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data of 40 degrees Celsius" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
            [alertView show];
        }
    }
    if (pressureAltitude == 10000) {
        if (temp < 0) {
            NSArray *lowerValue = [dictionaryValues objectForKey:self.key1];
            if ([lowerValue objectAtIndex:self.a] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float lower = [[lowerValue objectAtIndex:self.a] floatValue];
                groundRoll = lower;
            }
            
            
            NSArray *lValue = [dictionaryObstacleValues objectForKey:self.key1];
            if ([lValue objectAtIndex:self.a] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float l = [[lValue objectAtIndex:self.a] floatValue];
                clearObstacle = l;
            }
        }
        if (temp >= 0 && temp < 40) {
            float x = ((temp - self.tempLower)/10);
            NSArray *lowerValue = [dictionaryValues objectForKey:self.key1];
            if ([lowerValue objectAtIndex:self.a] == nil || [lowerValue objectAtIndex:self.b] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float lower0 = [[lowerValue objectAtIndex:self.a] floatValue];
                float lower1 = [[lowerValue objectAtIndex:self.b] floatValue];
                float lower = (((lower1 - lower0)*x)+lower0);
                groundRoll = lower;
            }
            
            
            NSArray *lValue = [dictionaryObstacleValues objectForKey:self.key1];
            if ([lValue objectAtIndex:self.a] == nil || [lValue objectAtIndex:self.b] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float l0 = [[lValue objectAtIndex:self.a] floatValue];
                float l1 = [[lValue objectAtIndex:self.b] floatValue];
                float l = (((l1 - l0)*x)+l0);
                clearObstacle = l;
            }
        }
        if (temp == 40) {
            NSArray *lowerValue = [dictionaryValues objectForKey:self.key1];
            if ([lowerValue objectAtIndex:self.a] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float lower = [[lowerValue objectAtIndex:self.a] floatValue];
                groundRoll = lower;
            }
            
            NSArray *lValue = [dictionaryObstacleValues objectForKey:self.key1];
            if ([lValue objectAtIndex:self.a] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float l = [[lValue objectAtIndex:self.a] floatValue];
                clearObstacle = l;
            }
        }
        if (temp > 40) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data of 40 degrees Celsius" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
            [alertView show];
        }
    }
    if (pressureAltitude > 10000) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Pressure Exceeds POH Data of 10,000 Feet" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
        [alertView show];
    }
    
    if (weight < 2900) {
        self.groundroll = groundRoll;
        self.clearObstacle = clearObstacle;
    }
    if (weight == 2900) {
        self.groundroll2500 = groundRoll;
        self.clearObstacle2500 = clearObstacle;
    }
    else {
        self.groundroll3000 = groundRoll;
        self.clearObstacle3000 = clearObstacle;
    }
    
    [self.view endEditing:YES];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"landingData"]) {
        CDCirrusSR22TM3400LandingDataViewController *controller = segue.destinationViewController;
        controller.nNumberText = self.nNumberText;
        controller.emptyWeightText = self.emptyWeightText;
        controller.pilotWeightText = self.pilotWeightText;
        controller.frontSeatWeightText = self.frontSeatWeightText;
        controller.rearSeat1WeightText = self.rearSeat1WeightText;
        controller.rearSeat2WeightText = self.rearSeat2WeightText;
        controller.bagAreaWeightText = self.bagAreaWeightText;
        controller.fuelGalText = self.fuelGalText;
        controller.fuelWeightText = self.fuelWeightText;
        controller.taxiBurnWeightText = self.taxiBurnWeightText;
        controller.totalWeightText = self.totalWeightLabel.text;
        controller.emptyArmText = self.emptyArmText;
        controller.pilotArmText = self.pilotArmText;
        controller.frontSeatArmText = self.frontSeatArmText;
        controller.rearSeat1ArmText = self.rearSeat1ArmText;
        controller.rearSeat2ArmText = self.rearSeat2ArmText;
        controller.bagAreaArmText = self.bagAreaArmText;
        controller.fuelArmText = self.fuelArmText;
        controller.taxiBurnArmText = self.taxiBurnArmText;
        controller.totalArmText = self.totalArmText;
        controller.emptyMomentText = self.emptyMomentText;
        controller.pilotMomentText = self.pilotMomentText;
        controller.frontSeatMomentText = self.frontSeatMomentText;
        controller.rearSeat1MomentText = self.rearSeat1MomentText;
        controller.rearSeat2MomentText = self.rearSeat2MomentText;
        controller.bagAreaMomentText = self.bagAreaMomentText;
        controller.fuelMomentText = self.fuelMomentText;
        controller.taxiBurnMomentText = self.taxiBurnMomentText;
        controller.totalMomentText = self.totalMomentText;
        controller.takeoffCGLimitsText = self.takeoffCGLimitsText;
        controller.takeoffManeuveringSpeedText = self.takeoffManeuveringSpeedText;
        controller.takeoffFieldElevation = self.fieldElevationTextField.text;
        controller.takeoffPressure = self.pressureTextField.text;
        controller.takeoffTemperature = self.temperatureTextField.text;
        controller.takeoffWindDirection = self.windDirectionTextField.text;
        controller.takeoffWindSpeed = self.windSpeedTextField.text;
        controller.takeoffRunway = self.runwayTextField.text;
        controller.takeoffGroundRoll = self.takeoffGroundRollLabel.text;
        controller.takeoffClearObstacle = self.takeoffClearObstacleLabel.text;
        controller.takeoffDensityAltitude = self.densityAltitudeLabel.text;
        controller.takeoffHeadWindComponent = self.headwindComponent;
        controller.takeoffCrosswindComponent = self.takeoffCrossWindComponent;
        
    }
}



@end
