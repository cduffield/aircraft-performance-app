//
//  CDCirrusSR22TM3600LandingGraphViewController.h
//  Aircraft Performance
//
//  Created by Chip Duffield on 8/20/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDCirrusSR22TM3600LandingGraphViewController : UIViewController

@property (nonatomic) NSString *landingWeight;
@property (nonatomic) NSString *landingArm;
@property (nonatomic) NSString *landingCG;

@end
