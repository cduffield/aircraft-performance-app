//
//  CDPiperWarriorIIILandingDataViewController.m
//  Aircraft Performance
//
//  Created by Chip Duffield on 10/6/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import "CDPiperWarriorIIILandingDataViewController.h"
#import "CDPiperWarriorIIILandingGraphViewController.h"
#import "CDPiperWarriorIIIPDFViewController.h"

@interface CDPiperWarriorIIILandingDataViewController ()

@end

@implementation CDPiperWarriorIIILandingDataViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = false;
    
    self.landingFieldElevationTextField.text = self.takeoffFieldElevation;
    self.landingPressureTextField.text = self.takeoffPressure;
    self.landingTemperatureTextField.text = self.takeoffTemperature;
    self.landingWindDirectionTextField.text = self.takeoffWindDirection;
    self.landingWindSpeedTextField.text = self.takeoffWindSpeed;
    self.landingRunwayTextField.text = self.takeoffRunway;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)calculate:(id)sender {
    float totalWeightBefore = [self.totalWeightText floatValue];
    float totalMomentBefore = [self.totalMomentText floatValue] * 1000;
    float fuelArm = [self.fuelArmText floatValue];
    float fuelGallonsBefore = [self.fuelGalText floatValue];
    float fuelWeightBefore = fuelGallonsBefore * 6;
    float fuelMomentBefore = fuelArm * fuelWeightBefore;
    float fuelGPH = [self.gphTextField.text floatValue];
    float fuelETE = [self.timeEnrouteTextField.text floatValue];
    self.takeoffFuelWeight = [[NSString alloc] initWithFormat:@"%.f",fuelWeightBefore];
    
    //calculate zero fuel weight, moment, and arm
    float zeroFuelWeight = totalWeightBefore - fuelWeightBefore;
    float zeroFuelMoment = totalMomentBefore - fuelMomentBefore;
    
    //calculate new fuel
    float fuelBurnGallons = fuelGPH * (fuelETE/60.0);
    float fuelWeightAfter = (fuelGallonsBefore - fuelBurnGallons) * 6;
    float fuelMomentAfter = fuelWeightAfter * fuelArm;
    self.landingFuelGallons = [[NSString alloc] initWithFormat:@"%.f",(fuelGallonsBefore-fuelBurnGallons)];
    self.landingFuelWeight = [[NSString alloc] initWithFormat:@"%.f",fuelWeightAfter];
    
    //calulate new totals
    float totalWeightAfter = zeroFuelWeight + fuelWeightAfter;
    float totalMomentAfter = fuelMomentAfter + zeroFuelMoment;
    float totalArmAfter = totalMomentAfter/totalWeightAfter;
    float totalM = totalMomentAfter/1000;
    self.landingTotalWeight = [[NSString alloc] initWithFormat:@"%.f",totalWeightAfter];
    
    //Max weight calculations
    float aircraftMaxWeight = 2440.0;
    float m = 92.45;
    float b = 5723.58;
    float (^maxWeightAtCG)(float,float,float) = ^(float totalA, float m, float b) {
        if (totalA <88.3) {
            float weight = ((m * totalA) - b);
            return weight;
        }
        else {
            float weight = 2440;
            return weight;
        }
        
    };
    float maxWeight = maxWeightAtCG(totalArmAfter, m, b);
    
    
    // Maneuvering Speed Calculation
    float manSpeedMax = 111;
    float manSpeed = manSpeedMax * sqrtf(totalWeightAfter/aircraftMaxWeight);
    self.landingManeuveringSpeedText = [[NSString alloc] initWithFormat:@"%.f",manSpeed];
    
    //set cg response
    NSString *cglimits;
    if (totalArmAfter >= 83 && totalArmAfter <= 93) {
        if (totalWeightAfter <= maxWeight) {
            cglimits = @"Within Limits";
        }
        else if (totalWeightAfter > aircraftMaxWeight) {
            float overWeight = totalWeightAfter - aircraftMaxWeight;
            cglimits = [[NSString alloc] initWithFormat:@"Overweight by %.f lbs",overWeight];
        }
        else {
            cglimits = @"Outside of Limits";
        }
    }
    else {
        cglimits = @"Outside of Limits";
    }
    self.cgLimitsLabel.text = cglimits;
    self.landingTotalWeightLabel.text = [[NSString alloc] initWithFormat:@"%.f",totalWeightAfter];
    self.landingTotalArmLabel.text = [[NSString alloc] initWithFormat:@"%.1f",totalArmAfter];
    self.landingTotalMomentLabel.text = [[NSString alloc] initWithFormat:@"%.02f",totalM];
    
    
    
    // change distances
    float standardDistance = 590;
    float standardDistanceObstacle = 1110;
    self.standardWeight = 2325;
    float pressure = [self.landingPressureTextField.text floatValue];
    float temp = [self.landingTemperatureTextField.text floatValue];
    float fieldElevation = [self.landingFieldElevationTextField.text floatValue];
    float pressureAltitude = (((29.92 - pressure)*1000) + fieldElevation);
    float isaTemp = (15 -((pressureAltitude/1000) * 1.98));
    //float headwind = [self.headwindTextField.text floatValue];
    float tempInF = ((temp * 1.8)+32);
    
    //calculate density altitude
    float densityAltitude = (pressureAltitude + (118.8 * (temp - isaTemp)));
    self.landingDensityAltitude = [[NSString alloc] initWithFormat:@"%.f",densityAltitude];
    
    //change constant values
    if (pressureAltitude >= 0 && pressureAltitude <= 2000) {
        self.constant = -1.14;
        self.constantObstacle = -0.67;
    }
    if (pressureAltitude > 1000 && pressureAltitude <= 2000) {
        self.constant = -0.83;
        self.constantObstacle = -0.66;
    }
    if (pressureAltitude > 2000 && pressureAltitude <= 3000) {
        self.constant = -0.92;
        self.constantObstacle = -0.55;
    }
    if (pressureAltitude > 3000 && pressureAltitude <= 4000) {
        self.constant = -0.95;
        self.constantObstacle = -0.56;
    }
    if (pressureAltitude > 4000 && pressureAltitude <= 5000) {
        self.constant = -0.93;
        self.constantObstacle = -0.58;
    }
    if (pressureAltitude > 5000 && pressureAltitude <= 6000) {
        self.constant = -0.96;
        self.constantObstacle = -0.63;
    }
    if (pressureAltitude > 6000 && pressureAltitude <= 7000) {
        self.constant = -0.94;
        self.constantObstacle = -0.62;
    }
    
    
    [self densityRatio:pressureAltitude temperature:tempInF];
    
    //wind
    float windDirection = [self.landingWindDirectionTextField.text floatValue];
    float windSpeed = [self.landingWindSpeedTextField.text floatValue];
    float runwayDirection = ([self.landingRunwayTextField.text floatValue]*10);
    
    
    float distance = standardDistance * (pow(self.dRatio,self.constant));
    float distanceObstacle = standardDistanceObstacle * (pow(self.dRatio,self.constantObstacle));
    
    
    
    [self weightCorrection:totalWeightAfter distance:distance];
    self.groundRollDistance = self.newdistance;
    //self.landingGroundRollLabel.text = [[NSString alloc] initWithFormat:@"%.f",self.newdistance];
    [self weightCorrection:totalWeightAfter distance:distanceObstacle];
    self.clearObstacleDistance = self.newdistance;
    //self.landingClearObstacleLabel.text = [[NSString alloc] initWithFormat:@"%.f",self.newdistance];
    
    if ((windDirection - runwayDirection) <= 90) {
        float angle = windDirection - runwayDirection;
        float windComponent = cosf(angle*M_PI/180);
        float headwind = windSpeed * windComponent;
        float crosswindComponent = sinf(angle*M_PI/180);
        float crosswind = windSpeed * crosswindComponent;
        float crosswindAbs = fabsf(crosswind);
        self.landingCrossWindComponent = [[NSString alloc] initWithFormat:@"%.f",crosswindAbs];
        self.landingHeadWindComponent = [[NSString alloc] initWithFormat:@"%.f",headwind];
        int headwindInt = (int)floorf(headwind);
        if (headwindInt > 15) {
            self.headwindPercentage = .24;
            self.headwindPercentageObstacle = .19;
        } else {
            self.headwindPercentage = (headwindInt * .016);
            self.headwindPercentageObstacle = (headwindInt *.013);
        }
        //int headwindCorrection = ((headwindInt/13)*self.headwindPercentage);
        float groundrollCorrectedForWind = self.groundRollDistance * (1 - self.headwindPercentage);
        float clearObstacleCorrectedForWind = self.clearObstacleDistance * (1- self.headwindPercentageObstacle);
        self.landingGroundRollLabel.text = [[NSString alloc] initWithFormat:@"%.f",groundrollCorrectedForWind];
        self.landingClearObstacleLabel.text = [[NSString alloc] initWithFormat:@"%.f",clearObstacleCorrectedForWind];
    } else {
        float angle = windDirection - runwayDirection;
        float windComponent = cosf(angle*M_PI/180);
        float tailwind = windSpeed * windComponent;
        float crosswindComponent = sinf(angle*M_PI/180);
        float crosswind = windSpeed * crosswindComponent;
        self.landingCrossWindComponent = [[NSString alloc] initWithFormat:@"%.f",crosswind];
        self.landingHeadWindComponent = [[NSString alloc] initWithFormat:@"%.f",tailwind];
        int tailwindInt = (int)floorf(tailwind);
        if (tailwindInt > 5) {
            self.tailwindPercentage = .28;
            self.tailwindPercentageObstacle = .22;
        } else {
            self.tailwindPercentage = (tailwindInt * .056);
            self.tailwindPercentageObstacle = (tailwindInt * .044);
        }
        //int tailwindCorrection = ((tailwindInt/2)*self.tailwindPercentage);
        float groundrollCorrectedForWind = self.groundRollDistance * (1 + self.tailwindPercentage);
        float clearObstacleCorrectedForWind = self.clearObstacleDistance * (1 + self.tailwindPercentageObstacle);
        self.landingGroundRollLabel.text = [[NSString alloc] initWithFormat:@"%.f",groundrollCorrectedForWind];
        self.landingClearObstacleLabel.text = [[NSString alloc] initWithFormat:@"%.f",clearObstacleCorrectedForWind];
        
    }
    
    
    [self.view endEditing:YES];
    
}

#pragma mark - Helper Methods
- (void)densityRatio:(float)pressureAltitude temperature:(float)tempF
{
    self.dRatio = (519 * pow((1-(0.00000689*pressureAltitude)),5.256))/(tempF + 460);
}


- (void)weightCorrection:(float)weight distance:(float)startingDistance
{
    self.newdistance = startingDistance * pow(weight/self.standardWeight,2);
}




#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"landingGraph"]) {
        CDPiperWarriorIIILandingGraphViewController *controller = (CDPiperWarriorIIILandingGraphViewController *)segue.destinationViewController;
        controller.landingWeight = self.landingTotalWeightLabel.text;
        controller.landingArm = self.landingTotalArmLabel.text;
        controller.landingCG = self.cgLimitsLabel.text;
    }
    if ([segue.identifier isEqualToString:@"pdfView"]) {
        CDPiperWarriorIIIPDFViewController *controller = (CDPiperWarriorIIIPDFViewController *)segue.destinationViewController;
        controller.totalWeight = self.totalWeightText;
        controller.pilotWeight = self.pilotWeightText;
        controller.frontSeat = self.frontSeatWeightText;
        controller.rearSeat1 = self.rearSeat1WeightText;
        controller.rearSeat2 = self.rearSeat2WeightText;
        controller.bagArea1 = self.bagAreaWeightText;
        controller.fuelGal = self.fuelGalText;
        controller.fuelWeight = self.fuelWeightText;
        controller.emptyWeight = self.emptyWeightText;
        controller.emptyArm = self.emptyArmText;
        controller.landingFuelGal = self.landingFuelGallons;
        controller.landingFuelWeight = self.landingFuelWeight;
        controller.landingTotalWeight = self.landingTotalWeightLabel.text;
        controller.totalArm = self.totalArmText;
        controller.totalMoment = self.totalMomentText;
        controller.landingTotalArm = self.landingTotalArmLabel.text;
        controller.landingTotalMoment = self.landingTotalMomentLabel.text;
        controller.landingCG = self.cgLimitsLabel.text;
        controller.takeoffCG = self.takeoffCGLimitsText;
        controller.landingGroundRoll = self.landingGroundRollLabel.text;
        controller.landingClearObstacle = self.landingClearObstacleLabel.text;
        controller.takeoffGroundRoll = self.takeoffGroundRoll;
        controller.takeoffObstacleClearance = self.takeoffClearObstacle;
        controller.takeoffDensityAltitude = self.takeoffDensityAltitude;
        controller.landingDensityAltitude = self.landingDensityAltitude;
        controller.nNumber = self.nNumberText;
        controller.ldgManeurvingSpeed = self.landingManeuveringSpeedText;
        controller.toManeuveringSpeed = self.takeoffManeuveringSpeedText;
        controller.landingWindDirection = self.landingWindDirectionTextField.text;
        controller.landingWindSpeed = self.landingWindSpeedTextField.text;
        controller.landingRunway = self.landingRunwayTextField.text;
        controller.takeoffWindDirection = self.takeoffWindDirection;
        controller.takeoffWindSpeed = self.takeoffWindSpeed;
        controller.takeoffRunway = self.takeoffRunway;
        controller.landingWindComponent = self.landingHeadWindComponent;
        controller.takeofWindComponent = self.takeoffHeadWindComponent;
        controller.landingCrossWindComponent = self.landingCrossWindComponent;
        controller.takeoffCrossWindComponent = self.takeoffCrosswindComponent;
    }
    
}


@end
