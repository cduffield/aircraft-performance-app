//
//  CDCirrusSR22TM3600LandingDataViewController.h
//  Aircraft Performance
//
//  Created by Chip Duffield on 8/20/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDCirrusSR22TM3600LandingDataViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *gphTextField;
@property (weak, nonatomic) IBOutlet UITextField *timeEnrouteTextField;
@property (weak, nonatomic) IBOutlet UITextField *fieldElevationTextField;
@property (weak, nonatomic) IBOutlet UITextField *pressureTextField;
@property (weak, nonatomic) IBOutlet UITextField *temperatureTextField;
@property (weak, nonatomic) IBOutlet UITextField *windDirectionTextField;
@property (weak, nonatomic) IBOutlet UITextField *windSpeedTextField;
@property (weak, nonatomic) IBOutlet UITextField *runwayTextField;
@property (weak, nonatomic) IBOutlet UILabel *totalWeightLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalArmLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalMomentLabel;
@property (weak, nonatomic) IBOutlet UILabel *landingCGLabel;
@property (weak, nonatomic) IBOutlet UILabel *landingGroundRollLabel;
@property (weak, nonatomic) IBOutlet UILabel *landingClearObstacleLabel;

@property (nonatomic) NSDictionary *landingDistance;
@property (nonatomic) NSDictionary *landingDistanceObstacle;

@property (nonatomic) NSString *nNumberText;
@property (nonatomic) NSString *emptyWeightText;
@property (nonatomic) NSString *emptyArmText;
@property (nonatomic) NSString *pilotWeightText;
@property (nonatomic) NSString *frontSeatWeightText;
@property (nonatomic) NSString *rearSeat1WeightText;
@property (nonatomic) NSString *rearSeat2WeightText;
@property (nonatomic) NSString *bagAreaWeightText;
@property (nonatomic) NSString *fuelGalText;
@property (nonatomic) NSString *fuelWeightText;
@property (nonatomic) NSString *taxiBurnWeightText;
@property (nonatomic) NSString *totalWeightText;
@property (nonatomic) NSString *pilotArmText;
@property (nonatomic) NSString *frontSeatArmText;
@property (nonatomic) NSString *rearSeat1ArmText;
@property (nonatomic) NSString *rearSeat2ArmText;
@property (nonatomic) NSString *bagAreaArmText;
@property (nonatomic) NSString *fuelArmText;
@property (nonatomic) NSString *taxiBurnArmText;
@property (nonatomic) NSString *totalArmText;
@property (nonatomic) NSString *emptyMomentText;
@property (nonatomic) NSString *pilotMomentText;
@property (nonatomic) NSString *frontSeatMomentText;
@property (nonatomic) NSString *rearSeat1MomentText;
@property (nonatomic) NSString *rearSeat2MomentText;
@property (nonatomic) NSString *bagAreaMomentText;
@property (nonatomic) NSString *fuelMomentText;
@property (nonatomic) NSString *taxiBurnMomentText;
@property (nonatomic) NSString *totalMomentText;
@property (nonatomic) NSString *takeoffCGLimitsText;
@property (nonatomic) NSString *takeoffManeuveringSpeedText;
@property (nonatomic) NSString *takeoffFieldElevation;
@property (nonatomic) NSString *takeoffPressure;
@property (nonatomic) NSString *takeoffTemperature;
@property (nonatomic) NSString *takeoffWindDirection;
@property (nonatomic) NSString *takeoffWindSpeed;
@property (nonatomic) NSString *takeoffRunway;
@property (nonatomic) NSString *takeoffGroundRoll;
@property (nonatomic) NSString *takeoffClearObstacle;
@property (nonatomic) NSString *takeoffDensityAltitude;
@property (nonatomic) NSString *takeoffHeadWindComponent;
@property (nonatomic) NSString *takeoffCrosswindComponent;

@property (nonatomic) float a;
@property (nonatomic) float b;
@property (nonatomic) float tempLower;
@property (nonatomic) float pLower;
@property (nonatomic) float pHigher;
@property (nonatomic) NSNumber *key1;
@property (nonatomic) NSNumber *key2;

@property (nonatomic) float maxM;
@property (nonatomic) float maxB;
@property (nonatomic) float minM;
@property (nonatomic) float minB;
@property (nonatomic) NSString *cgLimits;

@property (nonatomic) NSString *takeoffFuelWeight;
@property (nonatomic) NSString *landingFuelGallons;
@property (nonatomic) NSString *landingFuelWeight;
@property (nonatomic) NSString *landingTotalWeight;
@property (nonatomic) NSString *landingManeuveringSpeedText;
@property (nonatomic) NSString *landingDensityAltitude;
@property (nonatomic) float landingGroundRoll;
@property (nonatomic) float landingClearObstacle;
@property (nonatomic) NSString *landingCrossWindComponent;
@property (nonatomic) NSString *landingHeadWindComponent;

- (IBAction)calculate:(id)sender;

@end
