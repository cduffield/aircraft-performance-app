//
//  CDCirrusSR22TM3600List.m
//  Aircraft Performance
//
//  Created by Chip Duffield on 9/2/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import "CDCirrusSR22TM3600List.h"


@implementation CDCirrusSR22TM3600List

@dynamic arm;
@dynamic nNumber;
@dynamic emptyWeight;

@end
