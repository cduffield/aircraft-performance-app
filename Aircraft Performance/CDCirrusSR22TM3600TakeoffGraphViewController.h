//
//  CDCirrusSR22TM3600TakeoffGraphViewController.h
//  Aircraft Performance
//
//  Created by Chip Duffield on 8/20/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDCirrusSR22TM3600TakeoffGraphViewController : UIViewController

@property (nonatomic) NSString *totalArm;
@property (nonatomic) NSString *totalWeight;
@property (nonatomic) NSString *cg;

@end
