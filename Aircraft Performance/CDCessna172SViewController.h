//
//  CDCessna172SViewController.h
//  Aircraft Performance
//
//  Created by Chip Duffield on 8/16/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDCessna172SViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *pilotWeightTextField;
@property (weak, nonatomic) IBOutlet UITextField *frontSeatWeightTextField;
@property (weak, nonatomic) IBOutlet UITextField *rearSeat1TextField;
@property (weak, nonatomic) IBOutlet UITextField *rearSeat2TextField;
@property (weak, nonatomic) IBOutlet UITextField *bagArea1TextField;
@property (weak, nonatomic) IBOutlet UITextField *bagArea2TextField;
@property (weak, nonatomic) IBOutlet UITextField *fuelGalTextField;

@property (nonatomic) float bagArea1;
@property (nonatomic) float bagArea2;
@property (nonatomic) float fuelGal;

@property (nonatomic) NSString *emptyWeight;
@property (nonatomic) NSString *arm;
@property (nonatomic) NSString *nNumber;

@end
