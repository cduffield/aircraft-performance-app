//
//  CDPiperSeminole.m
//  Aircraft Performance
//
//  Created by Chip Duffield on 10/9/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import "CDPiperSeminole.h"


@implementation CDPiperSeminole

@dynamic nNumber;
@dynamic arm;
@dynamic emptyWeight;

@end
