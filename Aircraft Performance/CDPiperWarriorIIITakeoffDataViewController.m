//
//  CDPiperWarriorIIITakeoffDataViewController.m
//  Aircraft Performance
//
//  Created by Chip Duffield on 10/6/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import "CDPiperWarriorIIITakeoffDataViewController.h"
#import "CDPiperWarriorIIILandingDataViewController.h"

@interface CDPiperWarriorIIITakeoffDataViewController ()

@end

@implementation CDPiperWarriorIIITakeoffDataViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = false;
    
    self.takeoffTotalWeightLabel.text = self.totalWeightText;
    self.weight = [self.totalWeightText floatValue];
    
}




- (IBAction)calculate:(id)sender {
    float standardDistance = 1125;
    float standardDistanceObstacle = 2000;
    self.standardWeight = 2440;
    float pressure = [self.takeoffPressureTextField.text floatValue];
    float temp = [self.takeoffTemperatureTextField.text floatValue];
    float fieldElevation = [self.takeoffFieldElevationTextField.text floatValue];
    float pressureAltitude = (((29.92 - pressure)*1000) + fieldElevation);
    float isaTemp = (15 -((pressureAltitude/1000) * 1.98));
    //float headwind = [self.headwindTextField.text floatValue];
    float tempInF = ((temp * 1.8)+32);
    
    //calculate density altitude
    float densityAltitude = (pressureAltitude + (118.8 * (temp - isaTemp)));
    self.takeoffDensityAltitudeLabel.text = [[NSString alloc] initWithFormat:@"%.f",densityAltitude];
    
    
    if (pressureAltitude >= 0 && pressureAltitude < 2000) {
        self.constant = -5.04;
        self.constantObstacle = -4.00;
    }
    if (pressureAltitude >= 2000 && pressureAltitude < 3000) {
        self.constant = -4.26;
        self.constantObstacle = -3.73;
    }
    if (pressureAltitude >= 3000 && pressureAltitude < 4000) {
        self.constant = -4.26;
        self.constantObstacle = -3.58;
    }
    if (pressureAltitude >= 4000 && pressureAltitude < 5000) {
        self.constant = -3.97;
        self.constantObstacle = -3.60;
    }
    if (pressureAltitude >= 5000 && pressureAltitude < 6000) {
        self.constant = -3.85;
        self.constantObstacle = -3.53;
    }
    if (pressureAltitude >= 6000 && pressureAltitude < 7000) {
        self.constant = -3.65;
        self.constantObstacle = -3.44;
    }
    
    
    [self densityRatio:pressureAltitude temperature:tempInF];
    
    //wind
    float windDirection = [self.takeoffWindDirectionTextField.text floatValue];
    float windSpeed = [self.takeoffWindSpeedTextField.text floatValue];
    float runwayDirection = ([self.takeoffRunwayTextField.text floatValue]*10);
    
    
    
    float distance = standardDistance * (pow(self.dRatio,self.constant));
    float distanceObstacle = standardDistanceObstacle * (pow(self.dRatio,self.constantObstacle));
    
    
    
    [self weightCorrection:self.weight distance:distance];
    self.groundRollDistance = self.newdistance;
    //self.takeoffGroundRollLabel.text = [[NSString alloc] initWithFormat:@"%.f",self.newdistance];
    [self weightCorrection:self.weight distance:distanceObstacle];
    self.clearObstacleDistance = self.newdistance;
    //self.takeoffClearObstacleLabel.text = [[NSString alloc] initWithFormat:@"%.f",self.newdistance];
    
    
    if ((windDirection - runwayDirection) <= 90) {
        float angle = windDirection - runwayDirection;
        float windComponent = cosf(angle*M_PI/180);
        float headwind = windSpeed * windComponent;
        float crosswindComponent = sinf(angle*M_PI/180);
        float crosswind = windSpeed * crosswindComponent;
        float crosswindAbs = fabsf(crosswind);
        self.takeoffCrossWindComponent = [[NSString alloc] initWithFormat:@"%.f",crosswindAbs];
        self.takeoffHeadWindComponent = [[NSString alloc] initWithFormat:@"%.f",headwind];
        int headwindInt = (int)floorf(headwind);
        if (headwindInt > 15) {
            self.headwindPercentage = .24;
            self.headwindPercentageObstacle = .18;
        } else {
            self.headwindPercentage = (headwindInt * .016);
            self.headwindPercentageObstacle = (headwindInt *.012);
        }
        //int headwindCorrection = ((headwindInt/13)*self.headwindPercentage);
        float groundrollCorrectedForWind = self.groundRollDistance * (1 - self.headwindPercentage);
        float clearObstacleCorrectedForWind = self.clearObstacleDistance * (1- self.headwindPercentageObstacle);
        self.takeoffGroundRollLabel.text = [[NSString alloc] initWithFormat:@"%.f",groundrollCorrectedForWind];
        self.takeoffClearObstacleLabel.text = [[NSString alloc] initWithFormat:@"%.f",clearObstacleCorrectedForWind];
    } else {
        float angle = windDirection - runwayDirection;
        float windComponent = cosf(angle*M_PI/180);
        float tailwind = windSpeed * windComponent;
        float crosswindComponent = sinf(angle*M_PI/180);
        float crosswind = windSpeed * crosswindComponent;
        self.takeoffCrossWindComponent = [[NSString alloc] initWithFormat:@"%.f",crosswind];
        self.takeoffHeadWindComponent = [[NSString alloc] initWithFormat:@"%.f",tailwind];
        int tailwindInt = (int)floorf(tailwind);
        if (tailwindInt > 5) {
            self.tailwindPercentage = .30;
            self.tailwindPercentageObstacle = .19;
        } else {
            self.tailwindPercentage = (tailwindInt * .06);
            self.tailwindPercentageObstacle = (tailwindInt * .038);
        }
        //int tailwindCorrection = ((tailwindInt/2)*self.tailwindPercentage);
        float groundrollCorrectedForWind = self.groundRollDistance * (1 + self.tailwindPercentage);
        float clearObstacleCorrectedForWind = self.clearObstacleDistance * (1 + self.tailwindPercentageObstacle);
        self.takeoffGroundRollLabel.text = [[NSString alloc] initWithFormat:@"%.f",groundrollCorrectedForWind];
        self.takeoffClearObstacleLabel.text = [[NSString alloc] initWithFormat:@"%.f",clearObstacleCorrectedForWind];
        
    }
    
    
    [self.view endEditing:YES];
    
    
}

#pragma mark - Helper Methods
- (void)densityRatio:(float)pressureAltitude temperature:(float)tempF
{
    self.dRatio = (519 * pow((1-(0.00000689*pressureAltitude)),5.256))/(tempF + 460);
}


- (void)weightCorrection:(float)weight distance:(float)startingDistance
{
    self.newdistance = startingDistance * pow(weight/self.standardWeight,2);
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"landingData"]) {
        CDPiperWarriorIIILandingDataViewController *controller = segue.destinationViewController;
        controller.nNumberText = self.nNumberText;
        controller.emptyWeightText = self.emptyWeightText;
        controller.pilotWeightText = self.pilotWeightText;
        controller.frontSeatWeightText = self.frontSeatWeightText;
        controller.rearSeat1WeightText = self.rearSeat1WeightText;
        controller.rearSeat2WeightText = self.rearSeat2WeightText;
        controller.bagAreaWeightText = self.bagAreaWeightText;
        controller.fuelGalText = self.fuelGalText;
        controller.fuelWeightText = self.fuelWeightText;
        controller.taxiBurnWeightText = self.taxiBurnWeightText;
        controller.totalWeightText = self.takeoffTotalWeightLabel.text;
        controller.emptyArmText = self.emptyArmText;
        controller.pilotArmText = self.pilotArmText;
        controller.frontSeatArmText = self.frontSeatArmText;
        controller.rearSeat1ArmText = self.rearSeat1ArmText;
        controller.rearSeat2ArmText = self.rearSeat2ArmText;
        controller.bagAreaArmText = self.bagAreaArmText;
        controller.fuelArmText = self.fuelArmText;
        controller.taxiBurnArmText = self.taxiBurnArmText;
        controller.totalArmText = self.totalArmText;
        controller.emptyMomentText = self.emptyMomentText;
        controller.pilotMomentText = self.pilotMomentText;
        controller.frontSeatMomentText = self.frontSeatMomentText;
        controller.rearSeat1MomentText = self.rearSeat1MomentText;
        controller.rearSeat2MomentText = self.rearSeat2MomentText;
        controller.bagAreaMomentText = self.bagAreaMomentText;
        controller.fuelMomentText = self.fuelMomentText;
        controller.taxiBurnMomentText = self.taxiBurnMomentText;
        controller.totalMomentText = self.totalMomentText;
        controller.takeoffCGLimitsText = self.takeoffCGLimitsText;
        controller.takeoffManeuveringSpeedText = self.takeoffManeuveringSpeedText;
        controller.takeoffFieldElevation = self.takeoffFieldElevationTextField.text;
        controller.takeoffPressure = self.takeoffPressureTextField.text;
        controller.takeoffTemperature = self.takeoffTemperatureTextField.text;
        controller.takeoffWindDirection = self.takeoffWindDirectionTextField.text;
        controller.takeoffWindSpeed = self.takeoffWindSpeedTextField.text;
        controller.takeoffRunway = self.takeoffRunwayTextField.text;
        controller.takeoffGroundRoll = self.takeoffGroundRollLabel.text;
        controller.takeoffClearObstacle = self.takeoffClearObstacleLabel.text;
        controller.takeoffDensityAltitude = self.takeoffDensityAltitudeLabel.text;
        controller.takeoffHeadWindComponent = self.takeoffHeadWindComponent;
        controller.takeoffCrosswindComponent = self.takeoffCrossWindComponent;
        
    }
    
}



@end
