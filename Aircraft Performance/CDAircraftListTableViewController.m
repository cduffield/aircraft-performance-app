//
//  CDAircraftListTableViewController.m
//  Aircraft Performance
//
//  Created by Chip Duffield on 6/29/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import "CDAircraftListTableViewController.h"

@interface CDAircraftListTableViewController ()

@end

@implementation CDAircraftListTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //self.tableView.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cellBackground.png"]];
    
    self.aircraftTypes = @{@"Cessna" : @[@"Cessna 172R",@"Cessna 172S"],
                           @"Cirrus" : @[@"Cirrus SR20 MTOW3000", @"Cirrus SR20 MTOW3050", @"Cirrus SR22 MTOW3400", @"Cirrus SR22T MTOW3400", @"Cirrus SR22T MTOW3600"],
                           @"Piper" : @[@"Piper Warrior II",@"Piper Warrior III",@"Piper Seminole"]};
    self.aircraftSectionTitles = @[@"Cessna",@"Cirrus",@"Piper"];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    // Return the number of sections.
    return [self.aircraftSectionTitles count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [self.aircraftSectionTitles objectAtIndex:section];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    // Return the number of rows in the section.
    NSString *sectionTitle = [self.aircraftSectionTitles objectAtIndex:section];
    NSArray *sectionAircraft = [self.aircraftTypes objectForKey:sectionTitle];
    return [sectionAircraft count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        if (indexPath.row == 0)
        {
            static NSString *CellIdentifier = @"Cell";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
            
            // Configure the cell...
            NSString *sectionTitle = [self.aircraftSectionTitles objectAtIndex:indexPath.section];
            NSArray *sectionAircraft = [self.aircraftTypes objectForKey:sectionTitle];
            NSString *aircraft = [sectionAircraft objectAtIndex:indexPath.row];
            cell.textLabel.text = aircraft;
            cell.backgroundColor = [UIColor clearColor];
            return cell;
        }
        if (indexPath.row == 1)
        {
            static NSString *CellIdentifier = @"cessna172S";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
            
            // Configure the cell...
            NSString *sectionTitle = [self.aircraftSectionTitles objectAtIndex:indexPath.section];
            NSArray *sectionAircraft = [self.aircraftTypes objectForKey:sectionTitle];
            NSString *aircraft = [sectionAircraft objectAtIndex:indexPath.row];
            cell.textLabel.text = aircraft;
            cell.backgroundColor = [UIColor clearColor];
            return cell;
        }

        else {
            static NSString *CellIdentifier = @"Cell";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
            
            // Configure the cell...
            NSString *sectionTitle = [self.aircraftSectionTitles objectAtIndex:indexPath.section];
            NSArray *sectionAircraft = [self.aircraftTypes objectForKey:sectionTitle];
            NSString *aircraft = [sectionAircraft objectAtIndex:indexPath.row];
            cell.textLabel.text = aircraft;
            cell.backgroundColor = [UIColor clearColor];
            return cell;
        }

    }
    if (indexPath.section == 1) {
        if (indexPath.row == 0)
        {
            static NSString *CellIdentifier = @"CirrusSR20M3000";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
            
            // Configure the cell...
            NSString *sectionTitle = [self.aircraftSectionTitles objectAtIndex:indexPath.section];
            NSArray *sectionAircraft = [self.aircraftTypes objectForKey:sectionTitle];
            NSString *aircraft = [sectionAircraft objectAtIndex:indexPath.row];
            cell.textLabel.text = aircraft;
            cell.backgroundColor = [UIColor clearColor];
            return cell;
        }
        if (indexPath.row == 1)
        {
            static NSString *CellIdentifier = @"cirrusSR20M3050";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
            
            // Configure the cell...
            NSString *sectionTitle = [self.aircraftSectionTitles objectAtIndex:indexPath.section];
            NSArray *sectionAircraft = [self.aircraftTypes objectForKey:sectionTitle];
            NSString *aircraft = [sectionAircraft objectAtIndex:indexPath.row];
            cell.textLabel.text = aircraft;
            cell.backgroundColor = [UIColor clearColor];
            return cell;
        }
        if (indexPath.row == 2)
        {
            static NSString *CellIdentifier = @"cirrusSR22M3400";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
            
            // Configure the cell...
            NSString *sectionTitle = [self.aircraftSectionTitles objectAtIndex:indexPath.section];
            NSArray *sectionAircraft = [self.aircraftTypes objectForKey:sectionTitle];
            NSString *aircraft = [sectionAircraft objectAtIndex:indexPath.row];
            cell.textLabel.text = aircraft;
            cell.backgroundColor = [UIColor clearColor];
            return cell;
        }
        if (indexPath.row == 3)
        {
            static NSString *CellIdentifier = @"cirrusSR22TM3400";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
            
            // Configure the cell...
            NSString *sectionTitle = [self.aircraftSectionTitles objectAtIndex:indexPath.section];
            NSArray *sectionAircraft = [self.aircraftTypes objectForKey:sectionTitle];
            NSString *aircraft = [sectionAircraft objectAtIndex:indexPath.row];
            cell.textLabel.text = aircraft;
            cell.backgroundColor = [UIColor clearColor];
            return cell;
        }

        if (indexPath.row == 4)
        {
            static NSString *CellIdentifier = @"cirrusSR22TM3600";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
            
            // Configure the cell...
            NSString *sectionTitle = [self.aircraftSectionTitles objectAtIndex:indexPath.section];
            NSArray *sectionAircraft = [self.aircraftTypes objectForKey:sectionTitle];
            NSString *aircraft = [sectionAircraft objectAtIndex:indexPath.row];
            cell.textLabel.text = aircraft;
            cell.backgroundColor = [UIColor clearColor];
            return cell;
        }
        else {
            static NSString *CellIdentifier = @"Cell";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
            
            // Configure the cell...
            NSString *sectionTitle = [self.aircraftSectionTitles objectAtIndex:indexPath.section];
            NSArray *sectionAircraft = [self.aircraftTypes objectForKey:sectionTitle];
            NSString *aircraft = [sectionAircraft objectAtIndex:indexPath.row];
            cell.textLabel.text = aircraft;
            cell.backgroundColor = [UIColor clearColor];
            return cell;
        }

    }
    if (indexPath.section == 2) {
        if (indexPath.row == 0) {
            static NSString *CellIdentifier = @"piperWarriorII";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
            
            // Configure the cell...
            NSString *sectionTitle = [self.aircraftSectionTitles objectAtIndex:indexPath.section];
            NSArray *sectionAircraft = [self.aircraftTypes objectForKey:sectionTitle];
            NSString *aircraft = [sectionAircraft objectAtIndex:indexPath.row];
            cell.textLabel.text = aircraft;
            cell.backgroundColor = [UIColor clearColor];
            return cell;

        }
        if (indexPath.row == 1) {
            static NSString *CellIdentifier = @"piperWarriorIII";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
            
            // Configure the cell...
            NSString *sectionTitle = [self.aircraftSectionTitles objectAtIndex:indexPath.section];
            NSArray *sectionAircraft = [self.aircraftTypes objectForKey:sectionTitle];
            NSString *aircraft = [sectionAircraft objectAtIndex:indexPath.row];
            cell.textLabel.text = aircraft;
            cell.backgroundColor = [UIColor clearColor];
            return cell;
            
        }
        if (indexPath.row == 2) {
            static NSString *CellIdentifier = @"piperSeminole";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
            
            // Configure the cell...
            NSString *sectionTitle = [self.aircraftSectionTitles objectAtIndex:indexPath.section];
            NSArray *sectionAircraft = [self.aircraftTypes objectForKey:sectionTitle];
            NSString *aircraft = [sectionAircraft objectAtIndex:indexPath.row];
            cell.textLabel.text = aircraft;
            cell.backgroundColor = [UIColor clearColor];
            return cell;
            
        }
        else {
            static NSString *CellIdentifier = @"Cell";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
            
            // Configure the cell...
            NSString *sectionTitle = [self.aircraftSectionTitles objectAtIndex:indexPath.section];
            NSArray *sectionAircraft = [self.aircraftTypes objectForKey:sectionTitle];
            NSString *aircraft = [sectionAircraft objectAtIndex:indexPath.row];
            cell.textLabel.text = aircraft;
            cell.backgroundColor = [UIColor clearColor];
            return cell;
        }

    }
    else {
        static NSString *CellIdentifier = @"Cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        
        // Configure the cell...
        NSString *sectionTitle = [self.aircraftSectionTitles objectAtIndex:indexPath.section];
        NSArray *sectionAircraft = [self.aircraftTypes objectForKey:sectionTitle];
        NSString *aircraft = [sectionAircraft objectAtIndex:indexPath.row];
        cell.textLabel.text = aircraft;
        cell.backgroundColor = [UIColor clearColor];
        return cell;
    }
    

}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
