//
//  CDAppDelegate.h
//  Aircraft Performance
//
//  Created by Chip Duffield on 6/29/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;



@end
