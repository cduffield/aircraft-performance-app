//
//  CDAddCessna172RViewController.m
//  Aircraft Performance
//
//  Created by Chip Duffield on 6/29/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import "CDAddCessna172RViewController.h"
#import "CDCessna172RList.h"
#import "CDCoreDataStack.h"
#import "CDCessna172RViewController.h"


@interface CDAddCessna172RViewController ()

@property (weak, nonatomic) IBOutlet UITextField *nNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *emptyWeightTextField;
@property (weak, nonatomic) IBOutlet UITextField *armTextField;

@end

@implementation CDAddCessna172RViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dismissSelf {
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)insertCessna172R {
    CDCoreDataStack *coreDataStack = [CDCoreDataStack defaultStack];
    CDCessna172RList *entry = [NSEntityDescription insertNewObjectForEntityForName:@"CDCessna172RList" inManagedObjectContext:coreDataStack.managedObjectContext];
    entry.nNumber = self.nNumberTextField.text;
    entry.emptyWeight = self.emptyWeightTextField.text;
    entry.arm = self.armTextField.text;
    
    [coreDataStack saveContext];
}

- (IBAction)doneWasPressed:(id)sender {
    [self insertCessna172R];
    [self dismissSelf];
}

- (IBAction)cancelWasPressed:(id)sender {
    [self dismissSelf]; 
}

/*
#pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"cessnaWeights"]) {
        CDCessna172RViewController *controller = (CDCessna172RViewController *)segue.destinationViewController;
        controller.emptyWeight = self.emptyWeightTextField.text;
        controller.arm = self.armTextField.text;
    }
     
     
}
 */


@end
