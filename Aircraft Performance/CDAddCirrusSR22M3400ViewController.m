//
//  CDAddCirrusSR22M3400ViewController.m
//  Aircraft Performance
//
//  Created by Chip Duffield on 8/18/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import "CDAddCirrusSR22M3400ViewController.h"
#import "CDCoreDataStack.h"
#import "CDCirrusSR22M3400List.h"
#import "CDCirrusSR22M3400TableViewController.h"

@interface CDAddCirrusSR22M3400ViewController ()

@property (weak, nonatomic) IBOutlet UITextField *nNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *emptyWeightTextField;
@property (weak, nonatomic) IBOutlet UITextField *armTextField;

@end

@implementation CDAddCirrusSR22M3400ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    self.automaticallyAdjustsScrollViewInsets = false;
}

- (void)dismissSelf {
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)insertCirrusSR22M3400 {
    CDCoreDataStack *coreDataStack = [CDCoreDataStack defaultStack];
    CDCirrusSR22M3400List *entry = [NSEntityDescription insertNewObjectForEntityForName:@"CDCirrusSR22M3400List" inManagedObjectContext:coreDataStack.managedObjectContext];
    entry.nNumber = self.nNumberTextField.text;
    entry.emptyWeight = self.emptyWeightTextField.text;
    entry.arm = self.armTextField.text;
    
    [coreDataStack saveContext];
}
- (IBAction)doneWasPressed:(id)sender {
    [self insertCirrusSR22M3400];
    [self dismissSelf];
}
- (IBAction)cancelWasPressed:(id)sender {
    [self dismissSelf];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
