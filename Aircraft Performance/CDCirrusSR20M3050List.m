//
//  CDCirrusSR20M3050List.m
//  Aircraft Performance
//
//  Created by Chip Duffield on 9/9/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import "CDCirrusSR20M3050List.h"


@implementation CDCirrusSR20M3050List

@dynamic arm;
@dynamic nNumber;
@dynamic emptyWeight;

@end
