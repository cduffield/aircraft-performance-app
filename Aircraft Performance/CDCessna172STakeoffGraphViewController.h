//
//  CDCessna172STakeoffGraphViewController.h
//  Aircraft Performance
//
//  Created by Chip Duffield on 8/16/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CorePlot-CocoaTouch.h"

@interface CDCessna172STakeoffGraphViewController : UIViewController <CPTPlotDataSource>

@property (nonatomic) NSString *totalArm;
@property (nonatomic) NSString *totalWeight;
@property (nonatomic) NSString *cg;

@end
