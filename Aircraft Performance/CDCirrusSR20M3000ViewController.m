//
//  CDCirrusSR20M3000ViewController.m
//  Aircraft Performance
//
//  Created by Chip Duffield on 7/30/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import "CDCirrusSR20M3000ViewController.h"
#import "CDCirrusSR20M3000CalculationViewController.h"

@interface CDCirrusSR20M3000ViewController ()

@end

@implementation CDCirrusSR20M3000ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor colorWithRed:77/255.0 green:75/255.0 blue:82/255.0 alpha:1.0];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation
- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if ([identifier isEqualToString:@"calculate"]) {
        self.bagArea1 = [self.baggageAreaTextField.text floatValue];
        self.fuelGal = [self.fuelGalTextField.text floatValue];
        
        if (self.fuelGal > 56.0) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"56 gallons Max" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
            [alertView show];
            
            return NO;
        }
        if (self.bagArea1 > 130.0) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Max Bag Area 1 Limit is 130 lb" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
            [alertView show];
            
            return  NO;
        }
        
    }
    
    return  YES;
}


// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"calculate"]) {
        CDCirrusSR20M3000CalculationViewController *controller = segue.destinationViewController;
        controller.nNumberText = self.nNumberText;
        controller.emptyWeightText = self.emptyWeightText;
        controller.emptyArmText = self.emptyArmText;
        controller.pilotWeightText = self.pilotTextField.text;
        controller.frontSeatWeightText = self.frontSeatTextField.text;
        controller.rearSeat1WeightText = self.rearSeat1TextField.text;
        controller.rearSeat2WeightText = self.rearSeat2TextField.text;
        controller.bagAreaWeightText = self.baggageAreaTextField.text;
        controller.fuelGalText = self.fuelGalTextField.text;
    }
}


@end
