//
//  CDCessna172RCalculationViewController.h
//  Aircraft Performance
//
//  Created by Chip Duffield on 6/30/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDCessna172RCalculationViewController : UIViewController

@property (nonatomic) NSString *pilotWeight;
@property (nonatomic) NSString *frontSeatWeight;
@property (nonatomic) NSString *rearSeat1Weight;
@property (nonatomic) NSString *rearSeat2Weight;
@property (nonatomic) NSString *bagArea1Weight;
@property (nonatomic) NSString *bagArea2Weight;
@property (nonatomic) NSString *fuelGal;
@property (nonatomic) NSString *emptyWeight;
@property (nonatomic) NSString *arm;
@property (nonatomic) NSString *nNumber;

@property (weak, nonatomic) IBOutlet UILabel *emptyWeightContent;
@property (weak, nonatomic) IBOutlet UILabel *pilotWeightContent;
@property (weak, nonatomic) IBOutlet UILabel *frontSeatWeightContent;
@property (weak, nonatomic) IBOutlet UILabel *rearSeat1WeightContent;
@property (weak, nonatomic) IBOutlet UILabel *rearSeat2WeightContent;
@property (weak, nonatomic) IBOutlet UILabel *bagArea1Content;
@property (weak, nonatomic) IBOutlet UILabel *bagArea2Content;
@property (weak, nonatomic) IBOutlet UILabel *fuelWeightContent;
@property (weak, nonatomic) IBOutlet UILabel *totalWeightContent;
@property (weak, nonatomic) IBOutlet UILabel *fuelGalContent;

@property (weak, nonatomic) IBOutlet UILabel *emptyArmContent;
@property (weak, nonatomic) IBOutlet UILabel *pilotArmContent;
@property (weak, nonatomic) IBOutlet UILabel *frontSeatArmContent;
@property (weak, nonatomic) IBOutlet UILabel *rearSeat1ArmContent;
@property (weak, nonatomic) IBOutlet UILabel *rearSeat2ArmContent;
@property (weak, nonatomic) IBOutlet UILabel *bagArea1ArmContent;
@property (weak, nonatomic) IBOutlet UILabel *bagArea2ArmContent;
@property (weak, nonatomic) IBOutlet UILabel *fuelArmContent;
@property (weak, nonatomic) IBOutlet UILabel *totalArmContent;

@property (weak, nonatomic) IBOutlet UILabel *emtpyMomentContent;
@property (weak, nonatomic) IBOutlet UILabel *pilotMomentContent;
@property (weak, nonatomic) IBOutlet UILabel *frontSeatMomentContent;
@property (weak, nonatomic) IBOutlet UILabel *rearSeat1MomentContent;
@property (weak, nonatomic) IBOutlet UILabel *rearSeat2MomentContent;
@property (weak, nonatomic) IBOutlet UILabel *bagArea1MomentContent;
@property (weak, nonatomic) IBOutlet UILabel *bagArea2MomentContent;
@property (weak, nonatomic) IBOutlet UILabel *fuelMomentContent;
@property (weak, nonatomic) IBOutlet UILabel *totalMomentContent;

@property (weak, nonatomic) IBOutlet UILabel *cgLabelContent;
@property (weak, nonatomic) IBOutlet UILabel *maneuveringSpeedContent;
@property (weak, nonatomic) IBOutlet UILabel *taxiBurnWeightContent;
@property (weak, nonatomic) IBOutlet UILabel *taxiBurnArmContent;
@property (weak, nonatomic) IBOutlet UILabel *taxiBurnMomentContent;

@property (nonatomic) NSString *totalWeightText;
@property (nonatomic) NSString *cgLimits;

@property (nonatomic) float totalWeight;
@property (nonatomic) float totalA; 


@end
