//
//  CDCessna172RLandingGraphViewController.m
//  Aircraft Performance
//
//  Created by Chip Duffield on 7/9/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import "CDCessna172RLandingGraphViewController.h"
#import "CDCessna172RPDFViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface CDCessna172RLandingGraphViewController ()

@end

@implementation CDCessna172RLandingGraphViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CPTGraphHostingView *hostView = [[CPTGraphHostingView alloc] initWithFrame:self.view.frame];
    [self.view addSubview:hostView];
    
    CPTXYGraph *graph = [[CPTXYGraph alloc] initWithFrame:hostView.bounds];
    hostView.hostedGraph = graph;
    
    graph.paddingLeft = 20;
    graph.paddingRight = 20;
    graph.paddingBottom = 20;
    graph.paddingTop = 80;
    graph.title = @"C.G. Location";
    
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *)graph.defaultPlotSpace;
    
    [plotSpace setYRange: [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(1500) length:CPTDecimalFromFloat(2500)]];
    [plotSpace setXRange: [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(34) length:CPTDecimalFromFloat(15)]];
    
    CPTXYAxisSet *axisSet = (CPTXYAxisSet *)graph.axisSet;
    
    CPTXYAxis *x = axisSet.xAxis;
    x.majorIntervalLength = CPTDecimalFromFloat(1);
    x.minorTicksPerInterval = 10;
    x.borderWidth = 2;
    
    CPTXYAxis *y = axisSet.yAxis;
    y.majorIntervalLength = CPTDecimalFromFloat(100);
    y.minorTicksPerInterval = 100;
    y.borderWidth = 2;
    
    CPTScatterPlot *plot = [[CPTScatterPlot alloc] initWithFrame:CGRectZero];
    plot.identifier = @"cgRange";
    plot.dataSource = self;
    
    
    CPTPlotSymbol *plotSymbol = [CPTPlotSymbol ellipsePlotSymbol];
    plotSymbol.size = CGSizeMake(10.0, 10.0);
    if ([self.landingCG isEqualToString:@"Within Limits"]) {
        plotSymbol.fill = [CPTFill fillWithColor:[CPTColor greenColor]];
    }
    else {
        plotSymbol.fill = [CPTFill fillWithColor:[CPTColor redColor]];
    }
    
    CPTScatterPlot *locationPlot = [[CPTScatterPlot alloc] initWithFrame:CGRectZero];
    locationPlot.identifier = @"cgLocation";
    locationPlot.plotSymbol = plotSymbol;
    locationPlot.dataSource = self;
    
    [graph addPlot:plot toPlotSpace:graph.defaultPlotSpace];
    [graph addPlot:locationPlot toPlotSpace:graph.defaultPlotSpace];
    
    self.landingIMG = [graph imageOfLayer];
    self.landingPDF = UIImagePNGRepresentation(self.landingIMG); 
    

}

- (NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plotnumberOfRecords {
    if ([(NSString *)plotnumberOfRecords.identifier isEqualToString:@"cgRange"]) {
        return 5;
    }
    else if ([(NSString *)plotnumberOfRecords.identifier isEqualToString:@"cgLocation"]) {
        return 1;
    }
    return 0;
}

- (NSNumber *)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index {
    NSNumber *num = nil;
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    NSNumber *totalWeight = [formatter numberFromString: self.landingWeight];
    NSNumber *totalArm = [formatter numberFromString:self.landingArm];
    NSArray *x = @[@35, @35, @40,@47.3, @47.3];
    NSArray *y = @[@1500, @1950, @2450,@2450, @1500];
    
    
    switch (fieldEnum) {
        case CPTScatterPlotFieldX:
            if ([(NSString *)plot.identifier isEqualToString:@"cgRange"]) {
                num = x[index];
            }
            else if ([(NSString *)plot.identifier isEqualToString:@"cgLocation"]) {
                return totalArm;
            }
            break;
        case CPTScatterPlotFieldY:
            if ([(NSString *)plot.identifier isEqualToString:@"cgRange"]) {
                num = y[index];
            }
            else if ([(NSString *)plot.identifier isEqualToString:@"cgLocation"]) {
                return totalWeight;
            }
            break;
    }
    return num;
}

+ (UIImage *)imageWithView:(UIView *)view
{
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.opaque, 0.0);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return img;
}


#pragma mark - Navigation
/*
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"cessna172RPDF"]) {
        CDCessna172RPDFViewController *controller = (CDCessna172RPDFViewController *)segue.destinationViewController;
        controller.landingPDF = self.landingIMG;
    }
}
 */


@end
