//
//  CDCirrusSR20M3000List.m
//  Aircraft Performance
//
//  Created by Chip Duffield on 7/30/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import "CDCirrusSR20M3000List.h"


@implementation CDCirrusSR20M3000List

@dynamic arm;
@dynamic emptyWeight;
@dynamic nNumber;

@end
