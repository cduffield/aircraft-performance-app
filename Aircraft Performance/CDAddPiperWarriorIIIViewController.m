//
//  CDAddPiperWarriorIIIViewController.m
//  Aircraft Performance
//
//  Created by Chip Duffield on 10/6/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import "CDAddPiperWarriorIIIViewController.h"
#import "CDCoreDataStack.h"
#import "CDPiperWarriorIIIList.h"
#import "CDPiperWarriorIIITakeoffGraphViewController.h"

@interface CDAddPiperWarriorIIIViewController ()

@property (weak, nonatomic) IBOutlet UITextField *nNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *emptyWeightTextField;
@property (weak, nonatomic) IBOutlet UITextField *armTextField;

@end

@implementation CDAddPiperWarriorIIIViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dismissSelf {
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)insertPiperWarriorIII {
    CDCoreDataStack *coreDataStack = [CDCoreDataStack defaultStack];
    CDPiperWarriorIIIList *entry = [NSEntityDescription insertNewObjectForEntityForName:@"CDPiperWarriorIIIList" inManagedObjectContext:coreDataStack.managedObjectContext];
    entry.nNumber = self.nNumberTextField.text;
    entry.emptyWeight = self.emptyWeightTextField.text;
    entry.arm = self.armTextField.text;
    
    [coreDataStack saveContext];
}

- (IBAction)doneWasPressed:(id)sender {
    [self insertPiperWarriorIII];
    [self dismissSelf];
}

- (IBAction)cancelWasPressed:(id)sender {
    [self dismissSelf];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
