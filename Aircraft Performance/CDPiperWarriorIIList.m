//
//  CDPiperWarriorIIList.m
//  Aircraft Performance
//
//  Created by Chip Duffield on 8/2/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import "CDPiperWarriorIIList.h"


@implementation CDPiperWarriorIIList

@dynamic nNumber;
@dynamic emptyWeight;
@dynamic arm;

@end
