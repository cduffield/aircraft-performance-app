//
//  CDCirrusSR20M3050List.h
//  Aircraft Performance
//
//  Created by Chip Duffield on 9/9/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface CDCirrusSR20M3050List : NSManagedObject

@property (nonatomic, retain) NSString * arm;
@property (nonatomic, retain) NSString * nNumber;
@property (nonatomic, retain) NSString * emptyWeight;

@end
