//
//  CDCessna172SList.m
//  Aircraft Performance
//
//  Created by Chip Duffield on 9/9/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import "CDCessna172SList.h"


@implementation CDCessna172SList

@dynamic arm;
@dynamic emptyWeight;
@dynamic nNumber;

@end
