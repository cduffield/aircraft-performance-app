//
//  CDCirrusSR20M3050TakeoffGraphViewController.m
//  Aircraft Performance
//
//  Created by Chip Duffield on 8/18/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import "CDCirrusSR20M3050TakeoffGraphViewController.h"

@interface CDCirrusSR20M3050TakeoffGraphViewController ()

@end

@implementation CDCirrusSR20M3050TakeoffGraphViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    CPTGraphHostingView *hostView = [[CPTGraphHostingView alloc] initWithFrame:self.view.frame];
    [self.view addSubview:hostView];
    
    CPTXYGraph *graph = [[CPTXYGraph alloc] initWithFrame:hostView.bounds];
    hostView.hostedGraph = graph;
    
    graph.paddingLeft = 20;
    graph.paddingRight = 20;
    graph.paddingBottom = 20;
    graph.paddingTop = 80;
    graph.title = @"C.G. Location";
    
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *)graph.defaultPlotSpace;
    
    [plotSpace setYRange: [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(2000) length:CPTDecimalFromFloat(3100)]];
    [plotSpace setXRange: [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(138) length:CPTDecimalFromFloat(12)]];
    
    CPTXYAxisSet *axisSet = (CPTXYAxisSet *)graph.axisSet;
    
    CPTXYAxis *x = axisSet.xAxis;
    x.majorIntervalLength = CPTDecimalFromFloat(1);
    x.minorTicksPerInterval = 10;
    x.borderWidth = 2;
    
    CPTXYAxis *y = axisSet.yAxis;
    y.majorIntervalLength = CPTDecimalFromFloat(100);
    y.minorTicksPerInterval = 100;
    y.borderWidth = 2;
    
    CPTScatterPlot *plot = [[CPTScatterPlot alloc] initWithFrame:CGRectZero];
    plot.identifier = @"cgRange";
    plot.dataSource = self;
    
    
    CPTPlotSymbol *plotSymbol = [CPTPlotSymbol ellipsePlotSymbol];
    plotSymbol.size = CGSizeMake(10.0, 10.0);
    if ([self.cg isEqualToString:@"Within Limits"]) {
        plotSymbol.fill = [CPTFill fillWithColor:[CPTColor greenColor]];
    }
    else {
        plotSymbol.fill = [CPTFill fillWithColor:[CPTColor redColor]];
    }
    
    CPTScatterPlot *locationPlot = [[CPTScatterPlot alloc] initWithFrame:CGRectZero];
    locationPlot.identifier = @"cgLocation";
    locationPlot.plotSymbol = plotSymbol;
    locationPlot.dataSource = self;
    
    [graph addPlot:plot toPlotSpace:graph.defaultPlotSpace];
    [graph addPlot:locationPlot toPlotSpace:graph.defaultPlotSpace];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plotnumberOfRecords {
    if ([(NSString *)plotnumberOfRecords.identifier isEqualToString:@"cgRange"]) {
        return 6;
    }
    else if ([(NSString *)plotnumberOfRecords.identifier isEqualToString:@"cgLocation"]) {
        return 1;
    }
    return 0;
}

- (NSNumber *)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index {
    NSNumber *num = nil;
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    NSNumber *totalWeight = [formatter numberFromString: self.totalWeight];
    NSNumber *totalArm = [formatter numberFromString:self.totalArm];
    NSArray *x = @[@137.8, @139.1, @140.7, @148.1, @148.1,@137.8];
    NSArray *y = @[@2100, @2700, @3050, @3050, @2100, @2100];
    
    
    
    
    switch (fieldEnum) {
        case CPTScatterPlotFieldX:
            if ([(NSString *)plot.identifier isEqualToString:@"cgRange"]) {
                num = x[index];
            }
            else if ([(NSString *)plot.identifier isEqualToString:@"cgLocation"]) {
                return totalArm;
            }
            break;
        case CPTScatterPlotFieldY:
            if ([(NSString *)plot.identifier isEqualToString:@"cgRange"]) {
                num = y[index];
            }
            else if ([(NSString *)plot.identifier isEqualToString:@"cgLocation"]) {
                return totalWeight;
            }
            break;
    }
    return num;
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
