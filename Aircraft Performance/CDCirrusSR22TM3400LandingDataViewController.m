//
//  CDCirrusSR22TM3400LandingDataViewController.m
//  Aircraft Performance
//
//  Created by Chip Duffield on 8/19/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import "CDCirrusSR22TM3400LandingDataViewController.h"
#import "CDCirrusSR22TM3400LandingGraphViewController.h"
#import "CDCirrusSR22TM3400PDFViewController.h"

@interface CDCirrusSR22TM3400LandingDataViewController ()

@end

@implementation CDCirrusSR22TM3400LandingDataViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.automaticallyAdjustsScrollViewInsets = false;
    
    self.fieldElevationTextField.text = self.takeoffFieldElevation;
    self.pressureTextField.text = self.takeoffPressure;
    self.temperatureTextField.text = self.takeoffTemperature;
    self.windDirectionTextField.text = self.takeoffWindDirection;
    self.windSpeedTextField.text = self.takeoffWindSpeed;
    self.runwayTextField.text = self.takeoffRunway;
    
    NSArray *pressureSeaLevel = [[NSArray alloc] initWithObjects:@1082,@1121,@1161,@1200,@1240,nil];
    NSArray *pressure1000 = [[NSArray alloc] initWithObjects:@1122,@1163,@1204,@1245,@1286,nil];
    NSArray *pressure2000 = [[NSArray alloc] initWithObjects:@1163,@1206,@1248,@1291,@1334,nil];
    NSArray *pressure3000 = [[NSArray alloc] initWithObjects:@1207,@1251,@1295,@1339,@1384,nil];
    NSArray *pressure4000 = [[NSArray alloc] initWithObjects:@1252,@1298,@1344,@1390,@1436,nil];
    NSArray *pressure5000 = [[NSArray alloc] initWithObjects:@1300,@1348,@1395,@1443,@1490,nil];
    NSArray *pressure6000 = [[NSArray alloc] initWithObjects:@1350,@1399,@1449,@1498,@1547,nil];
    NSArray *pressure7000 = [[NSArray alloc] initWithObjects:@1402,@1453,@1504,@1556,@1607,nil];
    NSArray *pressure8000 = [[NSArray alloc] initWithObjects:@1456,@1509,@1563,@1616,@1669,nil];
    NSArray *pressure9000 = [[NSArray alloc] initWithObjects:@1513,@1569,@1624,@1679,@1735,nil];
    NSArray *pressure10000 = [[NSArray alloc] initWithObjects:@1573,@1630,@1688,@1746,@1803,nil];
    self.landingDistance = [[NSDictionary alloc] initWithObjectsAndKeys:pressureSeaLevel,@0,pressure1000,@1000,pressure2000,@2000,pressure3000,@3000,pressure4000,@4000,pressure5000,@5000,pressure6000,@6000,pressure7000,@7000,pressure8000,@8000,pressure9000,@9000,pressure10000,@10000, nil];
    
    NSArray *pressureSeaLevelat50 = [[NSArray alloc] initWithObjects:@2262,@2316,@2372,@2428,@2485,nil];
    NSArray *pressure1000at50 = [[NSArray alloc] initWithObjects:@2317,@2374,@2433,@2492,@2551,nil];
    NSArray *pressure2000at50 = [[NSArray alloc] initWithObjects:@2375,@2436,@2497,@2559,@2621,nil];
    NSArray *pressure3000at50 = [[NSArray alloc] initWithObjects:@2437,@2501,@2565,@2630,@2696,nil];
    NSArray *pressure4000at50 = [[NSArray alloc] initWithObjects:@2503,@2569,@2637,@2705,@2774,nil];
    NSArray *pressure5000at50 = [[NSArray alloc] initWithObjects:@2572,@2642,@2713,@2785,@2857,nil];
    NSArray *pressure6000at50 = [[NSArray alloc] initWithObjects:@2645,@2719,@2794,@2869,@2945,nil];
    NSArray *pressure7000at50 = [[NSArray alloc] initWithObjects:@2723,@2800,@2879,@2958,@3038,nil];
    NSArray *pressure8000at50 = [[NSArray alloc] initWithObjects:@2805,@2887,@2969,@3052,@3136,nil];
    NSArray *pressure9000at50 = [[NSArray alloc] initWithObjects:@2892,@2978,@3064,@3152,@3240,nil];
    NSArray *pressure10000at50 = [[NSArray alloc] initWithObjects:@2984,@3074,@3165,@3257,@3350,nil];
    self.landingDistanceObstacle = [[NSDictionary alloc] initWithObjectsAndKeys:pressureSeaLevelat50,@0,pressure1000at50,@1000,pressure2000at50,@2000,pressure3000at50,@3000,pressure4000at50,@4000,pressure5000at50,@5000,pressure6000at50,@6000,pressure7000at50,@7000,pressure8000at50,@8000,pressure9000at50,@9000,pressure10000at50,@10000, nil];
    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)calculate:(id)sender {
    float totalWeightBefore = [self.totalWeightText floatValue];
    float totalMomentBefore = [self.totalMomentText floatValue] * 1000;
    float fuelArm = [self.fuelArmText floatValue];
    float fuelGallonsBefore = [self.fuelGalText floatValue];
    float fuelWeightBefore = fuelGallonsBefore * 6;
    float fuelMomentBefore = fuelArm * fuelWeightBefore;
    float fuelGPH = [self.gphTextField.text floatValue];
    float fuelETE = [self.timeEnrouteTextField.text floatValue];
    self.takeoffFuelWeight = [[NSString alloc] initWithFormat:@"%.f",fuelWeightBefore];
    
    //calculate zero fuel weight, moment, and arm
    float zeroFuelWeight = totalWeightBefore - fuelWeightBefore;
    float zeroFuelMoment = totalMomentBefore - fuelMomentBefore;
    
    //calculate new fuel
    float fuelBurnGallons = fuelGPH * (fuelETE/60.0);
    float fuelWeightAfter = (fuelGallonsBefore - fuelBurnGallons) * 6;
    float fuelMomentAfter = fuelWeightAfter * fuelArm;
    self.landingFuelGallons = [[NSString alloc] initWithFormat:@"%.f",(fuelGallonsBefore-fuelBurnGallons)];
    self.landingFuelWeight = [[NSString alloc] initWithFormat:@"%.f",fuelWeightAfter];
    
    //calulate new totals
    float totalWeightAfter = zeroFuelWeight + fuelWeightAfter;
    float totalMomentAfter = fuelMomentAfter + zeroFuelMoment;
    float totalArmAfter = totalMomentAfter/totalWeightAfter;
    float totalM = totalMomentAfter/1000;
    self.landingTotalWeight = [[NSString alloc] initWithFormat:@"%.f",totalWeightAfter];
    
    // CG Calculation
    float aircraftMaxWeight = 3400;
    if (totalArmAfter < 139.1) {
        self.maxM = 461.54;
        self.maxB = 61500;
    } else if (totalArmAfter >= 139.1 && totalArmAfter < 141.4) {
        self.maxM = 218.75;
        self.maxB = 27728.13;
    } else if (totalArmAfter >= 141.4 && totalArmAfter < 142.7) {
        self.maxM = 146.15;
        self.maxB = 17456.15;
    } else if (totalArmAfter >= 142.7) {
        self.maxM = 0;
        self.maxB = 0;
    }
    
    
    
    float (^maxWeightAtCG)(float,float,float) = ^(float totalArm, float m, float b) {
        if (totalArm < 142.7) {
            float weight = ((m * totalArm) - b);
            return weight;
        }
        else {
            float weight = 3400.0;
            return weight;
        }
        
    };
    
    
    float maxWeight = maxWeightAtCG(totalArmAfter, self.maxM, self.maxB);
    
    
    //set cg response
    if (totalArmAfter >= 137.8 && totalArmAfter <= 148.1) {
        if (totalWeightAfter <= maxWeight) {
            self.cgLimits = @"Within Limits";
        }
        else if (totalWeightAfter > aircraftMaxWeight) {
            float overWeight = totalWeightAfter - aircraftMaxWeight;
            self.cgLimits = [[NSString alloc] initWithFormat:@"Overweight by %.f lbs",overWeight];
        }
        else {
            self.cgLimits = @"Outside of Limits";
        }
    }
    else {
        self.cgLimits = @"Outside of Limits";
    }
    self.landingCGLabel.text = self.cgLimits;
    
    
    // Maneuvering Speed
    float manSpeedMax = 133;
    float manSpeed = manSpeedMax * sqrtf(totalWeightAfter/aircraftMaxWeight);
    self.landingManeuveringSpeedText = [[NSString alloc] initWithFormat:@"%.f",manSpeed];
    
    
    self.landingCGLabel.text = self.cgLimits;
    self.totalWeightLabel.text = [[NSString alloc] initWithFormat:@"%.f",totalWeightAfter];
    self.totalArmLabel.text = [[NSString alloc] initWithFormat:@"%.1f",totalArmAfter];
    self.totalMomentLabel.text = [[NSString alloc] initWithFormat:@"%.02f",totalM];
    
    
    
    
    
    //constants for both
    float pressure = [self.pressureTextField.text floatValue];
    float temp = [self.temperatureTextField.text floatValue];
    float fieldElevation = [self.fieldElevationTextField.text floatValue];
    float pressureAltitude = (((29.92 - pressure)*1000) + fieldElevation);
    //float wind = [self.windTextField.text floatValue];
    
    float groundrollCorrectedForWind;
    float clearObstacleCorrectedForWind;
    
    
    
    //wind
    float windDirection = [self.windDirectionTextField.text floatValue];
    float windSpeed = [self.windSpeedTextField.text floatValue];
    float runwayDirection = ([self.runwayTextField.text floatValue]*10);
    
    if (runwayDirection > 360) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please enter runway without last Digit. Use 9 instead of 090" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
        [alertView show];
    } else if (windDirection > 360) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error'" message:@"Wind Direction can not be greater that 360" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
        [alertView show];
        
    } else {
        //calculate density altitude
        float isaTemp = (15 -((pressureAltitude/1000) * 1.98));
        float densityAltitude = (pressureAltitude + (118.8 * (temp - isaTemp)));
        self.landingDensityAltitude = [[NSString alloc] initWithFormat:@"%.f",densityAltitude];
        
        //calculate ground roll
        [self calculateDistances:temp pressure:pressure windDirection:windDirection windSpeed:windSpeed runwayDirection:runwayDirection dictionary:self.landingDistance dictionaryObstacle:self.landingDistanceObstacle];
        
        
        if ((windDirection - runwayDirection) <= 90) {
            float angle = windDirection - runwayDirection;
            float windComponent = cosf(angle*M_PI/180);
            float headwind = windSpeed * windComponent;
            float crosswindComponent = sinf(angle*M_PI/180);
            float crosswind = windSpeed * crosswindComponent;
            float crosswindAbs = fabsf(crosswind);
            self.landingCrossWindComponent = [[NSString alloc] initWithFormat:@"%.f",crosswindAbs];
            self.landingHeadWindComponent = [[NSString alloc] initWithFormat:@"%.f",headwind];
            int headwindInt = (int)floorf(headwind);
            int headwindCorrection = ((headwindInt/13)*.1);
            groundrollCorrectedForWind = self.landingGroundRoll * (1 - headwindCorrection);
            clearObstacleCorrectedForWind = self.landingClearObstacle * (1- headwindCorrection);
            self.landingGroundRollLabel.text = [[NSString alloc] initWithFormat:@"%.f",groundrollCorrectedForWind];
            self.landingClearObstacleLabel.text = [[NSString alloc] initWithFormat:@"%.f",clearObstacleCorrectedForWind];
        } else {
            float angle = windDirection - runwayDirection;
            float windComponent = cosf(angle*M_PI/180);
            float tailwind = windSpeed * windComponent;
            float crosswindComponent = sinf(angle*M_PI/180);
            float crosswind = windSpeed * crosswindComponent;
            self.landingCrossWindComponent = [[NSString alloc] initWithFormat:@"%.f",crosswind];
            self.landingHeadWindComponent = [[NSString alloc] initWithFormat:@"%.f",tailwind];
            int tailwindInt = (int)floorf(tailwind);
            int tailwindCorrection = ((tailwindInt/2)*.1);
            groundrollCorrectedForWind = self.landingGroundRoll * (1 + tailwindCorrection);
            clearObstacleCorrectedForWind = self.landingClearObstacle * (1 + tailwindCorrection);
            self.landingGroundRollLabel.text = [[NSString alloc] initWithFormat:@"%.f",groundrollCorrectedForWind];
            self.landingClearObstacleLabel.text = [[NSString alloc] initWithFormat:@"%.f",clearObstacleCorrectedForWind];
            
        }
        
    }
    
    
}

- (void)calculateTempIndex:(float)temp {
    if (temp < 0) {
        self.a = 0;
    }
    if (temp >= 0 && temp < 10) {
        self.a = 0;
        self.b = 1;
        self.tempLower = 0;
    }
    if (temp >= 10 && temp < 20) {
        self.a = 1;
        self.b = 2;
        self.tempLower = 10;
    }
    if (temp >= 20 && temp < 30) {
        self.a = 2;
        self.b = 3;
        self.tempLower = 20;
    }
    if (temp >= 30 && temp < 40) {
        self.a = 3;
        self.b = 4;
        self.tempLower = 30;
    }
    if (temp == 40) {
        self.a = 4;
    }
}

- (void)pressureAltitudeKeys:(int)pressureAltitude {
    if (pressureAltitude < 0) {
        self.key1 = @0;
    }
    if (pressureAltitude >= 0 && pressureAltitude < 1000) {
        self.key1 = @0;
        self.key2 = @1000;
        self.pLower = 0;
        self.pHigher = 1000;
    }
    if (pressureAltitude >= 1000 && pressureAltitude < 2000) {
        self.key1 = @1000;
        self.key2 = @2000;
        self.pLower = 1000;
        self.pHigher = 2000;
    }
    if (pressureAltitude >= 2000 && pressureAltitude < 3000) {
        self.key1 = @2000;
        self.key2 = @3000;
        self.pLower = 2000;
        self.pHigher = 3000;
    }
    if (pressureAltitude >= 3000 && pressureAltitude < 4000) {
        self.key1 = @3000;
        self.key2 = @4000;
        self.pLower = 3000;
        self.pHigher = 4000;
    }
    if (pressureAltitude >= 4000 && pressureAltitude < 5000) {
        self.key1 = @4000;
        self.key2 = @5000;
        self.pLower = 4000;
        self.pHigher = 5000;
    }
    if (pressureAltitude >= 5000 && pressureAltitude < 6000) {
        self.key1 = @5000;
        self.key2 = @6000;
        self.pLower = 5000;
        self.pHigher = 6000;
    }
    if (pressureAltitude >= 6000 && pressureAltitude < 7000) {
        self.key1 = @6000;
        self.key2 = @7000;
        self.pLower = 6000;
        self.pHigher = 7000;
    }
    if (pressureAltitude >= 7000 && pressureAltitude < 8000) {
        self.key1 = @7000;
        self.key2 = @8000;
        self.pLower = 7000;
        self.pHigher = 8000;
    }
    if (pressureAltitude >= 8000 && pressureAltitude < 9000) {
        self.key1 = @8000;
        self.key2 = @9000;
        self.pLower = 8000;
        self.pHigher = 9000;
    }
    if (pressureAltitude >= 9000 && pressureAltitude < 10000) {
        self.key1 = @9000;
        self.key2 = @10000;
        self.pLower = 9000;
        self.pHigher = 10000;
    }
    if (pressureAltitude == 10000) {
        self.key1 = @10000;
    }
    else {
        
    }
}

- (void)calculateDistances:(float)temp pressure:(float)pressureAltitude windDirection:(float)windDirection windSpeed:(float)windSpeed runwayDirection:(float)runwayDirection dictionary:(NSDictionary *)dictionaryValues dictionaryObstacle:(NSDictionary *)dictionaryObstacleValues {
    [self calculateTempIndex:temp];
    [self pressureAltitudeKeys:pressureAltitude];
    float groundRoll;
    float clearObstacle;
    
    
    if (pressureAltitude < 0) {
        if (temp < 0) {
            NSArray *lowerValue = [dictionaryValues objectForKey:self.key1];
            if ([lowerValue objectAtIndex:self.a] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float lower = [[lowerValue objectAtIndex:self.a] floatValue];
                groundRoll = lower;
            }
            
            
            NSArray *lValue = [dictionaryObstacleValues objectForKey:self.key1];
            if ([lValue objectAtIndex:self.a] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float l = [[lValue objectAtIndex:self.a] floatValue];
                clearObstacle = l;
            }
        }
        if (temp >= 0 && temp < 40) {
            float x = ((temp - self.tempLower)/10);
            NSArray *lowerValue = [dictionaryValues objectForKey:self.key1];
            if ([lowerValue objectAtIndex:self.a] == nil || [lowerValue objectAtIndex:self.b] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float lower0 = [[lowerValue objectAtIndex:self.a] floatValue];
                float lower1 = [[lowerValue objectAtIndex:self.b] floatValue];
                float lower = (((lower1 - lower0)*x)+lower0);
                groundRoll = lower;
            }
            
            
            NSArray *lValue = [dictionaryObstacleValues objectForKey:self.key1];
            if ([lValue objectAtIndex:self.a] == nil || [lValue objectAtIndex:self.b] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float l0 = [[lValue objectAtIndex:self.a] floatValue];
                float l1 = [[lValue objectAtIndex:self.b] floatValue];
                float l = (((l1 - l0)*x)+l0);
                clearObstacle = l;
            }
            
        }
        if (temp == 40) {
            NSArray *lowerValue = [dictionaryValues objectForKey:self.key1];
            if ([lowerValue objectAtIndex:self.a] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float lower = [[lowerValue objectAtIndex:self.a] floatValue];
                groundRoll = lower;
            }
            
            NSArray *lValue = [dictionaryObstacleValues objectForKey:self.key1];
            if ([lValue objectAtIndex:self.a] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float l = [[lValue objectAtIndex:self.a] floatValue];
                clearObstacle = l;
            }
        }
        if (temp > 40) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data of 40 degrees Celsius" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
            [alertView show];
        }
    }
    
    if (pressureAltitude >= 0 && pressureAltitude < 10000) {
        if (temp < 0 ) {
            float y = ((pressureAltitude - self.pLower)/1000);
            NSArray *lowerValue = [dictionaryValues objectForKey:self.key1];
            NSArray *higherValue = [dictionaryValues objectForKey:self.key2];
            if ([lowerValue objectAtIndex:0] == nil || [higherValue objectAtIndex:0] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float lower = [[lowerValue objectAtIndex:0] floatValue];
                float higher = [[higherValue objectAtIndex:0] floatValue];
                groundRoll = (((higher - lower)*y)+lower);
            }
            
            
            NSArray *lValue = [dictionaryObstacleValues objectForKey:self.key1];
            NSArray *hValue = [dictionaryObstacleValues objectForKey:self.key2];
            if ([lValue objectAtIndex:0] == nil || [hValue objectAtIndex:0] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float l = [[lValue objectAtIndex:0] floatValue];
                float h = [[hValue objectAtIndex:0] floatValue];
                clearObstacle = (((h - l)*y)+l);
            }
        }
        
        
        if (temp >= 0 && temp < 40 ) {
            
            float y = ((pressureAltitude - self.pLower)/1000);
            float x = ((temp - self.tempLower)/10);
            NSArray *lowerValue = [dictionaryValues objectForKey:self.key1];
            NSArray *higherValue = [dictionaryValues objectForKey:self.key2];
            if ([lowerValue objectAtIndex:self.a] == nil || [lowerValue objectAtIndex:self.b] == nil || [higherValue objectAtIndex:self.a] == nil || [higherValue objectAtIndex:self.b] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float lower0 = [[lowerValue objectAtIndex:self.a] floatValue];
                float lower1 = [[lowerValue objectAtIndex:self.b] floatValue];
                float lower = (((lower1 - lower0)*x)+lower0);
                float higher0 = [[higherValue objectAtIndex:self.a] floatValue];
                float higher1 = [[higherValue objectAtIndex:self.b] floatValue];
                float higher = (((higher1 - higher0)*x)+higher0);
                groundRoll = (((higher - lower)*y)+lower);
            }
            
            NSArray *lValue = [dictionaryObstacleValues objectForKey:self.key1];
            NSArray *hValue = [dictionaryObstacleValues objectForKey:self.key2];
            if ([lValue objectAtIndex:self.a] == nil || [lValue objectAtIndex:self.b] == nil || [hValue objectAtIndex:self.a] == nil || [hValue objectAtIndex:self.b] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float l0 = [[lValue objectAtIndex:self.a] floatValue];
                float l1 = [[lValue objectAtIndex:self.b] floatValue];
                float l = (((l1 - l0)*x)+l0);
                float h0 = [[hValue objectAtIndex:self.a] floatValue];
                float h1 = [[hValue objectAtIndex:self.b] floatValue];
                float h = (((h1 - h0)*x)+h0);
                clearObstacle = (((h - l)*y)+l);
            }
        }
        if (temp == 40) {
            float y = ((pressureAltitude - self.pLower)/1000);
            NSArray *lowerValue = [dictionaryValues objectForKey:self.key1];
            NSArray *higherValue = [dictionaryValues objectForKey:self.key2];
            if ([lowerValue objectAtIndex:self.a] == nil || [higherValue objectAtIndex:self.a]){
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float lower = [[lowerValue objectAtIndex:self.a] floatValue];
                float higher = [[higherValue objectAtIndex:self.a] floatValue];
                groundRoll = (((higher - lower)*y)+lower);
            }
            
            
            NSArray *lValue = [dictionaryObstacleValues objectForKey:self.key1];
            NSArray *hValue = [dictionaryObstacleValues objectForKey:self.key2];
            if ([lValue objectAtIndex:self.a] == nil || [hValue objectAtIndex:self.a] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float l = [[lValue objectAtIndex:self.a] floatValue];
                float h = [[hValue objectAtIndex:self.a] floatValue];
                clearObstacle = (((h - l)*y)+l);
            }
            
        }
        if (temp > 40) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data of 40 degrees Celsius" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
            [alertView show];
        }
    }
    if (pressureAltitude == 10000) {
        if (temp < 0) {
            NSArray *lowerValue = [dictionaryValues objectForKey:self.key1];
            if ([lowerValue objectAtIndex:self.a] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float lower = [[lowerValue objectAtIndex:self.a] floatValue];
                groundRoll = lower;
            }
            
            
            NSArray *lValue = [dictionaryObstacleValues objectForKey:self.key1];
            if ([lValue objectAtIndex:self.a] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float l = [[lValue objectAtIndex:self.a] floatValue];
                clearObstacle = l;
            }
        }
        if (temp >= 0 && temp < 40) {
            float x = ((temp - self.tempLower)/10);
            NSArray *lowerValue = [dictionaryValues objectForKey:self.key1];
            if ([lowerValue objectAtIndex:self.a] == nil || [lowerValue objectAtIndex:self.b] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float lower0 = [[lowerValue objectAtIndex:self.a] floatValue];
                float lower1 = [[lowerValue objectAtIndex:self.b] floatValue];
                float lower = (((lower1 - lower0)*x)+lower0);
                groundRoll = lower;
            }
            
            
            NSArray *lValue = [dictionaryObstacleValues objectForKey:self.key1];
            if ([lValue objectAtIndex:self.a] == nil || [lValue objectAtIndex:self.b] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float l0 = [[lValue objectAtIndex:self.a] floatValue];
                float l1 = [[lValue objectAtIndex:self.b] floatValue];
                float l = (((l1 - l0)*x)+l0);
                clearObstacle = l;
            }
        }
        if (temp == 40) {
            NSArray *lowerValue = [dictionaryValues objectForKey:self.key1];
            if ([lowerValue objectAtIndex:self.a] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float lower = [[lowerValue objectAtIndex:self.a] floatValue];
                groundRoll = lower;
            }
            
            NSArray *lValue = [dictionaryObstacleValues objectForKey:self.key1];
            if ([lValue objectAtIndex:self.a] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float l = [[lValue objectAtIndex:self.a] floatValue];
                clearObstacle = l;
            }
        }
        if (temp > 40) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data of 40 degrees Celsius" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
            [alertView show];
        }
    }
    if (pressureAltitude > 10000) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Pressure Exceeds POH Data of 10,000 Feet" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
        [alertView show];
    }
    
    
    self.landingGroundRoll = groundRoll;
    self.landingClearObstacle = clearObstacle;
    
    [self.view endEditing:YES];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"landingGraph"]) {
        CDCirrusSR22TM3400LandingGraphViewController *controller = (CDCirrusSR22TM3400LandingGraphViewController *)segue.destinationViewController;
        controller.landingWeight = self.totalWeightLabel.text;
        controller.landingArm = self.totalArmLabel.text;
        controller.landingCG = self.landingCGLabel.text;
    }
    if ([segue.identifier isEqualToString:@"pdf"]) {
        CDCirrusSR22TM3400PDFViewController *controller = (CDCirrusSR22TM3400PDFViewController *)segue.destinationViewController;
        controller.totalWeight = self.totalWeightText;
        controller.pilotWeight = self.pilotWeightText;
        controller.frontSeat = self.frontSeatWeightText;
        controller.rearSeat1 = self.rearSeat1WeightText;
        controller.rearSeat2 = self.rearSeat2WeightText;
        controller.bagArea1 = self.bagAreaWeightText;
        controller.fuelGal = self.fuelGalText;
        controller.fuelWeight = self.fuelWeightText;
        controller.emptyWeight = self.emptyWeightText;
        controller.emptyArm = self.emptyArmText;
        controller.landingFuelGal = self.landingFuelGallons;
        controller.landingFuelWeight = self.landingFuelWeight;
        controller.landingTotalWeight = self.totalWeightLabel.text;
        controller.totalArm = self.totalArmText;
        controller.totalMoment = self.totalMomentText;
        controller.landingTotalArm = self.totalArmLabel.text;
        controller.landingTotalMoment = self.totalMomentLabel.text;
        controller.landingCG = self.landingCGLabel.text;
        controller.takeoffCG = self.takeoffCGLimitsText;
        controller.landingGroundRoll = self.landingGroundRollLabel.text;
        controller.landingClearObstacle = self.landingClearObstacleLabel.text;
        controller.takeoffGroundRoll = self.takeoffGroundRoll;
        controller.takeoffObstacleClearance = self.takeoffClearObstacle;
        controller.takeoffDensityAltitude = self.takeoffDensityAltitude;
        controller.landingDensityAltitude = self.landingDensityAltitude;
        controller.nNumber = self.nNumberText;
        controller.ldgManeurvingSpeed = self.landingManeuveringSpeedText;
        controller.toManeuveringSpeed = self.takeoffManeuveringSpeedText;
        controller.landingWindDirection = self.windDirectionTextField.text;
        controller.landingWindSpeed = self.windSpeedTextField.text;
        controller.landingRunway = self.runwayTextField.text;
        controller.takeoffWindDirection = self.takeoffWindDirection;
        controller.takeoffWindSpeed = self.takeoffWindSpeed;
        controller.takeoffRunway = self.takeoffRunway;
        controller.landingWindComponent = self.landingHeadWindComponent;
        controller.takeofWindComponent = self.takeoffHeadWindComponent;
        controller.landingCrossWindComponent = self.landingCrossWindComponent;
        controller.takeoffCrossWindComponent = self.takeoffCrosswindComponent;
    }
    
    
}



@end
