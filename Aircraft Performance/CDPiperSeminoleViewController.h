//
//  CDPiperSeminoleViewController.h
//  Aircraft Performance
//
//  Created by Chip Duffield on 10/9/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDPiperSeminoleViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *pilotWeightTextField;
@property (weak, nonatomic) IBOutlet UITextField *frontSeatWeightTextField;
@property (weak, nonatomic) IBOutlet UITextField *rearSeat1WeightTextField;
@property (weak, nonatomic) IBOutlet UITextField *rearSeat2WeightTextField;
@property (weak, nonatomic) IBOutlet UITextField *bagAreaWeightTextField;
@property (weak, nonatomic) IBOutlet UITextField *fuelGalTextField;

@property (nonatomic) NSString *emptyWeightText;
@property (nonatomic) NSString *emptyArmText;
@property (nonatomic) NSString *nNumberText;
@property (nonatomic) float bagArea1;
@property (nonatomic) float fuelGal;

@end
