//
//  CDCessna172RViewController.m
//  Aircraft Performance
//
//  Created by Chip Duffield on 6/29/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import "CDCessna172RViewController.h"
#import "CDCessna172RCalculationViewController.h"

@interface CDCessna172RViewController ()

@end

@implementation CDCessna172RViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.automaticallyAdjustsScrollViewInsets = false; 
   
    

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Navigation

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if ([identifier isEqualToString:@"cessna172RCalculation"]) {
        self.bagArea1 = [self.bagArea1TextField.text floatValue];
        self.bagArea2 = [self.bagArea2TextField.text floatValue];
        self.fuelGal = [self.fuelGalTextField.text floatValue];
        
        if (self.fuelGal > 53.0) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"53 gallons Max" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
            [alertView show];
            
            return NO;
        }
        if (self.bagArea1 > 120.0) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Max Bag Area 1 Limit is 120 lb" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
            [alertView show];
            
            return  NO;
        }
        if (self.bagArea2 > 50.0) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Max Bag Area 2 limit is 50 lb" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
            [alertView show];
            
            return  NO; 
        }
        
    }
    
    return  YES;
}


// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
   
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"cessna172RCalculation"]) {
        CDCessna172RCalculationViewController *controller = segue.destinationViewController;
        controller.pilotWeight = self.pilotWeightTextField.text;
        controller.frontSeatWeight = self.frontSeatWeightTextField.text;
        controller.rearSeat1Weight = self.rearSeat1TextField.text;
        controller.rearSeat2Weight = self.rearSeat2TextField.text;
        controller.bagArea1Weight = self.bagArea1TextField.text;
        controller.bagArea2Weight = self.bagArea2TextField.text;
        controller.fuelGal = self.fuelGalTextField.text;
        controller.emptyWeight = self.emptyWeight;
        controller.arm = self.arm;
        controller.nNumber = self.nNumber;
    }
}




@end
