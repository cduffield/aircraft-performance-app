//
//  CDCirrusSR20M3000PDFViewController.h
//  Aircraft Performance
//
//  Created by Chip Duffield on 8/2/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDCirrusSR20M3000PDFViewController : UIViewController <UIDocumentInteractionControllerDelegate>

-(void)drawText:(NSString*)textToDraw inFrame:(CGRect)frameRect;
-(void)drawPDF:(NSString*)fileName;
- (IBAction)openWith:(id)sender;

@property (nonatomic) NSString *totalWeight;
@property (nonatomic) NSString *totalArm;
@property (nonatomic) NSString *totalMoment;
@property (nonatomic) NSString *pilotWeight;
@property (nonatomic) NSString *frontSeat;
@property (nonatomic) NSString *rearSeat1;
@property (nonatomic) NSString *rearSeat2;
@property (nonatomic) NSString *bagArea1;
@property (nonatomic) NSString *bagArea2;
@property (nonatomic) NSString *fuelGal;
@property (nonatomic) NSString *fuelWeight;
@property (nonatomic) NSString *emptyWeight;
@property (nonatomic) NSString *emptyArm;
@property (nonatomic) NSString *landingTotalWeight;
@property (nonatomic) NSString *landingFuelGal;
@property (nonatomic) NSString *landingFuelWeight;
@property (nonatomic) NSString *landingTotalArm;
@property (nonatomic) NSString *landingTotalMoment;
@property (nonatomic) NSString *takeoffCG;
@property (nonatomic) NSString *landingCG;
@property (nonatomic) NSString *takeoffGroundRoll;
@property (nonatomic) NSString *takeoffObstacleClearance;
@property (nonatomic) NSString *takeoffDensityAltitude;
@property (nonatomic) NSString *landingGroundRoll;
@property (nonatomic) NSString *landingClearObstacle;
@property (nonatomic) NSString *landingDensityAltitude;
@property (nonatomic) NSString *ldgManeurvingSpeed;
@property (nonatomic) NSString *toManeuveringSpeed;
@property (nonatomic) NSString *nNumber;
@property (nonatomic) NSString *todaysDate;
@property (nonatomic) NSString *takeoffWindSpeed;
@property (nonatomic) NSString *takeoffWindDirection;
@property (nonatomic) NSString *takeoffRunway;
@property (nonatomic) NSString *takeofWindComponent;
@property (nonatomic) NSString *takeoffCrossWindComponent;
@property (nonatomic) NSString *landingWindSpeed;
@property (nonatomic) NSString *landingWindDirection;
@property (nonatomic) NSString *landingRunway;
@property (nonatomic) NSString *landingWindComponent;
@property (nonatomic) NSString *landingCrossWindComponent;

@property (nonatomic) UIDocumentInteractionController *docController;
@property (nonatomic) NSURL *url;

@end
