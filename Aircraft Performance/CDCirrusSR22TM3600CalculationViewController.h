//
//  CDCirrusSR22TM3600CalculationViewController.h
//  Aircraft Performance
//
//  Created by Chip Duffield on 8/20/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDCirrusSR22TM3600CalculationViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *emptyWeightLabel;
@property (weak, nonatomic) IBOutlet UILabel *pilotWeightLabel;
@property (weak, nonatomic) IBOutlet UILabel *frontSeatWeightLabel;
@property (weak, nonatomic) IBOutlet UILabel *rearSeat1WeightLabel;
@property (weak, nonatomic) IBOutlet UILabel *rearSeat2WeightLabel;
@property (weak, nonatomic) IBOutlet UILabel *bagAreaWeightLabel;
@property (weak, nonatomic) IBOutlet UILabel *fuelWeightLabel;
@property (weak, nonatomic) IBOutlet UILabel *fuelGalLabel;
@property (weak, nonatomic) IBOutlet UILabel *taxiBurnWeightLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalWeightLabel;
@property (weak, nonatomic) IBOutlet UILabel *emptyArmLabel;
@property (weak, nonatomic) IBOutlet UILabel *pilotArmLabel;
@property (weak, nonatomic) IBOutlet UILabel *frontSeatArmLabel;
@property (weak, nonatomic) IBOutlet UILabel *rearSeat1ArmLabel;
@property (weak, nonatomic) IBOutlet UILabel *rearSeat2ArmLabel;
@property (weak, nonatomic) IBOutlet UILabel *bagAreaArmLabel;
@property (weak, nonatomic) IBOutlet UILabel *fuelArmLabel;
@property (weak, nonatomic) IBOutlet UILabel *taxiBurnArmLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalArmLabel;
@property (weak, nonatomic) IBOutlet UILabel *emptyMomentLabel;
@property (weak, nonatomic) IBOutlet UILabel *pilotMomentLabel;
@property (weak, nonatomic) IBOutlet UILabel *frontSeatMomentLabel;
@property (weak, nonatomic) IBOutlet UILabel *rearSeat1MomentLabel;
@property (weak, nonatomic) IBOutlet UILabel *rearSeat2MomentLabel;
@property (weak, nonatomic) IBOutlet UILabel *bagAreaMomentLabel;
@property (weak, nonatomic) IBOutlet UILabel *fuelMomentLabel;
@property (weak, nonatomic) IBOutlet UILabel *taxiBurnMomentLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalMomentLabel;
@property (weak, nonatomic) IBOutlet UILabel *cgLabel;
@property (weak, nonatomic) IBOutlet UILabel *maneuveringSpeedLabel;

@property (nonatomic) NSString *nNumberText;
@property (nonatomic) NSString *emptyWeightText;
@property (nonatomic) NSString *emptyArmText;
@property (nonatomic) NSString *pilotWeightText;
@property (nonatomic) NSString *frontSeatWeightText;
@property (nonatomic) NSString *rearSeat1WeightText;
@property (nonatomic) NSString *rearSeat2WeightText;
@property (nonatomic) NSString *bagAreaWeightText;
@property (nonatomic) NSString *fuelGalText;

@property (nonatomic) float emptyWeight;
@property (nonatomic) float pilotWeight;
@property (nonatomic) float frontSeatWeight;
@property (nonatomic) float rearSeat1Weight;
@property (nonatomic) float rearSeat2Weight;
@property (nonatomic) float bagAreaWeight;
@property (nonatomic) float fuelGal;
@property (nonatomic) float fuelWeight;
@property (nonatomic) float taxiBurnWeight;
@property (nonatomic) float totalWeight;
@property (nonatomic) float emptyArm;
@property (nonatomic) float pilotArm;
@property (nonatomic) float frontSeatArm;
@property (nonatomic) float rearSeat1Arm;
@property (nonatomic) float rearSeat2Arm;
@property (nonatomic) float bagAreaArm;
@property (nonatomic) float fuelArm;
@property (nonatomic) float taxiBurnArm;
@property (nonatomic) float totalArm;
@property (nonatomic) float emptyMoment;
@property (nonatomic) float pilotMoment;
@property (nonatomic) float frontSeatMoment;
@property (nonatomic) float rearSeat1Moment;
@property (nonatomic) float rearSeat2Moment;
@property (nonatomic) float bagAreaMoment;
@property (nonatomic) float fuelMoment;
@property (nonatomic) float taxiBurnMoment;
@property (nonatomic) float totalMoment;

@property (nonatomic) float m;
@property (nonatomic) float b;
@property (nonatomic) float minM;
@property (nonatomic) float minB;
@property (nonatomic) NSString *cgLimits;


@end
