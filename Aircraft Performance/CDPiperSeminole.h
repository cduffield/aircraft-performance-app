//
//  CDPiperSeminole.h
//  Aircraft Performance
//
//  Created by Chip Duffield on 10/9/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface CDPiperSeminole : NSManagedObject

@property (nonatomic, retain) NSString * nNumber;
@property (nonatomic, retain) NSString * arm;
@property (nonatomic, retain) NSString * emptyWeight;

@end
