//
//  CDCessna172SList.h
//  Aircraft Performance
//
//  Created by Chip Duffield on 9/9/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface CDCessna172SList : NSManagedObject

@property (nonatomic, retain) NSString * arm;
@property (nonatomic, retain) NSString * emptyWeight;
@property (nonatomic, retain) NSString * nNumber;

@end
