//
//  CDPiperSeminoleLandingGraphViewController.h
//  Aircraft Performance
//
//  Created by Chip Duffield on 10/9/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDPiperSeminoleLandingGraphViewController : UIViewController

@property (nonatomic) NSString *landingArm;
@property (nonatomic) NSString *landingWeight;
@property (nonatomic) NSString *landingCG;


@end
