//
//  CDCirrusSR20M3050TakeoffDataViewController.m
//  Aircraft Performance
//
//  Created by Chip Duffield on 8/18/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import "CDCirrusSR20M3050TakeoffDataViewController.h"
#import "CDCirrusSR20M3050LandingDataViewController.h"

@interface CDCirrusSR20M3050TakeoffDataViewController ()

@end

@implementation CDCirrusSR20M3050TakeoffDataViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = false;
    
    self.totalWeightLabel.text = self.totalWeightText;
    
    NSArray *seaLevel = [[NSArray alloc] initWithObjects:@787,@850,@915,@983,@1054,nil];
    NSArray *p1000 = [[NSArray alloc] initWithObjects:@864,@933,@1005,@1079,@1157,nil];
    NSArray *p2000 = [[NSArray alloc] initWithObjects:@949,@1025,@1104,@1186,@1271,nil];
    NSArray *p3000 = [[NSArray alloc] initWithObjects:@1043,@1126,@1213,@1304,@1398,nil];
    NSArray *p4000 = [[NSArray alloc] initWithObjects:@1147,@1239,@1335,@1434,@1537,nil];
    NSArray *p5000 = [[NSArray alloc] initWithObjects:@1263,@1364,@1469,@1579,@1693,nil];
    NSArray *p6000 = [[NSArray alloc] initWithObjects:@1392,@1503,@1619,@1739,@1865,nil];
    NSArray *p7000 = [[NSArray alloc] initWithObjects:@1534,@1657,@1785,nil,nil,nil];
    NSArray *p8000 = [[NSArray alloc] initWithObjects:@1692,@1828,@1969,nil,nil,nil];
    NSArray *p9000 = [[NSArray alloc] initWithObjects:@1868,@2018,@2174,nil,nil,nil];
    NSArray *p10000 = [[NSArray alloc] initWithObjects:@2064,@2229,nil,nil,nil,nil];
    self.toDistance2500 = [[NSDictionary alloc] initWithObjectsAndKeys:seaLevel,@0,p1000,@1000,p2000,@2000,p3000,@3000,p4000,@4000,p5000,@5000,p6000,@6000,p7000,@7000,p8000,@8000,p9000,@9000,p10000,@10000, nil];
    
    NSArray *seaLevelat3050 = [[NSArray alloc] initWithObjects:@1319,@1424,@1534,@1648,@1747,nil];
    NSArray *p1000at3050 = [[NSArray alloc] initWithObjects:@1448,@1563,@1684,@1809,@1940,nil];
    NSArray *p2000at3050 = [[NSArray alloc] initWithObjects:@1590,@1717,@1850,@1988,@2131,nil];
    NSArray *p3000at3050 = [[NSArray alloc] initWithObjects:@1748,@1888,@2034,@2185,@2343,nil];
    NSArray *p4000at3050 = [[NSArray alloc] initWithObjects:@1923,@2077,@2237,@2404,@2577,nil];
    NSArray *p5000at3050 = [[NSArray alloc] initWithObjects:@2117,@2287,@2463,@2647,@2837,nil];
    NSArray *p6000at3050 = [[NSArray alloc] initWithObjects:@2333,@2519,@2714,@2916,@3126,nil];
    NSArray *p7000at3050 = [[NSArray alloc] initWithObjects:@3572,@2777,@2992,nil,nil,nil];
    NSArray *p8000at3050 = [[NSArray alloc] initWithObjects:@2837,@3064,@3300,nil,nil,nil];
    NSArray *p9000at3050 = [[NSArray alloc] initWithObjects:@3132,@3383,@3644,nil,nil,nil];
    NSArray *p10000at3050 = [[NSArray alloc] initWithObjects:@3460,@3737,nil,nil,nil,nil];
    self.toDistance3050 = [[NSDictionary alloc] initWithObjectsAndKeys:seaLevelat3050,@0,p1000at3050,@1000,p2000at3050,@2000,p3000at3050,@3000,p4000at3050,@4000,p5000at3050,@5000,p6000at3050,@6000,p7000at3050,@7000,p8000at3050,@8000,p9000at3050,@9000,p10000at3050,@10000, nil];
    
    NSArray *pressureSeaLevel = [[NSArray alloc] initWithObjects:@1215,@1306,@1400,@1497,@1598,nil];
    NSArray *pressure1000 = [[NSArray alloc] initWithObjects:@1329,@1428,@1531,@1637,@1748,nil];
    NSArray *pressure2000 = [[NSArray alloc] initWithObjects:@1454,@1563,@1676,@1792,@1913,nil];
    NSArray *pressure3000 = [[NSArray alloc] initWithObjects:@1593,@1712,@1835,@1963,@2095,nil];
    NSArray *pressure4000 = [[NSArray alloc] initWithObjects:@1745,@1876,@2011,@2151,@1914,nil];
    NSArray *pressure5000 = [[NSArray alloc] initWithObjects:@1914,@2057,@2206,@2359,@2101,nil];
    NSArray *pressure6000 = [[NSArray alloc] initWithObjects:@2101,@2258,@2421,@2589,@2307,nil];
    NSArray *pressure7000 = [[NSArray alloc] initWithObjects:@2307,@2479,@2658,nil,nil,nil];
    NSArray *pressure8000 = [[NSArray alloc] initWithObjects:@2535,@2725,@2922,nil,nil,nil];
    NSArray *pressure9000 = [[NSArray alloc] initWithObjects:@2788,@2997,@3213,nil,nil,nil];
    NSArray *pressure10000 = [[NSArray alloc] initWithObjects:@3068,@3298,nil,nil,nil,nil];
    self.toDistanceObstacle2500 = [[NSDictionary alloc] initWithObjectsAndKeys:pressureSeaLevel,@0,pressure1000,@1000,pressure2000,@2000,pressure3000,@3000,pressure4000,@4000,pressure5000,@5000,pressure6000,@6000,pressure7000,@7000,pressure8000,@8000,pressure9000,@9000,pressure10000,@10000, nil];
    
    NSArray *pressureSeaLevelat3050 = [[NSArray alloc] initWithObjects:@1996,@2145,@2300,@2460,@2626,nil];
    NSArray *pressure1000at3050 = [[NSArray alloc] initWithObjects:@2183,@2346,@2515,@2691,@2872,nil];
    NSArray *pressure2000at3050 = [[NSArray alloc] initWithObjects:@2389,@2568,@2753,@2945,@3144,nil];
    NSArray *pressure3000at3050 = [[NSArray alloc] initWithObjects:@2616,@2812,@3015,@3226,@3444,nil];
    NSArray *pressure4000at3050 = [[NSArray alloc] initWithObjects:@2868,@3082,@3305,@3536,@3145,nil];
    NSArray *pressure5000at3050 = [[NSArray alloc] initWithObjects:@3145,@3381,@3625,@3879,@3452,nil];
    NSArray *pressure6000at3050 = [[NSArray alloc] initWithObjects:@3452,@3711,@3980,@4258,@3792,nil];
    NSArray *pressure7000at3050 = [[NSArray alloc] initWithObjects:@3792,@4076,@4371,nil,nil,nil];
    NSArray *pressure8000at3050 = [[NSArray alloc] initWithObjects:@4167,@4480,@4805,nil,nil,nil];
    NSArray *pressure9000at3050 = [[NSArray alloc] initWithObjects:@4584,@4928,@5285,nil,nil,nil];
    NSArray *pressure10000at3050 = [[NSArray alloc] initWithObjects:@5045,@5424,nil,nil,nil,nil];
    self.toDistanceObstacle3050 = [[NSDictionary alloc] initWithObjectsAndKeys:pressureSeaLevelat3050,@0,pressure1000at3050,@1000,pressure2000at3050,@2000,pressure3000at3050,@3000,pressure4000at3050,@4000,pressure5000at3050,@5000,pressure6000at3050,@6000,pressure7000at3050,@7000,pressure8000at3050,@8000,pressure9000at3050,@9000,pressure10000at3050,@10000, nil];
    
    
}




- (IBAction)calculate:(id)sender {
    
    float windDirection = [self.windDirectionTextField.text floatValue];
    float windSpeed = [self.windSpeedTextField.text floatValue];
    float runwayDirection = ([self.runwayTextField.text floatValue]*10);
    
    if (runwayDirection > 360) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please enter runway without last Digit. Use 9 instead of 090" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
        [alertView show];
        
    } else if (windDirection > 360) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error'" message:@"Wind Direction can not be greater that 360" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
        [alertView show];
    } else {
        
        //constants for both
        float pressure = [self.pressureTextField.text floatValue];
        float temp = [self.temperatureTextField.text floatValue];
        float fieldElevation = [self.fieldElevationTextField.text floatValue];
        float pressureAltitude = (((29.92 - pressure)*1000) + fieldElevation);
        float isaTemp = (15 -((pressureAltitude/1000) * 1.98));
        float totalWeight = [self.totalWeightText floatValue];
        
        
        float groundRollCorrectedForWind;
        float clearObstacleCorrectedForWind;
        
        //calculate density altitude
        float densityAltitude = (pressureAltitude + (118.8 * (temp - isaTemp)));
        self.densityAltitudeLabel.text = [[NSString alloc] initWithFormat:@"%.f",densityAltitude];
        
        if (totalWeight <= 2500) {
            [self calculateDistances:temp pressure:pressureAltitude windDirection:windDirection windSpeed:windSpeed runwayDirection:runwayDirection dictionary:self.toDistance2500 dictionaryObstacle:self.toDistanceObstacle2500 weight:totalWeight];
            
            if ((windDirection - runwayDirection) <= 90) {
                float angle = windDirection - runwayDirection;
                float windComponent = cosf(angle*M_PI/180);
                float headwind = windSpeed * windComponent;
                float crosswindComponent = sinf(angle*M_PI/180);
                float crosswind = windSpeed * crosswindComponent;
                float crosswindAbs = fabsf(crosswind);
                self.takeoffCrossWindComponent = [[NSString alloc] initWithFormat:@"%.f",crosswindAbs];
                self.headwindComponent = [[NSString alloc] initWithFormat:@"%.f",headwind];
                int headwindInt = (int)floorf(headwind);
                int headwindCorrection = ((headwindInt/13)*.1);
                groundRollCorrectedForWind = self.groundroll * (1 - headwindCorrection);
                clearObstacleCorrectedForWind = self.clearObstacle * (1- headwindCorrection);
                self.takeoffGroundRollLabel.text = [[NSString alloc] initWithFormat:@"%.f",groundRollCorrectedForWind];
                self.takeoffClearObstacleLabel.text = [[NSString alloc] initWithFormat:@"%.f",clearObstacleCorrectedForWind];
            } else {
                float angle = windDirection - runwayDirection;
                float windComponent = cosf(angle*M_PI/180);
                float tailwind = windSpeed * windComponent;
                float crosswindComponent = sinf(angle*M_PI/180);
                float crosswind = windSpeed * crosswindComponent;
                self.takeoffCrossWindComponent = [[NSString alloc] initWithFormat:@"%.f",crosswind];
                self.headwindComponent = [[NSString alloc] initWithFormat:@"%.f",tailwind];
                int tailwindInt = (int)floorf(tailwind);
                int tailwindCorrection = ((tailwindInt/2)*.1);
                groundRollCorrectedForWind = self.groundroll * (1 + tailwindCorrection);
                clearObstacleCorrectedForWind = self.clearObstacle * (1 + tailwindCorrection);
                self.takeoffGroundRollLabel.text = [[NSString alloc] initWithFormat:@"%.f",groundRollCorrectedForWind];
                self.takeoffClearObstacleLabel.text = [[NSString alloc] initWithFormat:@"%.f",clearObstacleCorrectedForWind];
                
            }
            
        } else {
            [self calculateDistances:temp pressure:pressureAltitude windDirection:windDirection windSpeed:windSpeed runwayDirection:runwayDirection dictionary:self.toDistance2500 dictionaryObstacle:self.toDistanceObstacle2500 weight:2500];
            [self calculateDistances:temp pressure:pressureAltitude windDirection:windDirection windSpeed:windSpeed runwayDirection:runwayDirection dictionary:self.toDistance3050 dictionaryObstacle:self.toDistanceObstacle3050 weight:3050];
            
            float y = ((totalWeight - 2500)/550);
            self.groundroll = (((self.groundroll3000 - self.groundroll2500)*y)+self.groundroll2500);
            self.clearObstacle = (((self.clearObstacle3000 - self.clearObstacle2500)*y)+self.clearObstacle2500);
            
            if ((windDirection - runwayDirection) <= 90) {
                float angle = windDirection - runwayDirection;
                float windComponent = cosf(angle*M_PI/180);
                float headwind = windSpeed * windComponent;
                float crosswindComponent = sinf(angle*M_PI/180);
                float crosswind = windSpeed * crosswindComponent;
                float crosswindAbs = fabsf(crosswind);
                self.takeoffCrossWindComponent = [[NSString alloc] initWithFormat:@"%.f",crosswindAbs];
                self.headwindComponent = [[NSString alloc] initWithFormat:@"%.f",headwind];
                int headwindInt = (int)floorf(headwind);
                int headwindCorrection = ((headwindInt/9)*.1);
                groundRollCorrectedForWind = self.groundroll * (1 - headwindCorrection);
                clearObstacleCorrectedForWind = self.clearObstacle * (1- headwindCorrection);
                self.takeoffGroundRollLabel.text = [[NSString alloc] initWithFormat:@"%.f",groundRollCorrectedForWind];
                self.takeoffClearObstacleLabel.text = [[NSString alloc] initWithFormat:@"%.f",clearObstacleCorrectedForWind];
            } else {
                float angle = windDirection - runwayDirection;
                float windComponent = cosf(angle*M_PI/180);
                float tailwind = windSpeed * windComponent;
                float crosswindComponent = sinf(angle*M_PI/180);
                float crosswind = windSpeed * crosswindComponent;
                self.takeoffCrossWindComponent = [[NSString alloc] initWithFormat:@"%.f",crosswind];
                self.headwindComponent = [[NSString alloc] initWithFormat:@"%.f",tailwind];
                int tailwindInt = (int)floorf(tailwind);
                int tailwindCorrection = ((tailwindInt/2)*.1);
                groundRollCorrectedForWind = self.groundroll * (1 + tailwindCorrection);
                clearObstacleCorrectedForWind = self.clearObstacle * (1 + tailwindCorrection);
                self.takeoffGroundRollLabel.text = [[NSString alloc] initWithFormat:@"%.f",groundRollCorrectedForWind];
                self.takeoffClearObstacleLabel.text = [[NSString alloc] initWithFormat:@"%.f",clearObstacleCorrectedForWind];
                
            }
            
        }
        
        
        
        
    }
}

- (void)calculateTempIndex:(float)temp {
    if (temp < 0) {
        self.a = 0;
    }
    if (temp >= 0 && temp < 10) {
        self.a = 0;
        self.b = 1;
        self.tempLower = 0;
    }
    if (temp >= 10 && temp < 20) {
        self.a = 1;
        self.b = 2;
        self.tempLower = 10;
    }
    if (temp >= 20 && temp < 30) {
        self.a = 2;
        self.b = 3;
        self.tempLower = 20;
    }
    if (temp >= 30 && temp < 40) {
        self.a = 3;
        self.b = 4;
        self.tempLower = 30;
    }
    if (temp == 40) {
        self.a = 4;
    }
}

- (void)pressureAltitudeKeys:(int)pressureAltitude {
    if (pressureAltitude < 0) {
        self.key1 = @0;
    }
    if (pressureAltitude >= 0 && pressureAltitude < 1000) {
        self.key1 = @0;
        self.key2 = @1000;
        self.pLower = 0;
        self.pHigher = 1000;
    }
    if (pressureAltitude >= 1000 && pressureAltitude < 2000) {
        self.key1 = @1000;
        self.key2 = @2000;
        self.pLower = 1000;
        self.pHigher = 2000;
    }
    if (pressureAltitude >= 2000 && pressureAltitude < 3000) {
        self.key1 = @2000;
        self.key2 = @3000;
        self.pLower = 2000;
        self.pHigher = 3000;
    }
    if (pressureAltitude >= 3000 && pressureAltitude < 4000) {
        self.key1 = @3000;
        self.key2 = @4000;
        self.pLower = 3000;
        self.pHigher = 4000;
    }
    if (pressureAltitude >= 4000 && pressureAltitude < 5000) {
        self.key1 = @4000;
        self.key2 = @5000;
        self.pLower = 4000;
        self.pHigher = 5000;
    }
    if (pressureAltitude >= 5000 && pressureAltitude < 6000) {
        self.key1 = @5000;
        self.key2 = @6000;
        self.pLower = 5000;
        self.pHigher = 6000;
    }
    if (pressureAltitude >= 6000 && pressureAltitude < 7000) {
        self.key1 = @6000;
        self.key2 = @7000;
        self.pLower = 6000;
        self.pHigher = 7000;
    }
    if (pressureAltitude >= 7000 && pressureAltitude < 8000) {
        self.key1 = @7000;
        self.key2 = @8000;
        self.pLower = 7000;
        self.pHigher = 8000;
    }
    if (pressureAltitude >= 8000 && pressureAltitude < 9000) {
        self.key1 = @8000;
        self.key2 = @9000;
        self.pLower = 8000;
        self.pHigher = 9000;
    }
    if (pressureAltitude >= 9000 && pressureAltitude < 10000) {
        self.key1 = @9000;
        self.key2 = @10000;
        self.pLower = 9000;
        self.pHigher = 10000;
    }
    if (pressureAltitude == 10000) {
        self.key1 = @10000;
    }
    else {
        
    }
}

- (void)calculateDistances:(float)temp pressure:(float)pressureAltitude windDirection:(float)windDirection windSpeed:(float)windSpeed runwayDirection:(float)runwayDirection dictionary:(NSDictionary *)dictionaryValues dictionaryObstacle:(NSDictionary *)dictionaryObstacleValues  weight:(float)weight {
    [self calculateTempIndex:temp];
    [self pressureAltitudeKeys:pressureAltitude];
    float groundRoll;
    float clearObstacle;
    
    
    if (pressureAltitude < 0) {
        if (temp < 0) {
            NSArray *lowerValue = [dictionaryValues objectForKey:self.key1];
            if ([lowerValue objectAtIndex:self.a] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float lower = [[lowerValue objectAtIndex:self.a] floatValue];
                groundRoll = lower;
            }
            
            
            NSArray *lValue = [dictionaryObstacleValues objectForKey:self.key1];
            if ([lValue objectAtIndex:self.a] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float l = [[lValue objectAtIndex:self.a] floatValue];
                clearObstacle = l;
            }
        }
        if (temp >= 0 && temp < 40) {
            float x = ((temp - self.tempLower)/10);
            NSArray *lowerValue = [dictionaryValues objectForKey:self.key1];
            if ([lowerValue objectAtIndex:self.a] == nil || [lowerValue objectAtIndex:self.b] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float lower0 = [[lowerValue objectAtIndex:self.a] floatValue];
                float lower1 = [[lowerValue objectAtIndex:self.b] floatValue];
                float lower = (((lower1 - lower0)*x)+lower0);
                groundRoll = lower;
            }
            
            
            NSArray *lValue = [dictionaryObstacleValues objectForKey:self.key1];
            if ([lValue objectAtIndex:self.a] == nil || [lValue objectAtIndex:self.b] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float l0 = [[lValue objectAtIndex:self.a] floatValue];
                float l1 = [[lValue objectAtIndex:self.b] floatValue];
                float l = (((l1 - l0)*x)+l0);
                clearObstacle = l;
            }
            
        }
        if (temp == 40) {
            NSArray *lowerValue = [dictionaryValues objectForKey:self.key1];
            if ([lowerValue objectAtIndex:self.a] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float lower = [[lowerValue objectAtIndex:self.a] floatValue];
                groundRoll = lower;
            }
            
            NSArray *lValue = [dictionaryObstacleValues objectForKey:self.key1];
            if ([lValue objectAtIndex:self.a] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float l = [[lValue objectAtIndex:self.a] floatValue];
                clearObstacle = l;
            }
        }
        if (temp > 40) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data of 40 degrees Celsius" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
            [alertView show];
        }
    }
    
    if (pressureAltitude >= 0 && pressureAltitude < 10000) {
        if (temp < 0 ) {
            float y = ((pressureAltitude - self.pLower)/1000);
            NSArray *lowerValue = [dictionaryValues objectForKey:self.key1];
            NSArray *higherValue = [dictionaryValues objectForKey:self.key2];
            if ([lowerValue objectAtIndex:0] == nil || [higherValue objectAtIndex:0] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float lower = [[lowerValue objectAtIndex:0] floatValue];
                float higher = [[higherValue objectAtIndex:0] floatValue];
                groundRoll = (((higher - lower)*y)+lower);
            }
            
            
            NSArray *lValue = [dictionaryObstacleValues objectForKey:self.key1];
            NSArray *hValue = [dictionaryObstacleValues objectForKey:self.key2];
            if ([lValue objectAtIndex:0] == nil || [hValue objectAtIndex:0] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float l = [[lValue objectAtIndex:0] floatValue];
                float h = [[hValue objectAtIndex:0] floatValue];
                clearObstacle = (((h - l)*y)+l);
            }
        }
        
        
        if (temp >= 0 && temp < 40 ) {
            
            float y = ((pressureAltitude - self.pLower)/1000);
            float x = ((temp - self.tempLower)/10);
            NSArray *lowerValue = [dictionaryValues objectForKey:self.key1];
            NSArray *higherValue = [dictionaryValues objectForKey:self.key2];
            if ([lowerValue objectAtIndex:self.a] == nil || [lowerValue objectAtIndex:self.b] == nil || [higherValue objectAtIndex:self.a] == nil || [higherValue objectAtIndex:self.b] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float lower0 = [[lowerValue objectAtIndex:self.a] floatValue];
                float lower1 = [[lowerValue objectAtIndex:self.b] floatValue];
                float lower = (((lower1 - lower0)*x)+lower0);
                float higher0 = [[higherValue objectAtIndex:self.a] floatValue];
                float higher1 = [[higherValue objectAtIndex:self.b] floatValue];
                float higher = (((higher1 - higher0)*x)+higher0);
                groundRoll = (((higher - lower)*y)+lower);
            }
            
            NSArray *lValue = [dictionaryObstacleValues objectForKey:self.key1];
            NSArray *hValue = [dictionaryObstacleValues objectForKey:self.key2];
            if ([lValue objectAtIndex:self.a] == nil || [lValue objectAtIndex:self.b] == nil || [hValue objectAtIndex:self.a] == nil || [hValue objectAtIndex:self.b] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float l0 = [[lValue objectAtIndex:self.a] floatValue];
                float l1 = [[lValue objectAtIndex:self.b] floatValue];
                float l = (((l1 - l0)*x)+l0);
                float h0 = [[hValue objectAtIndex:self.a] floatValue];
                float h1 = [[hValue objectAtIndex:self.b] floatValue];
                float h = (((h1 - h0)*x)+h0);
                clearObstacle = (((h - l)*y)+l);
            }
        }
        if (temp == 40) {
            float y = ((pressureAltitude - self.pLower)/1000);
            NSArray *lowerValue = [dictionaryValues objectForKey:self.key1];
            NSArray *higherValue = [dictionaryValues objectForKey:self.key2];
            if ([lowerValue objectAtIndex:self.a] == nil || [higherValue objectAtIndex:self.a]){
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float lower = [[lowerValue objectAtIndex:self.a] floatValue];
                float higher = [[higherValue objectAtIndex:self.a] floatValue];
                groundRoll = (((higher - lower)*y)+lower);
            }
            
            
            NSArray *lValue = [dictionaryObstacleValues objectForKey:self.key1];
            NSArray *hValue = [dictionaryObstacleValues objectForKey:self.key2];
            if ([lValue objectAtIndex:self.a] == nil || [hValue objectAtIndex:self.a] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float l = [[lValue objectAtIndex:self.a] floatValue];
                float h = [[hValue objectAtIndex:self.a] floatValue];
                clearObstacle = (((h - l)*y)+l);
            }
            
        }
        if (temp > 40) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data of 40 degrees Celsius" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
            [alertView show];
        }
    }
    if (pressureAltitude == 10000) {
        if (temp < 0) {
            NSArray *lowerValue = [dictionaryValues objectForKey:self.key1];
            if ([lowerValue objectAtIndex:self.a] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float lower = [[lowerValue objectAtIndex:self.a] floatValue];
                groundRoll = lower;
            }
            
            
            NSArray *lValue = [dictionaryObstacleValues objectForKey:self.key1];
            if ([lValue objectAtIndex:self.a] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float l = [[lValue objectAtIndex:self.a] floatValue];
                clearObstacle = l;
            }
        }
        if (temp >= 0 && temp < 40) {
            float x = ((temp - self.tempLower)/10);
            NSArray *lowerValue = [dictionaryValues objectForKey:self.key1];
            if ([lowerValue objectAtIndex:self.a] == nil || [lowerValue objectAtIndex:self.b] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float lower0 = [[lowerValue objectAtIndex:self.a] floatValue];
                float lower1 = [[lowerValue objectAtIndex:self.b] floatValue];
                float lower = (((lower1 - lower0)*x)+lower0);
                groundRoll = lower;
            }
            
            
            NSArray *lValue = [dictionaryObstacleValues objectForKey:self.key1];
            if ([lValue objectAtIndex:self.a] == nil || [lValue objectAtIndex:self.b] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float l0 = [[lValue objectAtIndex:self.a] floatValue];
                float l1 = [[lValue objectAtIndex:self.b] floatValue];
                float l = (((l1 - l0)*x)+l0);
                clearObstacle = l;
            }
        }
        if (temp == 40) {
            NSArray *lowerValue = [dictionaryValues objectForKey:self.key1];
            if ([lowerValue objectAtIndex:self.a] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float lower = [[lowerValue objectAtIndex:self.a] floatValue];
                groundRoll = lower;
            }
            
            NSArray *lValue = [dictionaryObstacleValues objectForKey:self.key1];
            if ([lValue objectAtIndex:self.a] == nil) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
            } else {
                float l = [[lValue objectAtIndex:self.a] floatValue];
                clearObstacle = l;
            }
        }
        if (temp > 40) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Exceeds POH Data of 40 degrees Celsius" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
            [alertView show];
        }
    }
    if (pressureAltitude > 10000) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Pressure Exceeds POH Data of 10,000 Feet" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
        [alertView show];
    }
    
    if (weight < 2500) {
        self.groundroll = groundRoll;
        self.clearObstacle = clearObstacle;
    }
    if (weight == 2500) {
        self.groundroll2500 = groundRoll;
        self.clearObstacle2500 = clearObstacle;
    }
    else {
        self.groundroll3000 = groundRoll;
        self.clearObstacle3000 = clearObstacle;
    }
    
    [self.view endEditing:YES];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"landingData"]) {
        CDCirrusSR20M3050LandingDataViewController *controller = segue.destinationViewController;
        controller.nNumberText = self.nNumberText;
        controller.emptyWeightText = self.emptyWeightText;
        controller.pilotWeightText = self.pilotWeightText;
        controller.frontSeatWeightText = self.frontSeatWeightText;
        controller.rearSeat1WeightText = self.rearSeat1WeightText;
        controller.rearSeat2WeightText = self.rearSeat2WeightText;
        controller.bagAreaWeightText = self.bagAreaWeightText;
        controller.fuelGalText = self.fuelGalText;
        controller.fuelWeightText = self.fuelWeightText;
        controller.taxiBurnWeightText = self.taxiBurnWeightText;
        controller.totalWeightText = self.totalWeightLabel.text;
        controller.emptyArmText = self.emptyArmText;
        controller.pilotArmText = self.pilotArmText;
        controller.frontSeatArmText = self.frontSeatArmText;
        controller.rearSeat1ArmText = self.rearSeat1ArmText;
        controller.rearSeat2ArmText = self.rearSeat2ArmText;
        controller.bagAreaArmText = self.bagAreaArmText;
        controller.fuelArmText = self.fuelArmText;
        controller.taxiBurnArmText = self.taxiBurnArmText;
        controller.totalArmText = self.totalArmText;
        controller.emptyMomentText = self.emptyMomentText;
        controller.pilotMomentText = self.pilotMomentText;
        controller.frontSeatMomentText = self.frontSeatMomentText;
        controller.rearSeat1MomentText = self.rearSeat1MomentText;
        controller.rearSeat2MomentText = self.rearSeat2MomentText;
        controller.bagAreaMomentText = self.bagAreaMomentText;
        controller.fuelMomentText = self.fuelMomentText;
        controller.taxiBurnMomentText = self.taxiBurnMomentText;
        controller.totalMomentText = self.totalMomentText;
        controller.takeoffCGLimitsText = self.takeoffCGLimitsText;
        controller.takeoffManeuveringSpeedText = self.takeoffManeuveringSpeedText;
        controller.takeoffFieldElevation = self.fieldElevationTextField.text;
        controller.takeoffPressure = self.pressureTextField.text;
        controller.takeoffTemperature = self.temperatureTextField.text;
        controller.takeoffWindDirection = self.windDirectionTextField.text;
        controller.takeoffWindSpeed = self.windSpeedTextField.text;
        controller.takeoffRunway = self.runwayTextField.text;
        controller.takeoffGroundRoll = self.takeoffGroundRollLabel.text;
        controller.takeoffClearObstacle = self.takeoffClearObstacleLabel.text;
        controller.takeoffDensityAltitude = self.densityAltitudeLabel.text;
        controller.takeoffHeadWindComponent = self.headwindComponent;
        controller.takeoffCrosswindComponent = self.takeoffCrossWindComponent;
        
    }
}



@end
