//
//  CDCirrusSR20M3000List.h
//  Aircraft Performance
//
//  Created by Chip Duffield on 7/30/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface CDCirrusSR20M3000List : NSManagedObject

@property (nonatomic, retain) NSString * arm;
@property (nonatomic, retain) NSString * emptyWeight;
@property (nonatomic, retain) NSString * nNumber;

@end
