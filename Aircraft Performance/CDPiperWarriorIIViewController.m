//
//  CDPiperWarriorIIViewController.m
//  Aircraft Performance
//
//  Created by Chip Duffield on 8/2/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import "CDPiperWarriorIIViewController.h"
#import "CDPiperWarriorIICalculationViewController.h"


@interface CDPiperWarriorIIViewController ()

@end

@implementation CDPiperWarriorIIViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation
- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if ([identifier isEqualToString:@"calculate"]) {
        self.bagArea1 = [self.bagAreaWeightTextField.text floatValue];
        self.fuelGal = [self.fuelGalTextField.text floatValue];
        
        if (self.fuelGal > 48.0) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"48 gallons Max" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
            [alertView show];
            
            return NO;
        }
        if (self.bagArea1 > 200.0) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Max Bag Area 1 Limit is 200 lb" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
            [alertView show];
            
            return  NO;
        }
        
    }
    
    return  YES;
}


// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"calculate"]) {
        CDPiperWarriorIICalculationViewController *controller = segue.destinationViewController;
        controller.nNumberText = self.nNumberText;
        controller.emptyWeightText = self.emptyWeightText;
        controller.emptyArmText = self.emptyArmText;
        controller.pilotWeightText = self.pilotWeightTextField.text;
        controller.frontSeatWeightText = self.frontSeatWeightTextField.text;
        controller.rearSeat1WeightText = self.rearSeat1WeightTextField.text;
        controller.rearSeat2WeightText = self.rearSeat2WeightTextField.text;
        controller.bagAreaWeightText = self.bagAreaWeightTextField.text;
        controller.fuelGalText = self.fuelGalTextField.text;
    }
}


@end
