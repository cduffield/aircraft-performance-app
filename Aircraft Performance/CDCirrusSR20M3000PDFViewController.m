//
//  CDCirrusSR20M3000PDFViewController.m
//  Aircraft Performance
//
//  Created by Chip Duffield on 8/2/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import "CDCirrusSR20M3000PDFViewController.h"
#import <CoreText/CoreText.h>
#import "CDCirrusSR20M3000DataView.h"

@interface CDCirrusSR20M3000PDFViewController ()

@end

@implementation CDCirrusSR20M3000PDFViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSString *filename = [self getPDFFileName];
    
    [self drawPDF:filename];
    [self showPDFFile];
    
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)drawLabels
{
    NSArray* objects = [[NSBundle mainBundle] loadNibNamed:@"CirrusSR20M3000DataView" owner:nil options:nil];
    
    UIView* mainView = [objects objectAtIndex:0];
    
    CDCirrusSR20M3000DataView *pdfView = (CDCirrusSR20M3000DataView *)mainView;
    
    pdfView.nNumberContent.text = self.nNumber;
    
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEE, MMM d, Y"];
    self.todaysDate = [dateFormatter stringFromDate:date];
    pdfView.todaysDateContent.text = self.todaysDate;
    
    // Set everything equal to empty string
    pdfView.totalWeightContent.text = @"";
    pdfView.totalArmContent.text = @"";
    pdfView.totalMomentContent.text = @"";
    pdfView.pilotWeightContent.text = @"";
    pdfView.frontSeatWeightContent.text = @"";
    pdfView.rearSeat1WeightContent.text = @"";
    pdfView.rearSeat2WeightContent.text = @"";
    pdfView.bagArea1WeightContent.text = @"";
    pdfView.fuelGalContent.text = @"";
    pdfView.fuelWeightContent.text = @"";
    pdfView.emptyWeightContent.text = @"";
    pdfView.emptyArmContent.text = @"";
    pdfView.emptyMomentContent.text = @"";
    pdfView.pilotMomentContent.text = @"";
    pdfView.frontSeatMomentContent.text = @"";
    pdfView.rearSeat1MomentContent.text = @"";
    pdfView.rearSeat2MomentContent.text = @"";
    pdfView.bagArea1MomentContent.text = @"";
    pdfView.fuelMomentContent.text = @"";
    pdfView.landingEmptyMomentContent.text = @"";
    pdfView.landingPilotMomentContent.text = @"";
    pdfView.landingFrontSeatMomentContent.text = @"";
    pdfView.landingRearSeat1MomentContent.text = @"";
    pdfView.landingRearSeat2MomentContent.text = @"";
    pdfView.landingBagArea1MomentContent.text = @"";
    pdfView.landingFuelMomentContent.text = @"";
    pdfView.cgLabelContent.text = @"";
    pdfView.toDensityAltitudeContent.text = @"";
    pdfView.toGroundRollContent.text = @"";
    pdfView.to50ftObstacleContent.text = @"";
    pdfView.toManSpeedContent.text = @"";
    pdfView.toRunwayContent.text = @"";
    pdfView.toWindContent.text = @"";
    pdfView.toCrosswindComponentContent.text = @"";
    pdfView.landingCGLabelContent.text = @"";
    pdfView.landingGroundRollContent.text = @"";
    pdfView.landingClearObstacleContent.text = @"";
    pdfView.landingDensityAltitude.text = @"";
    pdfView.landingManSpeedContent.text = @"";
    pdfView.landingRunwayContent.text = @"";
    pdfView.landingWindContent.text = @"";
    pdfView.toHeadwindComponentContent.text = @"";
    pdfView.landingCrosswindComponentContent.text = @"";
    pdfView.landingPilotWeightContent.text = @"";
    pdfView.landingFrontSeatWeightContent.text = @"";
    pdfView.landingRearSeat1WeightContent.text = @"";
    pdfView.landingRearSeat2WeightContent.text = @"";
    pdfView.landingBagArea1WeightContent.text = @"";
    pdfView.landingEmptyWeightContent.text = @"";
    pdfView.landingEmptyArmContent.text = @"";
    pdfView.landingFuelGalContent.text = @"";
    pdfView.landingFuelWeightContent.text = @"";
    pdfView.landingTotalWeightContent.text = @"";
    pdfView.landingTotalArmContent.text = @"";
    pdfView.landingTotalMomentContent.text = @"";
    pdfView.landingWindContent.text = @"";
    
    // change to actual values
    pdfView.totalWeightContent.text = self.totalWeight;
    pdfView.totalArmContent.text = self.totalArm;
    pdfView.totalMomentContent.text = self.totalMoment;
    pdfView.pilotWeightContent.text = self.pilotWeight;
    pdfView.frontSeatWeightContent.text = self.frontSeat;
    pdfView.rearSeat1WeightContent.text = self.rearSeat1;
    pdfView.rearSeat2WeightContent.text = self.rearSeat2;
    pdfView.bagArea1WeightContent.text = self.bagArea1;
    pdfView.fuelGalContent.text = self.fuelGal;
    pdfView.fuelWeightContent.text = self.fuelWeight;
    pdfView.emptyWeightContent.text = self.emptyWeight;
    pdfView.emptyArmContent.text = self.emptyArm;
    
    
    float toEmptyWeightMoment = (([self.emptyArm floatValue] * [self.emptyWeight floatValue])/1000);
    float toPilotMoment = (([self.pilotWeight floatValue] * [pdfView.pilotArmContent.text floatValue])/1000);
    float toFrontSeatMoment = (([self.frontSeat floatValue] * [pdfView.frontSeatArmContent.text floatValue])/1000);
    float toRearSeat1Moment = (([self.rearSeat1 floatValue] * [pdfView.rearSeat1ArmContent.text floatValue])/1000);
    float toRearSeat2Moment = (([self.rearSeat2 floatValue] * [pdfView.rearSeat2ArmContent.text floatValue])/1000);
    float toBagArea1Moment = (([self.bagArea1 floatValue] * [pdfView.bagArea1ArmContent.text floatValue])/1000);
    float tofuelMoment = (([self.fuelWeight floatValue] * [pdfView.fuelArmContent.text floatValue])/1000);
    float ldgFuelMoment = (([self.landingFuelWeight floatValue] * [pdfView.fuelArmContent.text floatValue])/1000);
    
    
    
    pdfView.emptyMomentContent.text = [[NSString alloc] initWithFormat:@"%.2f",toEmptyWeightMoment];
    pdfView.pilotMomentContent.text = [[NSString alloc] initWithFormat:@"%.2f",toPilotMoment];
    pdfView.frontSeatMomentContent.text = [[NSString alloc] initWithFormat:@"%.2f",toFrontSeatMoment];
    pdfView.rearSeat1MomentContent.text = [[NSString alloc] initWithFormat:@"%.2f",toRearSeat1Moment];
    pdfView.rearSeat2MomentContent.text = [[NSString alloc] initWithFormat:@"%.2f",toRearSeat2Moment];
    pdfView.bagArea1MomentContent.text = [[NSString alloc] initWithFormat:@"%.2f",toBagArea1Moment];
    pdfView.fuelMomentContent.text = [[NSString alloc] initWithFormat:@"%.f",tofuelMoment];
    
    pdfView.landingEmptyMomentContent.text = [[NSString alloc] initWithFormat:@"%.f",toEmptyWeightMoment];
    pdfView.landingPilotMomentContent.text = [[NSString alloc] initWithFormat:@"%.2f",toPilotMoment];
    pdfView.landingFrontSeatMomentContent.text = [[NSString alloc] initWithFormat:@"%.2f",toFrontSeatMoment];
    pdfView.landingRearSeat1MomentContent.text = [[NSString alloc] initWithFormat:@"%.2f",toRearSeat1Moment];
    pdfView.landingRearSeat2MomentContent.text = [[NSString alloc] initWithFormat:@"%.2f",toRearSeat2Moment];
    pdfView.landingBagArea1MomentContent.text = [[NSString alloc] initWithFormat:@"%.2f",toBagArea1Moment];
    pdfView.landingFuelMomentContent.text = [[NSString alloc] initWithFormat:@"%.2f",ldgFuelMoment];
    
    pdfView.cgLabelContent.text = self.takeoffCG;
    pdfView.toDensityAltitudeContent.text = self.takeoffDensityAltitude;
    pdfView.toGroundRollContent.text = self.takeoffGroundRoll;
    pdfView.to50ftObstacleContent.text = self.takeoffObstacleClearance;
    pdfView.toManSpeedContent.text = self.toManeuveringSpeed;
    pdfView.toRunwayContent.text = self.takeoffRunway;
    NSString *takeoffWindText = [[NSString alloc] initWithFormat:@"%@/%@",self.takeoffWindDirection,self.takeoffWindSpeed];
    pdfView.toWindContent.text = takeoffWindText;
    pdfView.toCrosswindComponentContent.text = self.takeoffCrossWindComponent;
    pdfView.toHeadwindComponentContent.text = self.takeofWindComponent;
    
    
    pdfView.landingCGLabelContent.text = self.landingCG;
    pdfView.landingGroundRollContent.text = self.landingGroundRoll;
    pdfView.landingClearObstacleContent.text = self.landingClearObstacle;
    pdfView.landingDensityAltitude.text = self.landingDensityAltitude;
    pdfView.landingManSpeedContent.text = self.ldgManeurvingSpeed;
    pdfView.landingRunwayContent.text = self.landingRunway;
    NSString *landingWindText = [[NSString alloc] initWithFormat:@"%@/%@",self.landingWindDirection,self.landingWindSpeed];
    pdfView.landingWindContent.text = landingWindText;
    pdfView.landingHeadwindComponentContent.text = self.landingWindComponent;
    pdfView.landingCrosswindComponentContent.text = self.landingCrossWindComponent;
    
    
    pdfView.landingPilotWeightContent.text = self.pilotWeight;
    pdfView.landingFrontSeatWeightContent.text = self.frontSeat;
    pdfView.landingRearSeat1WeightContent.text = self.rearSeat1;
    pdfView.landingRearSeat2WeightContent.text = self.rearSeat2;
    pdfView.landingBagArea1WeightContent.text = self.bagArea1;
    pdfView.landingEmptyWeightContent.text = self.emptyWeight;
    pdfView.landingEmptyArmContent.text = self.emptyArm;
    pdfView.landingFuelGalContent.text = self.landingFuelGal;
    pdfView.landingFuelWeightContent.text = self.landingFuelWeight;
    pdfView.landingTotalWeightContent.text = self.landingTotalWeight;
    pdfView.landingTotalArmContent.text = self.landingTotalArm;
    pdfView.landingTotalMomentContent.text = self.landingTotalMoment;
  
    
    
    [self drawText:pdfView.totalWeightContent.text inFrame:pdfView.totalWeightContent.frame];
    [self drawText:pdfView.pilotWeightContent.text inFrame:pdfView.pilotWeightContent.frame];
    [self drawText:pdfView.frontSeatWeightContent.text inFrame:pdfView.frontSeatWeightContent.frame];
    [self drawText:pdfView.rearSeat1WeightContent.text inFrame:pdfView.rearSeat1WeightContent.frame];
    [self drawText:pdfView.rearSeat2WeightContent.text inFrame:pdfView.rearSeat2WeightContent.frame];
    [self drawText:pdfView.bagArea1WeightContent.text inFrame:pdfView.bagArea1WeightContent.frame];
    [self drawText:pdfView.fuelGalContent.text inFrame:pdfView.fuelGalContent.frame];
    [self drawText:pdfView.fuelWeightContent.text inFrame:pdfView.fuelWeightContent.frame];
    [self drawText:pdfView.emptyWeightContent.text inFrame:pdfView.emptyWeightContent.frame];
    [self drawText:pdfView.emptyArmContent.text inFrame:pdfView.emptyArmContent.frame];
    [self drawText:pdfView.landingPilotWeightContent.text inFrame:pdfView.landingPilotWeightContent.frame];
    [self drawText:pdfView.landingFrontSeatWeightContent.text inFrame:pdfView.landingFrontSeatWeightContent.frame];
    [self drawText:pdfView.landingRearSeat1WeightContent.text inFrame:pdfView.landingRearSeat1WeightContent.frame];
    [self drawText:pdfView.landingRearSeat2WeightContent.text inFrame:pdfView.landingRearSeat2WeightContent.frame];
    [self drawText:pdfView.landingBagArea1WeightContent.text inFrame:pdfView.landingBagArea1WeightContent.frame];
    [self drawText:pdfView.landingEmptyWeightContent.text inFrame:pdfView.landingEmptyWeightContent.frame];
    [self drawText:pdfView.landingEmptyArmContent.text inFrame:pdfView.landingEmptyArmContent.frame];
    [self drawText:pdfView.landingFuelGalContent.text inFrame:pdfView.landingFuelGalContent.frame];
    [self drawText:pdfView.landingFuelWeightContent.text inFrame:pdfView.landingFuelWeightContent.frame];
    [self drawText:pdfView.landingTotalWeightContent.text inFrame:pdfView.landingTotalWeightContent.frame];
    [self drawText:pdfView.emptyMomentContent.text inFrame:pdfView.emptyMomentContent.frame];
    [self drawText:pdfView.pilotMomentContent.text inFrame:pdfView.pilotMomentContent.frame];
    [self drawText:pdfView.frontSeatMomentContent.text  inFrame:pdfView.frontSeatMomentContent.frame];
    [self drawText:pdfView.rearSeat1MomentContent.text inFrame:pdfView.rearSeat1MomentContent.frame];
    [self drawText:pdfView.rearSeat2MomentContent.text inFrame:pdfView.rearSeat2MomentContent.frame];
    [self drawText:pdfView.bagArea1MomentContent.text inFrame:pdfView.bagArea1MomentContent.frame];
    [self drawText:pdfView.fuelMomentContent.text inFrame:pdfView.fuelMomentContent.frame];
    [self drawText:pdfView.landingTotalArmContent.text inFrame:pdfView.landingTotalArmContent.frame];
    [self drawText:pdfView.landingTotalMomentContent.text inFrame:pdfView.landingTotalMomentContent.frame];
    [self drawText:pdfView.landingCGLabelContent.text inFrame:pdfView.landingCGLabelContent.frame];
    [self drawText:pdfView.landingGroundRollContent.text inFrame:pdfView.landingGroundRollContent.frame];
    [self drawText:pdfView.landingClearObstacleContent.text inFrame:pdfView.landingClearObstacleContent.frame];
    [self drawText:pdfView.landingDensityAltitude.text inFrame:pdfView.landingDensityAltitude.frame];
    [self drawText:pdfView.nNumberContent.text inFrame:pdfView.nNumberContent.frame];
    [self drawText:pdfView.todaysDateContent.text inFrame:pdfView.todaysDateContent.frame];
    [self drawText:pdfView.landingManSpeedContent.text inFrame:pdfView.landingManSpeedContent.frame];
    [self drawText:pdfView.toManSpeedContent.text inFrame:pdfView.toManSpeedContent.frame];
    [self drawText:pdfView.toRunwayContent.text inFrame:pdfView.toRunwayContent.frame];
    [self drawText:pdfView.landingRunwayContent.text inFrame:pdfView.landingRunwayContent.frame];
    [self drawText:pdfView.toWindContent.text inFrame:pdfView.toWindContent.frame];
    [self drawText:pdfView.landingWindContent.text inFrame:pdfView.landingWindContent.frame];
    [self drawText:pdfView.toHeadwindComponentContent.text inFrame:pdfView.toHeadwindComponentContent.frame];
    [self drawText:pdfView.landingWindContent.text inFrame:pdfView.landingWindContent.frame];
    [self drawText:pdfView.toCrosswindComponentContent.text inFrame:pdfView.toCrosswindComponentContent.frame];
    
    
    
    
    
    for (UIView* view in [mainView subviews]) {
        if([view isKindOfClass:[UILabel class]])
        {
            UILabel* label = (UILabel*)view;
            [self drawText:label.text inFrame:label.frame];
            
        }
        
    }
    
}

-(void)drawText:(NSString*)textToDraw inFrame:(CGRect)frameRect
{
    CFStringRef stringRef = (__bridge CFStringRef)textToDraw;
    // Prepare the text using a Core Text Framesetter.
    CFAttributedStringRef currentText = CFAttributedStringCreate(NULL, stringRef, NULL);
    CTFramesetterRef framesetter = CTFramesetterCreateWithAttributedString(currentText);
    
    CGMutablePathRef framePath = CGPathCreateMutable();
    CGPathAddRect(framePath, NULL, frameRect);
    
    // Get the frame that will do the rendering.
    CFRange currentRange = CFRangeMake(0, 0);
    CTFrameRef frameRef = CTFramesetterCreateFrame(framesetter, currentRange, framePath, NULL);
    CGPathRelease(framePath);
    
    // Get the graphics context.
    CGContextRef    currentContext = UIGraphicsGetCurrentContext();
    
    // Put the text matrix into a known state. This ensures
    // that no old scaling factors are left in place.
    CGContextSetTextMatrix(currentContext, CGAffineTransformIdentity);
    
    
    
    // Core Text draws from the bottom-left corner up, so flip
    // the current transform prior to drawing.
    // Modify this to take into consideration the origin.
    CGContextTranslateCTM(currentContext, 0, frameRect.origin.y*2);
    CGContextScaleCTM(currentContext, 1.0, -1.0);
    
    // Draw the frame.
    CTFrameDraw(frameRef, currentContext);
    
    
    // Add these two lines to reverse the earlier transformation.
    CGContextScaleCTM(currentContext, 1.0, -1.0);
    CGContextTranslateCTM(currentContext, 0, (-1)*frameRect.origin.y*2);
    
    
    CFRelease(frameRef);
    CFRelease(stringRef);
    CFRelease(framesetter);
}

-(void)drawPDF:(NSString*)fileName
{
    // Create the PDF context using the default page size of 612 x 792.
    UIGraphicsBeginPDFContextToFile(fileName, CGRectZero, nil);
    // Mark the beginning of a new page.
    UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, 612, 792), nil);
    
    
    
    [self drawLabels];
    
    // Close the PDF context and write out the contents.
    UIGraphicsEndPDFContext();
}

- (IBAction)openWith:(id)sender {
    self.docController = [UIDocumentInteractionController interactionControllerWithURL:self.url];
    self.docController.delegate = self;
    CGRect navRect = self.view.frame;
    [self.docController presentOptionsMenuFromRect:navRect inView:self.view animated:YES];

}



-(NSString*)getPDFFileName
{
    NSString* fileName = @"CirrusSR20.PDF";
    
    NSArray *arrayPaths =
    NSSearchPathForDirectoriesInDomains(
                                        NSDocumentDirectory,
                                        NSUserDomainMask,
                                        YES);
    NSString *path = [arrayPaths objectAtIndex:0];
    NSString* pdfFileName = [path stringByAppendingPathComponent:fileName];
    
    return pdfFileName;
    
}

-(void)showPDFFile
{
    NSString* fileName = @"CirrusSR20.PDF";
    
    NSArray *arrayPaths =
    NSSearchPathForDirectoriesInDomains(
                                        NSDocumentDirectory,
                                        NSUserDomainMask,
                                        YES);
    NSString *path = [arrayPaths objectAtIndex:0];
    NSString* pdfFileName = [path stringByAppendingPathComponent:fileName];
    
    UIWebView* webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 60, 320, 568)];
    
    self.url = [NSURL fileURLWithPath:pdfFileName];
    NSURLRequest *request = [NSURLRequest requestWithURL:self.url];
    [webView setScalesPageToFit:YES];
    [webView loadRequest:request];
    
    [self.view addSubview:webView];
}

- (UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)controller
{
    return self;
}

- (UIView *)documentInteractionControllerViewForPreview:(UIDocumentInteractionController *)controller
{
    return self.view;
}

- (CGRect)documentInteractionControllerRectForPreview:(UIDocumentInteractionController *)controller
{
    return self.view.frame;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
