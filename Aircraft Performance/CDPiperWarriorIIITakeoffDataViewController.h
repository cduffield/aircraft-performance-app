//
//  CDPiperWarriorIIITakeoffDataViewController.h
//  Aircraft Performance
//
//  Created by Chip Duffield on 10/6/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDPiperWarriorIIITakeoffDataViewController : UIViewController


@property (weak, nonatomic) IBOutlet UILabel *takeoffTotalWeightLabel;
@property (weak, nonatomic) IBOutlet UITextField *takeoffFieldElevationTextField;
@property (weak, nonatomic) IBOutlet UITextField *takeoffPressureTextField;
@property (weak, nonatomic) IBOutlet UITextField *takeoffTemperatureTextField;
@property (weak, nonatomic) IBOutlet UITextField *takeoffWindDirectionTextField;
@property (weak, nonatomic) IBOutlet UITextField *takeoffWindSpeedTextField;
@property (weak, nonatomic) IBOutlet UITextField *takeoffRunwayTextField;
@property (weak, nonatomic) IBOutlet UILabel *takeoffDensityAltitudeLabel;
@property (weak, nonatomic) IBOutlet UILabel *takeoffGroundRollLabel;
@property (weak, nonatomic) IBOutlet UILabel *takeoffClearObstacleLabel;


@property (nonatomic) NSString *nNumberText;
@property (nonatomic) NSString *emptyWeightText;
@property (nonatomic) NSString *emptyArmText;
@property (nonatomic) NSString *pilotWeightText;
@property (nonatomic) NSString *frontSeatWeightText;
@property (nonatomic) NSString *rearSeat1WeightText;
@property (nonatomic) NSString *rearSeat2WeightText;
@property (nonatomic) NSString *bagAreaWeightText;
@property (nonatomic) NSString *fuelGalText;
@property (nonatomic) NSString *fuelWeightText;
@property (nonatomic) NSString *taxiBurnWeightText;
@property (nonatomic) NSString *totalWeightText;
@property (nonatomic) NSString *pilotArmText;
@property (nonatomic) NSString *frontSeatArmText;
@property (nonatomic) NSString *rearSeat1ArmText;
@property (nonatomic) NSString *rearSeat2ArmText;
@property (nonatomic) NSString *bagAreaArmText;
@property (nonatomic) NSString *fuelArmText;
@property (nonatomic) NSString *taxiBurnArmText;
@property (nonatomic) NSString *totalArmText;
@property (nonatomic) NSString *emptyMomentText;
@property (nonatomic) NSString *pilotMomentText;
@property (nonatomic) NSString *frontSeatMomentText;
@property (nonatomic) NSString *rearSeat1MomentText;
@property (nonatomic) NSString *rearSeat2MomentText;
@property (nonatomic) NSString *bagAreaMomentText;
@property (nonatomic) NSString *fuelMomentText;
@property (nonatomic) NSString *taxiBurnMomentText;
@property (nonatomic) NSString *totalMomentText;
@property (nonatomic) NSString *takeoffCGLimitsText;
@property (nonatomic) NSString *takeoffManeuveringSpeedText;
@property (nonatomic) NSString *takeoffHeadWindComponent;
@property (nonatomic) NSString *takeoffCrossWindComponent;

@property (nonatomic) float dRatio;
@property (nonatomic) float newdistance;
@property (nonatomic) float standardWeight;
@property (nonatomic, strong) NSString *toDistance;
@property (nonatomic, strong) NSString *toDistanceObstacle;
@property (nonatomic) float groundroll;
@property (nonatomic) float clearObstacle;
@property (nonatomic) float weight;
@property (nonatomic) float constant;
@property (nonatomic) float constantObstacle;
@property (nonatomic) float lowerValue;
@property (nonatomic) float higherValue;
@property (nonatomic) float groundRollDistance;
@property (nonatomic) float clearObstacleDistance;
@property (nonatomic) float headwindPercentage;
@property (nonatomic) float headwindPercentageObstacle;
@property (nonatomic) float tailwindPercentage;
@property (nonatomic) float tailwindPercentageObstacle;


- (IBAction)calculate:(id)sender;


@end
