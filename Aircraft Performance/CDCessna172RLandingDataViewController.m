//
//  CDCessna172RLandingDataViewController.m
//  Aircraft Performance
//
//  Created by Chip Duffield on 7/4/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import "CDCessna172RLandingDataViewController.h"
#import "CDCessna172RLandingGraphViewController.h"
#import "CDCessna172RPDFViewController.h"

@interface CDCessna172RLandingDataViewController ()

@end

@implementation CDCessna172RLandingDataViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.automaticallyAdjustsScrollViewInsets = false;
    self.view.backgroundColor = [UIColor colorWithRed:(126/255.0) green:(194/255.0) blue:(241/255.0) alpha:1.0];
    
    self.fieldElevationTextField.text = self.fieldElevationTO;
    self.pressureTextField.text = self.pressureTO;
    self.temperatureTextField.text = self.temperatureTO;
    self.windDirectionTextField.text = self.takeoffWindDirection;
    self.windSpeedTextField.text = self.takeoffWindSpeed;
    self.runwayTextField.text = self.takeoffRunway;
    
    NSArray *pSeaLevel = [[NSArray alloc] initWithObjects:@525,@540,@560,@580,@600,nil];
    NSArray *p1000 = [[NSArray alloc] initWithObjects:@545,@560,@580,@600,@620,nil];
    NSArray *p2000 = [[NSArray alloc] initWithObjects:@565,@585,@605,@625,@645,nil];
    NSArray *p3000 = [[NSArray alloc] initWithObjects:@585,@605,@625,@650,@670,nil];
    NSArray *p4000 = [[NSArray alloc] initWithObjects:@605,@630,@650,@670,@695,nil];
    NSArray *p5000 = [[NSArray alloc] initWithObjects:@630,@650,@675,@700,@720,nil];
    NSArray *p6000 = [[NSArray alloc] initWithObjects:@655,@675,@700,@725,@750,nil];
    NSArray *p7000 = [[NSArray alloc] initWithObjects:@680,@705,@730,@755,@775,nil];
    NSArray *p8000 = [[NSArray alloc] initWithObjects:@705,@730,@755,@780,@810,nil];
    self.landingGroundRoll = [[NSDictionary alloc] initWithObjectsAndKeys:pSeaLevel,@0,p1000,@1000,p2000,@2000,p3000,@3000,p4000,@4000,p5000,@5000,p6000,@6000,p7000,@7000,p8000,@8000, nil];
    
    NSArray *pSeaLevelAt50 = [[NSArray alloc] initWithObjects:@1250,@1280,@1310,@1340,@1370,nil];
    NSArray *p1000At50 = [[NSArray alloc] initWithObjects:@1280,@1310,@1345,@1375,@1405,nil];
    NSArray *p2000At50 = [[NSArray alloc] initWithObjects:@1310,@1345,@1375,@1410,@1440,nil];
    NSArray *p3000At50 = [[NSArray alloc] initWithObjects:@1345,@1380,@1415,@1445,@1480,nil];
    NSArray *p4000At50 = [[NSArray alloc] initWithObjects:@1380,@1415,@1450,@1485,@1520,nil];
    NSArray *p5000At50 = [[NSArray alloc] initWithObjects:@1415,@1455,@1490,@1525,@1560,nil];
    NSArray *p6000At50 = [[NSArray alloc] initWithObjects:@1455,@1490,@1530,@1565,@1605,nil];
    NSArray *p7000At50 = [[NSArray alloc] initWithObjects:@1495,@1535,@1570,@1610,@1650,nil];
    NSArray *p8000At50 = [[NSArray alloc] initWithObjects:@1535,@1575,@1615,@1655,@1695,nil];
    self.landingClearObstacle = [[NSDictionary alloc] initWithObjectsAndKeys:pSeaLevelAt50,@0,p1000At50,@1000,p2000At50,@2000,p3000At50,@3000,p4000At50,@4000,p5000At50,@5000,p6000At50,@6000,p7000At50,@7000,p8000At50,@8000, nil];
}

- (IBAction)calculateLanding:(id)sender {
    
    float totalWeightBefore = [self.totalWeightBefore floatValue];
    float totalMomentBefore = [self.totalMomentBefore floatValue] * 1000;
    float fuelArm = [self.fuelARM floatValue];
    float fuelGallonsBefore = [self.fuelGallongsBefore floatValue];
    float fuelWeightBefore = fuelGallonsBefore * 6;
    float fuelMomentBefore = fuelArm * fuelWeightBefore;
    float fuelGPH = [self.gphTextField.text floatValue];
    float fuelETE = [self.timeEnrouteTextField.text floatValue];
    self.fuelWeightBefore = [[NSString alloc] initWithFormat:@"%.f",fuelWeightBefore];
    
    //calculate zero fuel weight, moment, and arm
    float zeroFuelWeight = totalWeightBefore - fuelWeightBefore;
    float zeroFuelMoment = totalMomentBefore - fuelMomentBefore;
    
    //calculate new fuel
    float fuelBurnGallons = fuelGPH * (fuelETE/60.0);
    float fuelWeightAfter = (fuelGallonsBefore - fuelBurnGallons) * 6;
    float fuelMomentAfter = fuelWeightAfter * fuelArm;
    self.fuelGallonsLdg = [[NSString alloc] initWithFormat:@"%.f",(fuelGallonsBefore-fuelBurnGallons)];
    self.fuelWeightLdg = [[NSString alloc] initWithFormat:@"%.f",fuelWeightAfter];
    
    //calulate new totals
    float totalWeightAfter = zeroFuelWeight + fuelWeightAfter;
    float totalMomentAfter = fuelMomentAfter + zeroFuelMoment;
    float totalArmAfter = totalMomentAfter/totalWeightAfter;
    float totalM = totalMomentAfter/1000;
    self.totalWeightLdg = [[NSString alloc] initWithFormat:@"%.f",totalWeightAfter];
    
    //Max weight calculations
    float aircraftMaxWeight = 2450.0;
    float m = 66.67;
    float b = 283.45;
    float (^maxWeightAtCG)(float,float,float) = ^(float totalA, float m, float b) {
        if (totalA <41) {
            float weight = ((m * totalA) - b);
            return weight;
        }
        else {
            float weight = 2450.0;
            return weight;
        }
        
    };
    float maxWeight = maxWeightAtCG(totalArmAfter, m, b);
    
    //set cg response
    NSString *cglimits;
    if (totalArmAfter >= 35 && totalArmAfter <= 47.3) {
        if (totalWeightAfter <= maxWeight) {
            cglimits = @"Within Limits";
        }
        else if (totalWeightAfter > aircraftMaxWeight) {
            float overWeight = totalWeightAfter - aircraftMaxWeight;
            cglimits = [[NSString alloc] initWithFormat:@"Overweight by %.f lbs",overWeight];
        }
        else {
            cglimits = @"Outside of Limits";
        }
    }
    else {
        cglimits = @"Outside of Limits";
    }
    self.cgLabel.text = cglimits;
    self.totalWeightLabel.text = [[NSString alloc] initWithFormat:@"%.f",totalWeightAfter];
    self.totalArmLabel.text = [[NSString alloc] initWithFormat:@"%.1f",totalArmAfter];
    self.totalMomentLabel.text = [[NSString alloc] initWithFormat:@"%.02f",totalM];
    
    //constants for both
    float pressure = [self.pressureTextField.text floatValue];
    float temp = [self.temperatureTextField.text floatValue];
    float fieldElevation = [self.fieldElevationTextField.text floatValue];
    float pressureAltitude = (((29.92 - pressure)*1000) + fieldElevation);
    //float wind = [self.windTextField.text floatValue];
   
    float groundrollCorrectedForWind;
    float clearObstacleCorrectedForWind;
   
    
    
    //wind
    float windDirection = [self.windDirectionTextField.text floatValue];
    float windSpeed = [self.windSpeedTextField.text floatValue];
    float runwayDirection = ([self.runwayTextField.text floatValue]*10);
    
    if (runwayDirection > 360) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please enter runway without last Digit. Use 9 instead of 090" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
        [alertView show];
    } else if (windDirection > 360) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error'" message:@"Wind Direction can not be greater that 360" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
        [alertView show];

    } else {
        //calculate density altitude
        float isaTemp = (15 -((pressureAltitude/1000) * 1.98));
        float densityAltitude = (pressureAltitude + (118.8 * (temp - isaTemp)));
        self.landingDensityAltitude = [[NSString alloc] initWithFormat:@"%.f",densityAltitude];
        
        //manuevering speed calculation
        float aircraftMaxWeight = 2450;
        float manSpeedMax = 99;
        float manSpeed = manSpeedMax * sqrtf([self.totalWeightLdg floatValue]/aircraftMaxWeight);
        self.ldgManeuveringSpeed = [[NSString alloc] initWithFormat:@"%.f",manSpeed];
        
        
        
        
        //calculate ground roll
        
        
        [self calculateTempIndex:temp];
        [self pressureAltitudeKeys:pressureAltitude];
        
        if (pressureAltitude < 0) {
            if (temp < 0) {
                NSArray *lowerValue = [self.landingGroundRoll objectForKey:self.key1];
                if ([lowerValue objectAtIndex:self.a] == nil) {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Value Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                    [alertView show];
                } else {
                    float lower = [[lowerValue objectAtIndex:self.a] floatValue];
                    self.groundroll = lower;
                }
                
                
                NSArray *lValue = [self.landingClearObstacle objectForKey:self.key1];
                float l = [[lValue objectAtIndex:self.a] floatValue];
                self.clearObstacle = l;
            }
            if (temp >= 0 && temp < 40) {
                float x = ((temp - self.tempLower)/10);
                NSArray *lowerValue = [self.landingGroundRoll objectForKey:self.key1];
                if ([lowerValue objectAtIndex:self.a] == nil || [lowerValue objectAtIndex:self.b] == nil) {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Value Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                    [alertView show];
                } else {
                    float lower0 = [[lowerValue objectAtIndex:self.a] floatValue];
                    float lower1 = [[lowerValue objectAtIndex:self.b] floatValue];
                    float lower = (((lower1 - lower0)*x)+lower0);
                    self.groundroll = lower;
                }

                NSArray *lValue = [self.landingClearObstacle objectForKey:self.key1];
                if ([lValue objectAtIndex:self.a] == nil || [lValue objectAtIndex:self.b] == nil) {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Value Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                    [alertView show];
                } else {
                    float l0 = [[lValue objectAtIndex:self.a] floatValue];
                    float l1 = [[lValue objectAtIndex:self.b] floatValue];
                    float l = (((l1 - l0)*x)+l0);
                    self.clearObstacle = l;
                }
            }
            if (temp == 40) {
                NSArray *lowerValue = [self.landingGroundRoll objectForKey:self.key1];
                if ([lowerValue objectAtIndex:self.a] == nil) {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Value Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                    [alertView show];
                } else {
                    float lower = [[lowerValue objectAtIndex:self.a] floatValue];
                    self.groundroll = lower;
                }
                
                NSArray *lValue = [self.landingClearObstacle objectForKey:self.key1];
                if ([lValue objectAtIndex:self.a]) {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Value Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                    [alertView show];

                } else {
                    float l = [[lValue objectAtIndex:self.a] floatValue];
                    self.clearObstacle = l;
                }
            }
            if (temp > 40) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Value Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
                //self.groundRollLabel.text = @"N/A";
                //self.obstacleClearanceLabel.text = @"N/A";
            }
        }
        if (pressureAltitude >= 0 && pressureAltitude < 8000) {
            if (temp < 0 ) {
                float y = ((pressureAltitude - self.pLower)/1000);
                NSArray *lowerValue = [self.landingGroundRoll objectForKey:@0];
                NSArray *higherValue = [self.landingGroundRoll objectForKey:@1000];
                if ([lowerValue objectAtIndex:0] == nil || [higherValue objectAtIndex:0] == nil ) {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Value Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                    [alertView show];
                } else {
                    float lower = [[lowerValue objectAtIndex:0] floatValue];
                    float higher = [[higherValue objectAtIndex:0] floatValue];
                    self.groundroll = (((higher - lower)*y)+lower);
                }
                
                NSArray *lValue = [self.landingClearObstacle objectForKey:@0];
                NSArray *hValue = [self.landingClearObstacle objectForKey:@1000];
                if ([lValue objectAtIndex:0] == nil || [hValue objectAtIndex:0] == nil) {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Value Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                    [alertView show];
                } else {
                    float l = [[lValue objectAtIndex:0] floatValue];
                    float h = [[hValue objectAtIndex:0] floatValue];
                    self.clearObstacle = (((h - l)*y)+l);
                }
                
            }
            
            
            if (temp >= 0 && temp < 40 ) {
                
                float y = ((pressureAltitude - self.pLower)/1000);
                float x = ((temp - self.tempLower)/10);
                NSArray *lowerValue = [self.landingGroundRoll objectForKey:self.key1];
                
                NSArray *higherValue = [self.landingGroundRoll objectForKey:self.key2];
                if ([lowerValue objectAtIndex:self.a] == nil || [lowerValue objectAtIndex:self.b] == nil || [higherValue objectAtIndex:self.a] == nil || [higherValue objectAtIndex:self.b] == nil) {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Value Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                    [alertView show];
                } else {
                    float lower0 = [[lowerValue objectAtIndex:self.a] floatValue];
                    float lower1 = [[lowerValue objectAtIndex:self.b] floatValue];
                    float lower = (((lower1 - lower0)*x)+lower0);
                    float higher0 = [[higherValue objectAtIndex:self.a] floatValue];
                    float higher1 = [[higherValue objectAtIndex:self.b] floatValue];
                    float higher = (((higher1 - higher0)*x)+higher0);
                    self.groundroll = (((higher - lower)*y)+lower);
                }
                
                
                NSArray *lValue = [self.landingClearObstacle objectForKey:self.key1];
                
                NSArray *hValue = [self.landingClearObstacle objectForKey:self.key2];
                if ([lValue objectAtIndex:self.a] == nil || [lValue objectAtIndex:self.b] == nil || [hValue objectAtIndex:self.a] == nil || [hValue objectAtIndex:self.b] == nil) {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Value Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                    [alertView show];
                } else {
                    float l0 = [[lValue objectAtIndex:self.a] floatValue];
                    float l1 = [[lValue objectAtIndex:self.b] floatValue];
                    float l = (((l1 - l0)*x)+l0);
                    float h0 = [[hValue objectAtIndex:self.a] floatValue];
                    float h1 = [[hValue objectAtIndex:self.b] floatValue];
                    float h = (((h1 - h0)*x)+h0);
                    self.clearObstacle = (((h - l)*y)+l);
                }
               
            }
            if (temp == 40) {
                float y = ((pressureAltitude - self.pLower)/1000);
                NSArray *lowerValue = [self.landingGroundRoll objectForKey:self.key1];
                
                NSArray *higherValue = [self.landingGroundRoll objectForKey:self.key2];
                if ([lowerValue objectAtIndex:self.a] == nil || [higherValue objectAtIndex:self.a] == nil) {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Value Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                    [alertView show];
                } else {
                    float lower = [[lowerValue objectAtIndex:self.a] floatValue];
                    float higher = [[higherValue objectAtIndex:self.a] floatValue];
                    self.groundroll = (((higher - lower)*y)+lower);
                }
                
                
                NSArray *lValue = [self.landingClearObstacle objectForKey:self.key1];
                NSArray *hValue = [self.landingClearObstacle objectForKey:self.key2];
                if ([lValue objectAtIndex:self.a] == nil || [hValue objectAtIndex:self.a] == nil) {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Value Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                    [alertView show];
                } else {
                    float l = [[lValue objectAtIndex:self.a] floatValue];
                    float h = [[hValue objectAtIndex:self.a] floatValue];
                    self.clearObstacle = (((h - l)*y)+l);
                }
                
                
            }
            if (temp > 40) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Value Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
                //self.groundRollLabel.text = @"N/A";
                //self.obstacleClearanceLabel.text = @"N/A";
            }
        }
        
        if (pressureAltitude == 8000) {
            if (temp < 0) {
                NSArray *lowerValue = [self.landingGroundRoll objectForKey:self.key1];
                if ([lowerValue objectAtIndex:self.a] == nil) {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Value Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                    [alertView show];
                } else {
                    float lower = [[lowerValue objectAtIndex:self.a] floatValue];
                    self.groundroll = lower;
                }
                
                
                NSArray *lValue = [self.landingClearObstacle objectForKey:self.key1];
                if ([lValue objectAtIndex:self.a] == nil) {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Value Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                    [alertView show];
                } else {
                    float l = [[lValue objectAtIndex:self.a] floatValue];
                    self.clearObstacle = l;
                }
            }
            if (temp >= 0 && temp < 40) {
                float x = ((temp - self.tempLower)/10);
                NSArray *lowerValue = [self.landingGroundRoll objectForKey:self.key1];
                if ([lowerValue objectAtIndex:self.a] == nil || [lowerValue objectAtIndex:self.b] == nil) {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Value Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                    [alertView show];
                } else {
                    float lower0 = [[lowerValue objectAtIndex:self.a] floatValue];
                    float lower1 = [[lowerValue objectAtIndex:self.b] floatValue];
                    float lower = (((lower1 - lower0)*x)+lower0);
                    self.groundroll = lower;
                }
                
                
                NSArray *lValue = [self.landingClearObstacle objectForKey:self.key1];
                if ([lValue objectAtIndex:self.a] == nil || [lValue objectAtIndex:self.b] == nil) {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Value Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                    [alertView show];
                } else {
                    float l0 = [[lValue objectAtIndex:self.a] floatValue];
                    float l1 = [[lValue objectAtIndex:self.b] floatValue];
                    float l = (((l1 - l0)*x)+l0);
                    self.clearObstacle = l;
                }
            }
            if (temp == 40) {
                NSArray *lowerValue = [self.landingGroundRoll objectForKey:self.key1];
                if ([lowerValue objectAtIndex:self.a] == nil) {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Value Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                    [alertView show];
                } else {
                    float lower = [[lowerValue objectAtIndex:self.a] floatValue];
                    self.groundroll = lower;
                }
                
                NSArray *lValue = [self.landingClearObstacle objectForKey:self.key1];
                if ([lValue objectAtIndex:self.a] == nil) {
                    
                } else {
                    float l = [[lValue objectAtIndex:self.a] floatValue];
                    self.clearObstacle = l;
                }
            }
            if (temp > 40) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Temperature Value Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
                //self.groundRollLabel.text = @"N/A";
                //self.obstacleClearanceLabel.text = @"N/A";
            }
        }
        if (pressureAltitude > 8000) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Pressure Altitude Exceeds POH Data" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
            [alertView show];
            //self.groundRollLabel.text = @"N/A";
            //self.obstacleClearanceLabel.text = @"N/A";
        }
        
        if ((windDirection - runwayDirection) <= 90) {
            float angle = windDirection - runwayDirection;
            float windComponent = cosf(angle*M_PI/180);
            float headwind = windSpeed * windComponent;
            float crosswindComponent = sinf(angle*M_PI/180);
            float crosswind = windSpeed * crosswindComponent;
            float crosswindAbs = fabsf(crosswind);
            self.landingCrossWindComponent = [[NSString alloc] initWithFormat:@"%.f",crosswindAbs];
            self.landingWindComponent = [[NSString alloc] initWithFormat:@"%.f",headwind];
            int headwindInt = (int)floorf(headwind);
            int headwindCorrection = ((headwindInt/9)*.1);
            groundrollCorrectedForWind = self.groundroll * (1 - headwindCorrection);
            clearObstacleCorrectedForWind = self.clearObstacle * (1- headwindCorrection);
            self.groundRollLabel.text = [[NSString alloc] initWithFormat:@"%.f",groundrollCorrectedForWind];
            self.obstacleClearanceLabel.text = [[NSString alloc] initWithFormat:@"%.f",clearObstacleCorrectedForWind];
        } else  {
            float angle = windDirection - runwayDirection;
            float windComponent = cosf(angle*M_PI/180);
            float tailwind = windSpeed * windComponent;
            float crosswindComponent = sinf(angle*M_PI/180);
            float crosswind = windSpeed * crosswindComponent;
            self.landingCrossWindComponent = [[NSString alloc] initWithFormat:@"%.f",crosswind];
            self.landingWindComponent = [[NSString alloc] initWithFormat:@"%.f",tailwind];
            int tailwindInt = (int)floorf(tailwind);
            int tailwindCorrection = ((tailwindInt/2)*.1);
            groundrollCorrectedForWind = self.groundroll * (1 + tailwindCorrection);
            clearObstacleCorrectedForWind = self.clearObstacle * (1 + tailwindCorrection);
            self.groundRollLabel.text = [[NSString alloc] initWithFormat:@"%.f",groundrollCorrectedForWind];
            self.obstacleClearanceLabel.text = [[NSString alloc] initWithFormat:@"%.f",clearObstacleCorrectedForWind];
        }
        
        [self.view endEditing:YES];
    }
    
    
}

- (void)calculateTempIndex:(float)temp {
    if (temp < 0) {
        //fix this
        self.a = 0;
    }
    if (temp >= 0 && temp < 10) {
        self.a = 0;
        self.b = 1;
        self.tempLower = 0;
    }
    if (temp >= 10 && temp < 20) {
        self.a = 1;
        self.b = 2;
        self.tempLower = 10;
    }
    if (temp >= 20 && temp < 30) {
        self.a = 2;
        self.b = 3;
        self.tempLower = 20;
    }
    if (temp >= 30 && temp < 40) {
        self.a = 3;
        self.b = 4;
        self.tempLower = 30;
    }
    if (temp == 40) {
        self.a = 4;
    }
    // Put in error if value nil
}

- (void)pressureAltitudeKeys:(int)pressureAltitude {
    if (pressureAltitude < 0) {
        self.key1 = @0;
    }
    if (pressureAltitude >= 0 && pressureAltitude < 1000) {
        self.key1 = @0;
        self.key2 = @1000;
        self.pLower = 0;
        self.pHigher = 1000;
    }
    if (pressureAltitude >= 1000 && pressureAltitude < 2000) {
        self.key1 = @1000;
        self.key2 = @2000;
        self.pLower = 1000;
        self.pHigher = 2000;
    }
    if (pressureAltitude >= 2000 && pressureAltitude < 3000) {
        self.key1 = @2000;
        self.key2 = @3000;
        self.pLower = 2000;
        self.pHigher = 3000;
    }
    if (pressureAltitude >= 3000 && pressureAltitude < 4000) {
        self.key1 = @3000;
        self.key2 = @4000;
        self.pLower = 3000;
        self.pHigher = 4000;
    }
    if (pressureAltitude >= 4000 && pressureAltitude < 5000) {
        self.key1 = @4000;
        self.key2 = @5000;
        self.pLower = 4000;
        self.pHigher = 5000;
    }
    if (pressureAltitude >= 5000 && pressureAltitude < 6000) {
        self.key1 = @5000;
        self.key2 = @6000;
        self.pLower = 5000;
        self.pHigher = 6000;
    }
    if (pressureAltitude >= 6000 && pressureAltitude < 7000) {
        self.key1 = @6000;
        self.key2 = @7000;
        self.pLower = 6000;
        self.pHigher = 7000;
    }
    if (pressureAltitude >= 7000 && pressureAltitude < 8000) {
        self.key1 = @7000;
        self.key2 = @8000;
        self.pLower = 7000;
        self.pHigher = 8000;
    }
    if (pressureAltitude == 8000) {
        self.key1 = @1000;
    }
    else {
        
    }
}




#pragma mark - Navigation
- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if ([identifier isEqualToString:@"cessna172RPDF"]) {
        
        
        if (self.fieldElevationTextField.text.length == 0) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please enter a Field Elevation" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
            [alertView show];
            
            return NO;
        }
        if (self.pressureTextField.text.length == 0) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please enter an Altimeter Setting" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
            [alertView show];
            
            return  NO;
        }
        if (self.temperatureTextField.text.length == 0) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please Enter a temperature" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
            [alertView show];
            
            return  NO;
        }
        
    }
    
    return  YES;
}


// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"landingGraph"]) {
        CDCessna172RLandingGraphViewController *controller = (CDCessna172RLandingGraphViewController *)segue.destinationViewController;
        controller.landingWeight = self.totalWeightLabel.text;
        controller.landingArm = self.totalArmLabel.text;
        controller.landingCG = self.cgLabel.text; 
    }
    
    if ([segue.identifier isEqualToString:@"cessna172RPDF"]) {
        CDCessna172RPDFViewController *controller = (CDCessna172RPDFViewController *)segue.destinationViewController;
        controller.totalWeight = self.totalWeightBefore;
        controller.pilotWeight = self.pilotWeight;
        controller.frontSeat = self.frontSeatWeight;
        controller.rearSeat1 = self.rearSeat1Weight;
        controller.rearSeat2 = self.rearSeat2Weight;
        controller.bagArea1 = self.bagArea1Weight;
        controller.bagArea2 = self.bagArea2Weight;
        controller.fuelGal = self.fuelGallongsBefore;
        controller.fuelWeight = self.fuelWeightBefore;
        controller.emptyWeight = self.emptyWeight;
        controller.emptyArm = self.emptyArm;
        controller.landingFuelGal = self.fuelGallonsLdg;
        controller.landingFuelWeight = self.fuelWeightLdg;
        controller.landingTotalWeight = self.totalWeightLabel.text;
        controller.totalArm = self.totalArmTakeoff;
        controller.totalMoment = self.totalMomentBefore;
        controller.landingTotalArm = self.totalArmLabel.text;
        controller.landingTotalMoment = self.totalMomentLabel.text;
        controller.landingCG = self.cgLabel.text;
        controller.takeoffCG = self.takeoffCG;
        controller.landingCG = self.cgLabel.text;
        controller.landingGroundRoll = self.groundRollLabel.text;
        controller.landingClearObstacle = self.obstacleClearanceLabel.text;
        controller.takeoffGroundRoll = self.takeoffGroundRoll;
        controller.takeoffObstacleClearance = self.takeoffClearObstacle;
        controller.takeoffDensityAltitude = self.takeoffDensityAltitude;
        controller.landingDensityAltitude = self.landingDensityAltitude;
        controller.nNumber = self.nNumber;
        controller.ldgManeurvingSpeed = self.ldgManeuveringSpeed;
        controller.toManeuveringSpeed = self.toManeurveringSpeed;
        controller.landingWindDirection = self.windDirectionTextField.text;
        controller.landingWindSpeed = self.windSpeedTextField.text;
        controller.landingRunway = self.runwayTextField.text;
        controller.takeoffWindDirection = self.takeoffWindDirection;
        controller.takeoffWindSpeed = self.takeoffWindSpeed;
        controller.takeoffRunway = self.takeoffRunway;
        controller.landingWindComponent = self.landingWindComponent;
        controller.takeofWindComponent = self.takeoffWindComponent;
        controller.landingCrossWindComponent = self.landingCrossWindComponent;
        controller.takeoffCrossWindComponent = self.takeoffCrossWindComponent; 
    }
}


@end
