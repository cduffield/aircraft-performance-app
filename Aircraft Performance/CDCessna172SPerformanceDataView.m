//
//  CDCessna172SPerformanceDataView.m
//  Aircraft Performance
//
//  Created by Chip Duffield on 8/16/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import "CDCessna172SPerformanceDataView.h"

@implementation CDCessna172SPerformanceDataView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
