//
//  CDCessna172RList.m
//  Aircraft Performance
//
//  Created by Chip Duffield on 6/29/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import "CDCessna172RList.h"


@implementation CDCessna172RList

@dynamic nNumber;
@dynamic emptyWeight;
@dynamic arm;

@end
