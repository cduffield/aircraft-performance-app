//
//  CDCessna172RLandingGraphViewController.h
//  Aircraft Performance
//
//  Created by Chip Duffield on 7/9/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CorePlot-CocoaTouch.h"

@interface CDCessna172RLandingGraphViewController : UIViewController <CPTPlotDataSource>

@property (nonatomic) NSString *landingWeight;
@property (nonatomic) NSString *landingArm;
@property (nonatomic) NSString *landingCG;

@property (nonatomic) UIImage *landingIMG;
@property (nonatomic) NSData *landingPDF; 

@end
