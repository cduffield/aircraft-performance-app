//
//  CDCirrusSR20M3000CalculationViewController.m
//  Aircraft Performance
//
//  Created by Chip Duffield on 7/30/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import "CDCirrusSR20M3000CalculationViewController.h"
#import "CDCirrusSR20M3000TODataViewController.h"
#import "CDCirrusSR20M3000TakeoffGraphViewController.h"

@interface CDCirrusSR20M3000CalculationViewController ()

@end

@implementation CDCirrusSR20M3000CalculationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.automaticallyAdjustsScrollViewInsets = false;
    
    //set weights to floats
    self.emptyWeight = [self.emptyWeightText floatValue];
    self.pilotWeight = [self.pilotWeightText floatValue];
    self.frontSeatWeight = [self.frontSeatWeightText floatValue];
    self.rearSeat1Weight = [self.rearSeat1WeightText floatValue];
    self.rearSeat2Weight = [self.rearSeat2WeightText floatValue];
    self.bagAreaWeight = [self.bagAreaWeightText floatValue];
    self.fuelGal = [self.fuelGalText floatValue];
    self.fuelWeight = (self.fuelGal * 6);
    self.taxiBurnWeight = [self.taxiBurnWeightLabel.text floatValue];
    
    //set arms to floats
    self.emptyArm = [self.emptyArmText floatValue];
    self.pilotArm = [self.pilotArmLabel.text floatValue];
    self.frontSeatArm = [self.frontSeatArmLabel.text floatValue];
    self.rearSeat1Arm = [self.rearSeat1ArmLabel.text floatValue];
    self.rearSeat2Arm = [self.rearSeat2ArmLabel.text floatValue];
    self.bagAreaArm = [self.bagAreaArmLabel.text floatValue];
    self.fuelArm = [self.fuelArmLabel.text floatValue];
    self.taxiBurnArm = [self.taxiBurnArmLabel.text floatValue];
    
    //calculate moments
    self.emptyMoment = (self.emptyWeight * self.emptyArm);
    self.pilotMoment = (self.pilotWeight * self.pilotArm);
    self.frontSeatMoment = (self.frontSeatWeight * self.frontSeatArm);
    self.rearSeat1Moment = (self.rearSeat1Weight * self.rearSeat1Arm);
    self.rearSeat2Moment = (self.rearSeat2Weight * self.rearSeat2Arm);
    self.bagAreaMoment = (self.bagAreaWeight * self.bagAreaArm);
    self.fuelMoment = (self.fuelWeight * self.fuelArm);
    self.taxiBurnMoment = (self.taxiBurnWeight * self.taxiBurnArm);

    // Calculate totals
    self.totalWeight = self.emptyWeight + self.pilotWeight + self.frontSeatWeight + self.rearSeat1Weight + self.rearSeat2Weight + self.bagAreaWeight + self.fuelWeight + self.taxiBurnWeight;
    self.totalMoment = self.emptyMoment + self.pilotMoment + self.frontSeatMoment + self.rearSeat1Moment + self.rearSeat2Moment + self.bagAreaMoment + self.fuelMoment + self.taxiBurnMoment;
    self.totalArm = self.totalMoment/self.totalWeight;
    
    // Set weight labels
    self.emptyWeightLabel.text = [[NSString alloc] initWithFormat:@"%.f",self.emptyWeight];
    self.pilotWeightLabel.text = [[NSString alloc] initWithFormat:@"%.f",self.pilotWeight];
    self.frontSeatWeightLabel.text = [[NSString alloc] initWithFormat:@"%.f",self.frontSeatWeight];
    self.rearSeat1WeightLabel.text = [[NSString alloc] initWithFormat:@"%.f",self.rearSeat1Weight];
    self.rearSeat2WeightLabel.text = [[NSString alloc] initWithFormat:@"%.f", self.rearSeat2Weight];
    self.bagAreaWeightLabel.text = [[NSString alloc] initWithFormat:@"%.f", self.bagAreaWeight];
    self.fuelWeightLabel.text = [[NSString alloc] initWithFormat:@"%.f",self.fuelWeight];
    self.fuelGalLabel.text = [[NSString alloc] initWithFormat:@"%.f",self.fuelGal];
    self.totalWeightLabel.text = [[NSString alloc] initWithFormat:@"%.f",self.totalWeight];
    
    // Set Arm Labels
    self.emptyArmLabel.text = [[NSString alloc] initWithFormat:@"%.1f",self.emptyArm];
    self.totalArmLabel.text = [[NSString alloc] initWithFormat:@"%.2f",self.totalArm];
    
    // Set Moment Labels
    self.emptyMomentLabel.text = [[NSString alloc] initWithFormat:@"%.f",(self.emptyMoment/1000)];
    self.pilotMomentLabel.text = [[NSString alloc] initWithFormat:@"%.f",(self.pilotMoment/1000)];
    self.frontSeatMomentLabel.text = [[NSString alloc] initWithFormat:@"%.f",(self.frontSeatMoment/1000)];
    self.rearSeat1MomentLabel.text = [[NSString alloc] initWithFormat:@"%.f",(self.rearSeat1Moment/1000)];
    self.rearSeat2MomentLabel.text = [[NSString alloc] initWithFormat:@"%.f",(self.rearSeat2Moment/1000)];
    self.bagAreaMomentLabel.text = [[NSString alloc] initWithFormat:@"%.f",(self.bagAreaMoment/1000)];
    self.fuelMomentLabel.text = [[NSString alloc] initWithFormat:@"%.f",(self.fuelWeight/1000)];
    self.taxiBurnMomentLabel.text = [[NSString alloc] initWithFormat:@"%.f",self.taxiBurnMoment];
    self.totalMomentLabel.text = [[NSString alloc] initWithFormat:@"%.f",(self.totalMoment/1000)];
    
    // CG Calculation
    float aircraftMaxWeight = 3000;
    if (self.totalArm < 141.0) {
        self.m = 253.91;
        self.b = 33107.74;
    } else if (self.totalArm >= 141.0 && self.totalArm < 144.1) {
        self.m = 98.71;
        self.b = 11224.1;
    } else if (self.totalArm >= 144.1 && self.totalArm <= 148.0) {
        self.m = 0;
        self.b = 0;
    } else if (self.totalArm > 148.0) {
        self.m = -1000;
        self.b = -15100;
    }
    
    if (self.totalArm <= 144.6) {
        self.minM = 0;
        self.minB = 0;
    } else if (self.totalArm > 144.6 && self.totalArm <= 147.4) {
        self.minM = 164.29;
        self.minB = 21645.72;
    } else if (self.totalArm > 147.4) {
        self.minM = 471.43;
        self.minB = 66918.58;
    }
    
    float (^maxWeightAtCG)(float,float,float) = ^(float totalArm, float m, float b) {
        if (totalArm < 144.1 || totalArm > 148.0) {
            float weight = ((m * totalArm) - b);
            return weight;
        }
        else {
            float weight = 3000.0;
            return weight;
        }
        
    };
    float (^minWeightAtCG)(float,float,float) = ^(float totalArm, float m, float b) {
        if (totalArm < 144.1 || totalArm > 148.0) {
            float weight = ((m * totalArm) - b);
            return weight;
        }
        else {
            float weight = 2100.0;
            return weight;
        }
        
    };
    
    float maxWeight = maxWeightAtCG(self.totalArm, self.m, self.b);
    float minWeight = minWeightAtCG(self.totalArm, self.minM, self.minB);

    //set cg response
    if (self.totalArm >= 138.7 && self.totalArm <= 148.1) {
        if (self.totalWeight <= maxWeight && self.totalWeight >= minWeight) {
            self.cgLimits = @"Within Limits";
        }
        else if (self.totalWeight > aircraftMaxWeight) {
            float overWeight = self.totalWeight - aircraftMaxWeight;
            self.cgLimits = [[NSString alloc] initWithFormat:@"Overweight by %.f lbs",overWeight];
        }
        else {
            self.cgLimits = @"Outside of Limits";
        }
    }
    else {
        self.cgLimits = @"Outside of Limits";
    }
    self.cgLabel.text = self.cgLimits;
    
    // Maneuvering Speed
    float manSpeedMax = 131;
    float manSpeed = manSpeedMax * sqrtf(self.totalWeight/aircraftMaxWeight);
    self.maneuveringSpeedLabel.text = [[NSString alloc] initWithFormat:@"%.f",manSpeed];
    

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"takeoffData"]) {
        CDCirrusSR20M3000TODataViewController *controller = segue.destinationViewController;
        controller.nNumberText = self.nNumberText;
        controller.emptyWeightText = self.emptyWeightText;
        controller.pilotWeightText = self.pilotWeightText;
        controller.frontSeatWeightText = self.frontSeatWeightText;
        controller.rearSeat1WeightText = self.rearSeat1WeightText;
        controller.rearSeat2WeightText = self.rearSeat2WeightText;
        controller.bagAreaWeightText = self.bagAreaWeightText;
        controller.fuelGalText = self.fuelGalText;
        controller.fuelWeightText = self.fuelWeightLabel.text;
        controller.taxiBurnWeightText = self.taxiBurnWeightLabel.text;
        controller.totalWeightText = self.totalWeightLabel.text;
        controller.emptyArmText = self.emptyArmText;
        controller.pilotArmText = self.pilotArmLabel.text;
        controller.frontSeatArmText = self.frontSeatArmLabel.text;
        controller.rearSeat1ArmText = self.rearSeat1ArmLabel.text;
        controller.rearSeat2ArmText = self.rearSeat2ArmLabel.text;
        controller.bagAreaArmText = self.bagAreaArmLabel.text;
        controller.fuelArmText = self.fuelArmLabel.text;
        controller.taxiBurnArmText = self.taxiBurnArmLabel.text;
        controller.totalArmText = self.totalArmLabel.text;
        controller.emptyMomentText = self.emptyMomentLabel.text;
        controller.pilotMomentText = self.pilotMomentLabel.text;
        controller.frontSeatMomentText = self.frontSeatMomentLabel.text;
        controller.rearSeat1MomentText = self.rearSeat1MomentLabel.text;
        controller.rearSeat2MomentText = self.rearSeat2MomentLabel.text;
        controller.bagAreaMomentText = self.bagAreaMomentLabel.text;
        controller.fuelMomentText = self.fuelMomentLabel.text;
        controller.taxiBurnMomentText = self.taxiBurnMomentLabel.text;
        controller.totalMomentText = self.totalMomentLabel.text;
        controller.takeoffCGLimitsText = self.cgLimits;
        controller.takeoffManeuveringSpeedText = self.maneuveringSpeedLabel.text;
    }
    
    if ([segue.identifier isEqualToString:@"takeoffGraph"]) {
        CDCirrusSR20M3000TakeoffGraphViewController *controller = (CDCirrusSR20M3000TakeoffGraphViewController *)segue.destinationViewController;
        controller.totalWeight = self.totalWeightLabel.text;
        controller.totalArm = self.totalArmLabel.text;
        controller.cg = self.cgLimits;
    }

}

@end
