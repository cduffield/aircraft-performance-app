//
//  Aircraft_PerformanceTests.m
//  Aircraft PerformanceTests
//
//  Created by Chip Duffield on 6/29/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface Aircraft_PerformanceTests : XCTestCase

@end

@implementation Aircraft_PerformanceTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

@end
