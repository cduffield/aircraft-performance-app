//
//  CDPiperWarriorIICalculationViewController.h
//  Aircraft Performance
//
//  Created by Chip Duffield on 8/3/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDPiperWarriorIICalculationViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *emptyWeightLabel;
@property (weak, nonatomic) IBOutlet UILabel *pilotWeightLabel;
@property (weak, nonatomic) IBOutlet UILabel *frontSeatWeightLabel;
@property (weak, nonatomic) IBOutlet UILabel *rearSeat1WeightLabel;
@property (weak, nonatomic) IBOutlet UILabel *rearSeat2WeightLabel;
@property (weak, nonatomic) IBOutlet UILabel *bagAreaWeightLabel;
@property (weak, nonatomic) IBOutlet UILabel *fuelGalLabel;
@property (weak, nonatomic) IBOutlet UILabel *fuelWeightLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalWeightLabel;
@property (weak, nonatomic) IBOutlet UILabel *emptyArmLabel;
@property (weak, nonatomic) IBOutlet UILabel *pilotArmLabel;
@property (weak, nonatomic) IBOutlet UILabel *frontSeatArmLabel;
@property (weak, nonatomic) IBOutlet UILabel *rearSeat1ArmLabel;
@property (weak, nonatomic) IBOutlet UILabel *rearSeat2ArmLabel;
@property (weak, nonatomic) IBOutlet UILabel *bagAreaArmLabel;
@property (weak, nonatomic) IBOutlet UILabel *fuelArmLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalArmLabel;
@property (weak, nonatomic) IBOutlet UILabel *emptyMomentLabel;
@property (weak, nonatomic) IBOutlet UILabel *pilotMomentLabel;
@property (weak, nonatomic) IBOutlet UILabel *frontSeatMomentLabel;
@property (weak, nonatomic) IBOutlet UILabel *rearSeat1MomentLabel;
@property (weak, nonatomic) IBOutlet UILabel *rearSeat2MomentLabel;
@property (weak, nonatomic) IBOutlet UILabel *bagAreaMomentLabel;
@property (weak, nonatomic) IBOutlet UILabel *fuelMomentLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalMomentLabel;
@property (weak, nonatomic) IBOutlet UILabel *CGLimitsLabel;
@property (weak, nonatomic) IBOutlet UILabel *maneuveringSpeedLabel;

@property (nonatomic) NSString *nNumberText;
@property (nonatomic) NSString *emptyWeightText;
@property (nonatomic) NSString *emptyArmText;
@property (nonatomic) NSString *pilotWeightText;
@property (nonatomic) NSString *frontSeatWeightText;
@property (nonatomic) NSString *rearSeat1WeightText;
@property (nonatomic) NSString *rearSeat2WeightText;
@property (nonatomic) NSString *bagAreaWeightText;
@property (nonatomic) NSString *fuelGalText;


@end
