//
//  CDCirrusSR22TM3400List.m
//  Aircraft Performance
//
//  Created by Chip Duffield on 9/9/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import "CDCirrusSR22TM3400List.h"


@implementation CDCirrusSR22TM3400List

@dynamic arm;
@dynamic nNumber;
@dynamic emptyWeight;

@end
