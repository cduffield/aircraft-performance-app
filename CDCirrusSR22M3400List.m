//
//  CDCirrusSR22M3400List.m
//  Aircraft Performance
//
//  Created by Chip Duffield on 9/9/14.
//  Copyright (c) 2014 Chip Duffield. All rights reserved.
//

#import "CDCirrusSR22M3400List.h"


@implementation CDCirrusSR22M3400List

@dynamic arm;
@dynamic nNumber;
@dynamic emptyWeight;

@end
